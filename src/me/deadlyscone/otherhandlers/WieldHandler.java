package me.deadlyscone.otherhandlers;

import me.deadlyscone.enums.ArmorType;
import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class WieldHandler implements Listener {
	private final String ThisSkill = "defense";
	Plugin rpgskills;
	
	public WieldHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		rpgskills = plugin;
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		Player player = e.getPlayer();
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		if (RPGSkills.isSkillEnabled.get(worldName).get("defense") == true && ((boolean) RPGSkills.configData.get(worldName).get(ConfigType.DEFENSEARMORLEVELSENABLED).get(0))){
			if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
				if (e.getItem() != null && isArmor(e.getItem().getType())){
					
					worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
					int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
					int levelRequiredToEquip = (int)RPGSkills.configData.get(worldName).get(ConfigType.DEFENSEARMORLEVELS).get(e.getItem().getType());
					
					if(currentLevel < levelRequiredToEquip){
						levelToLowMessage(player, e.getItem().getType().toString().split("_")[0], levelRequiredToEquip);
						ItemStack item = e.getItem();
						removeArmorDelay(player, item);
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onInventoryDrag(InventoryDragEvent e){
		if (e.getWhoClicked() instanceof Player){
			
			Player player = (Player)e.getWhoClicked();
			String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
			
			if ( RPGSkills.isSkillEnabled.get(worldName).get("defense") == true && ((boolean) RPGSkills.configData.get(worldName).get(ConfigType.DEFENSEARMORLEVELSENABLED).get(0)) && e.getInventory().getType() == InventoryType.CRAFTING
					&& player.getGameMode() != GameMode.CREATIVE){
				boolean isArmorSlot = false;
				ItemStack item = e.getOldCursor();
				
				for (int slot : e.getInventorySlots()){
					if (slot >= 36){
						isArmorSlot = true;
					}
				}
				
				if(isArmorSlot && isArmor(item.getType())){
					int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
					int levelRequiredToEquip = (int)RPGSkills.configData.get(worldName).get(ConfigType.DEFENSEARMORLEVELS).get(item.getType());
					
					if (currentLevel < levelRequiredToEquip){
						levelToLowMessage(player, item.getType().toString().split("_")[0], levelRequiredToEquip);
						e.setCancelled(true);
					}
				}
			}
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e){
		if (e.getWhoClicked() instanceof Player){
			Player player = (Player)e.getWhoClicked();
			String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
			boolean canEquip = true;
			
			if (RPGSkills.isSkillEnabled.get(worldName).get("defense") == true && ((boolean) RPGSkills.configData.get(worldName).get(ConfigType.DEFENSEARMORLEVELSENABLED).get(0)) && e.getInventory().getType() == InventoryType.CRAFTING
					&& player.getGameMode() != GameMode.CREATIVE && ((e.getCurrentItem() != null && isArmor(e.getCurrentItem().getType())) || 
							(e.getCursor() != null && e.getSlotType() == SlotType.ARMOR && isArmor(e.getCursor().getType())))){
				
				worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
				int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
				int levelRequiredToEquip = e.getCurrentItem().getType() == Material.AIR ? (int)RPGSkills.configData.get(worldName).get(ConfigType.DEFENSEARMORLEVELS).get(e.getCursor().getType()) : (int)RPGSkills.configData.get(worldName).get(ConfigType.DEFENSEARMORLEVELS).get(e.getCurrentItem().getType());
				
				if (isArmor(e.getCurrentItem().getType()) && e.getCursor().getType() == Material.AIR && e.getSlotType() == SlotType.ARMOR){
					return;
				}
				//if player shift clicks their armor into their armor slot
				if (e.isShiftClick() && currentLevel < levelRequiredToEquip){
						canEquip = false;
					}
					
					//If the player puts armor into an armor slot
				if (e.getSlotType() == SlotType.ARMOR && currentLevel < levelRequiredToEquip){
						canEquip = false;
					}
				
				
				//FINALLY
				if (!canEquip){
					String armorType = (e.getCurrentItem().getType() == Material.AIR ? e.getCursor().getType().toString().split("_")[0] : e.getCurrentItem().getType().toString().split("_")[0]);
					levelToLowMessage(player, armorType, levelRequiredToEquip);
					e.setCancelled(true);
				}
			}
		}
		
	}
	
	@EventHandler
	public void onArmorDispense(BlockDispenseEvent e){
		
		String worldName = ExperienceSystemHandler.checkActiveWorld(e.getBlock().getWorld().getName());
		if (RPGSkills.isSkillEnabled.get(worldName).get("defense") == true && ((boolean) RPGSkills.configData.get(worldName).get(ConfigType.DEFENSEARMORLEVELSENABLED).get(0))){
			
		MaterialData m = e.getBlock().getState().getData();
		org.bukkit.material.Dispenser dispenser = (org.bukkit.material.Dispenser)m;
		Location relBlockLoc = e.getBlock().getRelative(dispenser.getFacing(), 1).getLocation();
		
		for (Player player : Bukkit.getServer().getOnlinePlayers()){
			if (player.getLocation().getBlock().getLocation().equals(relBlockLoc)){
				if (isArmor(e.getItem().getType())){
					
					worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
					int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
					int levelRequiredToEquip = (int)RPGSkills.configData.get(worldName).get(ConfigType.DEFENSEARMORLEVELS).get(e.getItem().getType());
					
					if(currentLevel < levelRequiredToEquip){
						levelToLowMessage(player, e.getItem().getType().toString().split("_")[0], levelRequiredToEquip);
						
						/**org.bukkit.block.Dispenser dispenserBlock = (org.bukkit.block.Dispenser)e.getBlock().getState();
						Item drop = e.getBlock().getWorld().dropItem(relBlockLoc, item);
						drop.setVelocity(e.getVelocity());
						**/
						e.setCancelled(true);
					}
				}
			}
		}
	  }
	}
	
	private void levelToLowMessage(Player player, String armorType, int levelReq){
		player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(levelReq) + " to wield " +
				armorType + " armor.");
	}
	
	private void removeArmorDelay(final Player player, final ItemStack item){
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.scheduleSyncDelayedTask(rpgskills, new Runnable() {
			@Override
            public void run() {
                for (ItemStack it : player.getInventory().getArmorContents()){
                	if (it != null && it.getType() == item.getType()){
                		switch(getArmorType(it.getType())){
                		case HELMET:
                			ItemStack hItem = player.getInventory().getHelmet();
                			player.getInventory().setHelmet(new ItemStack(Material.AIR, 1));
                			player.getInventory().addItem(hItem);
                			break;
						case BOOTS:
							ItemStack bItem = player.getInventory().getBoots();
                			player.getInventory().setBoots(new ItemStack(Material.AIR, 1));
                			player.getInventory().addItem(bItem);
							break;
						case CHESTPLATE:
							ItemStack cItem = player.getInventory().getChestplate();
                			player.getInventory().setChestplate(new ItemStack(Material.AIR, 1));
                			player.getInventory().addItem(cItem);
							break;
						case LEGGINGS:
							ItemStack lItem = player.getInventory().getLeggings();
                			player.getInventory().setLeggings(new ItemStack(Material.AIR, 1));
                			player.getInventory().addItem(lItem);
							break;
						default:
							break;
                		}
                		
                		
                	}
                }
            }
        }, 1L);
	}
	
	private ArmorType getArmorType(Material m){
		switch (m){
		
		case LEATHER_HELMET:
		case GOLD_HELMET:
		case DIAMOND_HELMET:	
		case CHAINMAIL_HELMET:	
		case IRON_HELMET:
			return ArmorType.HELMET;
		
		case LEATHER_CHESTPLATE:
		case IRON_CHESTPLATE:
		case GOLD_CHESTPLATE:
		case DIAMOND_CHESTPLATE:
		case CHAINMAIL_CHESTPLATE:
			return ArmorType.CHESTPLATE;
			
		case IRON_LEGGINGS:
		case LEATHER_LEGGINGS:
		case GOLD_LEGGINGS:
		case DIAMOND_LEGGINGS:
	    case CHAINMAIL_LEGGINGS:
	    	return ArmorType.LEGGINGS;
	    	
		case CHAINMAIL_BOOTS:
		case IRON_BOOTS:
		case LEATHER_BOOTS:
		case GOLD_BOOTS:
		case DIAMOND_BOOTS:
			return ArmorType.BOOTS;
			
			default:
				return null;
		}
			
	}
	
	private boolean isArmor(Material m){
		switch (m){
		case LEATHER_HELMET:
		case LEATHER_CHESTPLATE:
		case LEATHER_LEGGINGS:
		case LEATHER_BOOTS:
		
		case IRON_HELMET:
		case IRON_CHESTPLATE:
		case IRON_LEGGINGS:
		case IRON_BOOTS:
		
		case GOLD_HELMET:
		case GOLD_CHESTPLATE:
		case GOLD_LEGGINGS:
		case GOLD_BOOTS:
		
		case DIAMOND_HELMET:
		case DIAMOND_CHESTPLATE:
		case DIAMOND_LEGGINGS:
		case DIAMOND_BOOTS:
			
		case CHAINMAIL_HELMET:
		case CHAINMAIL_CHESTPLATE:
		case CHAINMAIL_LEGGINGS:
		case CHAINMAIL_BOOTS:
		return true;
		
		default:
			return false;
		}
	}
}
