package me.deadlyscone.otherhandlers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import me.deadlyscone.main.RPGSkills;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class MMOConversionHandler {

	private static String[] skills = {"attack", "archery", "mining", "woodcutting", "unarmed", "farming", "excavation", "fishing"};
	@SuppressWarnings("deprecation")
	public static void convertLevels(int mmoCap) throws IOException{
		

		boolean mcmmoRunning = Bukkit.getServer().getPluginManager().isPluginEnabled("mcMMO");
		if(mcmmoRunning){
			int t = mmoCap / 100;
			UUID player = null;
		    File path = new File(Bukkit.getServer().getPluginManager().getPlugin("mcMMO").getDataFolder(), "/flatfile/mcmmo.users");
			FileInputStream fis = new FileInputStream(path);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] d = line.split(":");
				try{
					player = Bukkit.getServer().getOfflinePlayer(d[0]).getUniqueId();
				}catch(NullPointerException e){
					System.out.println("[RPGSkills] Unable to convert player " + d[0] + ".");
					continue;
				}
				map.put("mining", Integer.valueOf(d[1])/t);
				map.put("woodcutting", Integer.valueOf(d[5])/t);
				map.put("unarmed", Integer.valueOf(d[8])/t);
				map.put("farming", Integer.valueOf(d[9])/t);
				map.put("excavation", Integer.valueOf(d[10])/t);
				map.put("archery", Integer.valueOf(d[11])/t);
				map.put("attack", Integer.valueOf(d[12])/t);
				map.put("fishing", Integer.valueOf(d[34])/t);
				
			    Iterator<Entry<String, Integer>> it = map.entrySet().iterator();
			    for (String worldName : RPGSkills.ConverterWorlds){
			    while (it.hasNext()) {
			        Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>)it.next();
			        
			        if (pairs.getValue() < RPGSkills.skillMinLevel.get(worldName).get(pairs.getKey())){
			        	map.put(pairs.getKey(), RPGSkills.skillMinLevel.get(worldName).get(pairs.getKey()));
			        }else if (pairs.getValue() > RPGSkills.skillMaxLevel.get(worldName).get(pairs.getKey())){
			        	map.put(pairs.getKey(), RPGSkills.skillMaxLevel.get(worldName).get(pairs.getKey()));
			        }
			        	
			    }
			    

			    for (String s :skills){
			    	HashMap<Integer, Double> levelExpMap = new HashMap<Integer, Double>(ExperienceSystemHandler.getBaseExpPerLevel(worldName, s));
			    	Double newLevel = levelExpMap.get(map.get(s));
			    	if (!player.equals(null) && newLevel != null){
						ExperienceSystemHandler.setPlayerExp(worldName, player, s, newLevel);
			    	}

			    }
			   }
			}
		 
			br.close();
			// fix this to save the value/@@@
			File customYmlSkills = new File(Bukkit.getPluginManager().getPlugin("RPGSkills").getDataFolder(), "/converter.yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			customSkillsConfig.set("convert", false);
			try{
				customSkillsConfig.save(customYmlSkills);
			}catch(Exception ex){
				ex.printStackTrace();
			}
			System.out.println("[RPGSkills] Successfully converted levels!");
		}else{
			System.out.println("[RPGSkills] Whoops!, To convert levels ensure mcmmo is running and its files exist.");
		}
	}

}
