package me.deadlyscone.otherhandlers;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SilkSpawnerHandler implements Listener{
	
	public SilkSpawnerHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onBreakSpawner(BlockBreakEvent e){
		if (e.getBlock().getType() == Material.MOB_SPAWNER && (boolean)RPGSkills.configData.get(e.getPlayer().getWorld().getName()).get(ConfigType.ISSILKSPAWNERSENABLED).get(0) && e.getPlayer().getItemInHand().containsEnchantment(Enchantment.SILK_TOUCH)){
			
			CreatureSpawner cs = (CreatureSpawner) e.getBlock().getState();
			ItemStack item = new ItemStack(e.getBlock().getType(), 1);
			ItemMeta im = item.getItemMeta();
			
			//set spawner name for later use
			im.setDisplayName(cs.getCreatureTypeName() + " Spawner");
			item.setItemMeta(im);
			
			//Drop the spawner
			e.setExpToDrop(0);
			e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), item);
			
		}
	}
	
	@EventHandler
	public void onPlaceSpawner(BlockPlaceEvent e){
		
		if (e.getBlock().getType() == Material.MOB_SPAWNER && (boolean)RPGSkills.configData.get(e.getPlayer().getWorld().getName()).get(ConfigType.ISSILKSPAWNERSENABLED).get(0)){
			CreatureSpawner cs = (CreatureSpawner) e.getBlock().getState();
			ItemStack item = e.getItemInHand();
			ItemMeta itemMeta = item.getItemMeta();
			String name = itemMeta.getDisplayName();
			
			if(name != null){
				String splitName[] = name.split(" ", 0);
				cs.setCreatureTypeByName(splitName[0]);
			}
		}
	}
	
	@EventHandler
	public void onSpawnerNameChange(InventoryClickEvent e){
		if(!e.isCancelled()){
			HumanEntity ent = e.getWhoClicked();
			 
			// not really necessary
			if(ent instanceof Player){
			Player player = (Player)ent;
			Inventory inv = e.getInventory();
			 
			// see if the event is about an anvil
			if(inv instanceof AnvilInventory){
			InventoryView view = e.getView();
			int rawSlot = e.getRawSlot();
			 
			// compare the raw slot with the inventory view to make sure we are talking about the upper inventory
			if(rawSlot == view.convertSlot(rawSlot)){
			if(rawSlot == 2){
			ItemStack item = e.getCurrentItem();
			// check if there is an item in the result slot
			if(item != null){
			ItemMeta meta = item.getItemMeta();
			 
			// it is possible that the item does not have meta data
			if(meta != null){
			// see whether the item is being renamed
			if(meta.hasDisplayName()){
			//String displayName = meta.getDisplayName();
			if (item.getType() == Material.MOB_SPAWNER && !player.hasPermission("rpgskills.spawner.rename")){
				player.sendMessage(ChatColor.RED + "You are not allowed to use mob spawners with an anvil!");
				e.setCancelled(true);
			}
			
			}
			}
			}
			}
			}
			}
			}
		}
	}

}
