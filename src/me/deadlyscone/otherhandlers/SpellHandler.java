package me.deadlyscone.otherhandlers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.SmallFireball;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.WitherSkull;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

public class SpellHandler{
	//give experience per item but don't subtract MP for each.
	
	
	public static boolean doSuperHeat(ItemStack itemToSmelt, Player player){
		//int cost = 0;
		
			switch (itemToSmelt.getType()){
			
			case IRON_ORE:
				itemToSmelt.setType(Material.IRON_INGOT);
				player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_HURT, 0.6f, 0.5f);
				return true;
			case GOLD_ORE:
				itemToSmelt.setType(Material.GOLD_INGOT);
				player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_HURT, 0.6f, 0.5f);
				return true;
			case SAND:
				itemToSmelt.setType(Material.GLASS);
				player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_HURT, 0.6f, 0.5f);
				return true;
			case COBBLESTONE:
				itemToSmelt.setType(Material.STONE);
				player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_HURT, 0.6f, 0.5f);
				return true;
			case CLAY_BALL:
				itemToSmelt.setType(Material.CLAY_BRICK);
				player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_HURT, 0.6f, 0.5f);
				return true;
			case NETHERRACK:
				itemToSmelt.setType(Material.NETHER_BRICK_ITEM);
				player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_HURT, 0.6f, 0.5f);
				return true;
			case CLAY:
				itemToSmelt.setType(Material.HARD_CLAY);
				player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_HURT, 0.6f, 0.5f);
				return true;
			case LOG:
				itemToSmelt.setType(Material.COAL);
				itemToSmelt.setDurability((short)1);
				player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_HURT, 0.6f, 0.5f);
				return true;
			case LOG_2:
				itemToSmelt.setType(Material.COAL);
				itemToSmelt.setDurability((short)1);
				player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_HURT, 0.6f, 0.5f);
				return true;
			case SMOOTH_BRICK:
				
				if (itemToSmelt.getDurability() == (short)0){
					itemToSmelt.setDurability((short)2);
					player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_HURT, 0.6f, 0.5f);
				}else{
					player.sendMessage(ChatColor.RED + "You cannot smelt this item!");
					return false;
				}
				return true;
			default:
				player.sendMessage(ChatColor.RED + "You cannot smelt this item!");
				return false;
			}
	}
	
	@SuppressWarnings("deprecation")
	public static boolean doBonesToApples(Player player, String worldName){
		Inventory playerInv = player.getInventory();
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("BONESTOAPPLES")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int bones = 0;
		int bonesReq = Integer.valueOf(data[in.indexOf("Bones Required")]);
		int yield = Integer.valueOf(data[in.indexOf("Yield")]);
		
		for (ItemStack item : playerInv){
			if (item != null && item.getType() == Material.BONE){
				bones += item.getAmount();
			}
		}

			if (bones > 0 && bones >= bonesReq){
				int bonesToRemove = ((bones/bonesReq)*bonesReq);
				int apples = ((bones/bonesReq)*yield);
				
				player.getInventory().removeItem(new ItemStack(Material.BONE, bonesToRemove));
				HashMap<Integer,ItemStack> overflow = player.getInventory().addItem(new ItemStack(Material.APPLE, apples));
				
				if (!overflow.isEmpty() && overflow.get(0) != null){
					player.sendMessage(ChatColor.RED+"Your inventory is full!");
					player.getWorld().dropItemNaturally(player.getLocation(), overflow.get(0));
				}
				player.updateInventory();
				return true;
			}
		return false;
	}
	
	public static boolean doWindBolt(Player attacker, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("WINDBOLT")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		
		double push = Double.valueOf(data[in.indexOf("Push")]);
		double lift = Double.valueOf(data[in.indexOf("Lift")]);
		int damage = Integer.valueOf(data[in.indexOf("Damage")]);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		Entity entity = getTarget(attacker, radius);
		
		if (entity != null && entity.isOnGround() && !entity.isDead()){
			Vector vec = new Vector(attacker.getLocation().getDirection().getX()*push, lift, attacker.getLocation().getDirection().getZ()*push);

			if (entity instanceof Damageable){
				Damageable damageablePlayer = (Damageable) entity;
				damageablePlayer.damage(damage, attacker);
				entity.setVelocity(vec);
			}
			return true;
		}
		return false;

	}
	public static boolean doWindSurge(Player attacker, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("WINDSURGE")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		
		double push = Double.valueOf(data[in.indexOf("Push")]);
		double lift = Double.valueOf(data[in.indexOf("Lift")]);
		int damage = Integer.valueOf(data[in.indexOf("Damage")]);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		Entity entity = getTarget(attacker, radius);
		
		if (entity != null && entity.isOnGround() && !entity.isDead()){
			Vector vec = new Vector(attacker.getLocation().getDirection().getX()*push, lift, attacker.getLocation().getDirection().getZ()*push);
			if (entity instanceof Damageable){
				Damageable damageablePlayer = (Damageable) entity;
				damageablePlayer.damage(damage, attacker);
				entity.setVelocity(vec);
			}
			return true;
		}
		return false;

	}
	
	//**WIND BARRAGE**
	public static void doWindBarrage(Player attacker, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("WINDBARRAGE")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		double damage = Double.valueOf(data[in.indexOf("Damage")]);
		double push = Double.valueOf(data[in.indexOf("Push")]);
		double lift = Double.valueOf(data[in.indexOf("Lift")]);
		
            for (Entity ent : attacker.getNearbyEntities(radius, radius, radius)){
            	if ((ent instanceof Creature || ent instanceof Player) && !ent.isDead()){
            		if (ent instanceof Tameable){
            			Tameable tam = (Tameable)ent;
            			if (tam.getOwner() != null && tam.getOwner().equals((AnimalTamer)attacker)){
            				continue;
            			}
            		}
            		if (ent instanceof Damageable){
        				Damageable damageableEntity = (Damageable) ent;
        				damageableEntity.damage(damage, attacker);
            		}
        			Vector vec = new Vector(Vector.getRandom().getX()*push, lift, Vector.getRandom().getZ()*push);
            		ent.setVelocity(vec);
            		
			
           }
		}
		
	}
	
	//**LIGHTNING BARRAGE**
	public static void doLightningBarrage(Player attacker, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("LIGHTNINGBARRAGE")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int fireTicks = Integer.valueOf(data[in.indexOf("Fire Ticks")]);
		double damage = Double.valueOf(data[in.indexOf("Damage")]);
		
            for (Entity ent : attacker.getNearbyEntities(radius, radius, radius)){
            	if ((ent instanceof Creature || ent instanceof Player) && !ent.isDead()){
            		if (ent instanceof Tameable){
            			Tameable tam = (Tameable)ent;
            			if (tam.getOwner() != null && tam.getOwner().equals((AnimalTamer)attacker)){
            				continue;
            			}
            		}
            		if (ent instanceof Damageable){
        				Damageable damageableEntity = (Damageable) ent;
        				((LivingEntity) ent).getWorld().strikeLightningEffect(ent.getLocation());
        				damageableEntity.damage(damage, attacker);
        				damageableEntity.setFireTicks(fireTicks);
        				
            		}
            		
			
           }
		}
		
	}
	
	//**HEALOTHER**
	public static void doHealGroup(Player caster, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("HEALGROUP")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		
            for (Entity ent : caster.getNearbyEntities(radius, radius, radius)){
            	if ((ent instanceof Creature || ent instanceof Player) && !ent.isDead() && ent != caster){
            		((LivingEntity) ent).addPotionEffect(new PotionEffect(PotionEffectType.HEAL, (20*Duration), Level));
           }
		}
		
	}
	
	//**BIND**
	public static boolean doBind(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("BIND")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		
		Entity entity = getTarget(player, radius);
		if ((entity instanceof LivingEntity) ){
		  ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (20*Duration), Level));
		  return true;
		}
		return false;
	}
	
	//**NIGHTVISION**
	public static void doNightVision(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("NIGHTVISION")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, (20*Duration), 1));
	}
	
	//**HEAL**
	public static void doHeal(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("HEAL")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, (20*Duration), Level));
	}
	
	//**HASTE**
	public static void doHaste(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("HASTE")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, (20*Duration), Level));
	}
	
	//**REGENERATION**
	public static void doRegeneration(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("REGENERATION")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, (20*Duration), Level));
	}
	
	//**VANISH**
	public static void doVanish(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("VANISH")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, (20*Duration), 1));
	}
	
	//**GILLS**
	public static void doGills(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("GILLS")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		player.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, (20*Duration), 1));
	}
	
	//**FIRERESISTANCE**
	public static void doResistFire(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("RESISTFIRE")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, (20*Duration), 1));
	}
	
	//**EYELESS**
	public static boolean doEyeless(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("EYELESS")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		
		Entity entity = getTarget(player, radius);
		if ((entity instanceof LivingEntity) ){
		  ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, (20*Duration), 1));
		  return true;
		}
		return false;
	}
	
	//**HUMIDIFY**
	@SuppressWarnings("deprecation")
	public static boolean doHumidify(Player player, String worldName){
		Inventory playerInv = player.getInventory();
		int buckets = 0;
		int bottles = 0;
		boolean hasBuckets = false, hasBottles = false;
		
		//get how many buckets
		for (ItemStack item : playerInv){
			if (item != null && item.getType() == Material.BUCKET){
				buckets += item.getAmount();
			}	
			if (item != null && item.getType() == Material.GLASS_BOTTLE){
				bottles += item.getAmount();
			}
		}
		
		//add and remove
		while(buckets > 0){
			player.getInventory().removeItem(new ItemStack(Material.BUCKET, 1));
			buckets--;
			HashMap<Integer, ItemStack> overflow = player.getInventory().addItem(new ItemStack(Material.WATER_BUCKET, 1));
			
			if (!overflow.isEmpty() && overflow.get(0) != null){
				player.sendMessage(ChatColor.RED+"Your inventory is full!");
				player.getWorld().dropItemNaturally(player.getLocation(), overflow.get(0));
				player.updateInventory();
				hasBuckets = true;
				break;
			}
			player.updateInventory();
			hasBuckets = true;
		}
		
		//add and remove
		if (bottles > 0){
		player.getInventory().removeItem(new ItemStack(Material.GLASS_BOTTLE, bottles));
		HashMap<Integer, ItemStack> overflow = player.getInventory().addItem(new ItemStack(Material.POTION, bottles));
		
		if (!overflow.isEmpty() && overflow.get(0) != null){
			player.sendMessage(ChatColor.RED+"Your inventory is full!");
			player.getWorld().dropItemNaturally(player.getLocation(), overflow.get(0));
		}
		player.updateInventory();
		hasBottles = true;
	  }
		
		if (hasBottles || hasBuckets){
			return true;
		}
		
		return false;
	}
	
	
	
	//**HEALOTHER**
	public static boolean doHealOther(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("HEALOTHER")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		
		Entity entity = getTarget(player, radius);
		if ((entity instanceof LivingEntity) ){
		  ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.HEAL, (20*Duration), Level));
		  return true;
		}
		return false;
	}
	
	//**SUFFOCATE**
	public static boolean doSuffocate(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("SUFFOCATE")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		
		Entity entity = getTarget(player, radius);
		if ((entity instanceof LivingEntity) ){
		  ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.HARM, (20*Duration), Level));
		  return true;
		}
		return false;
	}
	
	//**LIGHTNINGBOLT**
	public static boolean doLightningBolt(Player attacker, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("LIGHTNINGBOLT")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int fireTicks = Integer.valueOf(data[in.indexOf("Fire Ticks")]);
		double damage = Double.valueOf(data[in.indexOf("Damage")]);
		
		Entity entity = getTarget(attacker, radius);
		if ((entity instanceof LivingEntity && !entity.isDead()) ){
			Damageable damageableEntity = (Damageable) entity;
			((LivingEntity) entity).getWorld().strikeLightningEffect(entity.getLocation());
			damageableEntity.damage(damage, attacker);
			damageableEntity.setFireTicks(fireTicks);
		  return true;
		}
		return false;
	}
	
	//**CONFUSE**
	public static boolean doConfuse(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("CONFUSE")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		
		Entity entity = getTarget(player, radius);
		if ((entity instanceof LivingEntity) ){
		  ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, (20*Duration), 1));
		  return true;
		}
		return false;
	}
	
	//**CURSE**
	public static boolean doCurse(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("CURSE")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		
		Entity entity = getTarget(player, radius);
		if ((entity instanceof LivingEntity) ){
		  ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, (20*Duration), -Level));
		  return true;
		}
		return false;
	}
	
	//**DECAY**
	public static boolean doDecay(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("DECAY")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		
		Entity entity = getTarget(player, radius);
		if ((entity instanceof LivingEntity) ){
		  ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.WITHER, (20*Duration), Level));
		  return true;
		}
		return false;
	}
	
	//**POISON**
	public static boolean doPoison(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("POISON")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		
		Entity entity = getTarget(player, radius);
		if ((entity instanceof LivingEntity) ){
		  ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.POISON, (20*Duration), Level));
		  return true;
		}
		return false;
	}
	
	//**VUNERABLITY**
	public static boolean doVulnerability(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("VULNERABILITY")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		
		Entity entity = getTarget(player, radius);
		if ((entity instanceof LivingEntity) ){
		  ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, (20*Duration), -Level));
		  return true;
		}
		return false;
	}
	
	//**SNARE**
	public static boolean doSnare(Player player, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("SNARE")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		
		Entity entity = getTarget(player, radius);
		if ((entity instanceof LivingEntity) ){
		  ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (20*Duration), Level));
		  return true;
		}
		return false;
	}
	
	//**ENTANGLE**
	public static boolean doEntangle(Player player,String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("ENTANGLE")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int Level = Integer.valueOf(data[in.indexOf("Level")]);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		int Duration = Integer.valueOf(data[in.indexOf("Duration")]);
		
		Entity entity = getTarget(player, radius);
		if ((entity instanceof LivingEntity) ){
		  ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (20*Duration), Level));
		  return true;
		}
		return false;
	}
	
	//**FIRE BOLT**
	public static void doFireBolt(Player player, String worldName){
		UUID uuid = player.launchProjectile(SmallFireball.class, player.getLocation().getDirection()).getUniqueId();
		RPGSkills.SpellProjectileUUIDs.put(uuid, "FIREBOLT");
		RPGSkills.ProjectileInt.put(uuid, 1);
	}
	
	//**FIRE BARRAGE**
	public static void doFireBarrage(Player attacker, String worldName){
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("FIREBARRAGE")).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int radius = Integer.valueOf(data[in.indexOf("Radius")]);
		
            for (Entity ent : attacker.getNearbyEntities(radius, radius, radius)){
            	if ((ent instanceof Creature || ent instanceof Player) && !ent.isDead()){
            		if (ent instanceof Tameable){
            			Tameable tam = (Tameable)ent;
            			if (tam.getOwner() != null && tam.getOwner().equals((AnimalTamer)attacker)){
            				continue;
            			}
            		}
			Location highestAtEntityLoc = getHighestBlockY(ent);
			Vector vec = (ent.getLocation().toVector().subtract(highestAtEntityLoc.toVector()));
			Fireball sf = (Fireball) ent.getWorld().spawn(highestAtEntityLoc, Fireball.class);
			sf.setShooter(attacker);
			sf.setDirection(vec.multiply(0.2));
			RPGSkills.SpellProjectileUUIDs.put(sf.getUniqueId(), "FIREBARRAGE");
			RPGSkills.ProjectileInt.put(sf.getUniqueId(), 0);
			
           }
		}
	}
	public static void doShadowBolt(Player player, String worldName){
		UUID uuid = player.launchProjectile(WitherSkull.class, player.getLocation().getDirection()).getUniqueId();
		RPGSkills.SpellProjectileUUIDs.put(uuid, "SHADOWBOLT");
		RPGSkills.ProjectileInt.put(uuid, 1);
	}
	
	 public static Entity getTarget(final Player player, int radius) {
	        BlockIterator iterator = new BlockIterator(player.getWorld(), player
	                .getLocation().toVector(), player.getEyeLocation()
	                .getDirection(), 0, radius);
	        Entity target = null;
	        while (iterator.hasNext()) {
	            Block item = iterator.next();
	            for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
	                int acc = 2;
	                for (int x = -acc; x < acc; x++)
	                    for (int z = -acc; z < acc; z++)
	                        for (int y = -acc; y < acc; y++)
	                            if (entity.getLocation().getBlock()
	                                    .getRelative(x, y, z).equals(item)) {
	                                return target = entity;
	                            }
	            }
	        }
	        return target;
	    }
	 
	 private static Location getHighestBlockY(Entity entity){
			Location highestAtEntityLoc = null;
			
	    	for (int i = 1; i < 256; i++){
	    		Block b = entity.getWorld().getBlockAt(entity.getLocation());
	    		
	    		if (b.getRelative(BlockFace.UP, i).getType() != Material.AIR && 
	    				b.getRelative(BlockFace.UP, i).getLocation().getBlockY() > entity.getLocation().getBlockY()){
	    			highestAtEntityLoc = b.getRelative(BlockFace.UP, i).getLocation();
	    			highestAtEntityLoc.setY(highestAtEntityLoc.getY() - 1);
	    			return highestAtEntityLoc;
	    		}
	    		//else if (b.getRelative(BlockFace.UP, i).getLocation().getBlockY() <= entity.getLocation().getBlockY()){
	    			//highestAtEntityLoc = b.getRelative(BlockFace.UP, 20).getLocation();
	    			//return highestAtEntityLoc;
	    		//}
	    	}
	    	Block b = entity.getWorld().getBlockAt(entity.getLocation());
	    	return b.getRelative(BlockFace.UP, 20).getLocation();
	 }

}
