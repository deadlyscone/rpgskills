package me.deadlyscone.otherhandlers;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;

import me.deadlyscone.main.RPGSkills;

public class PlayerWorldChangeHandler implements Listener{
	
	public PlayerWorldChangeHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	
	@EventHandler
	public void onPlayerChangeWorld(PlayerChangedWorldEvent e){
		JoinLeaveHandler.updatePlayerPrefix(e.getPlayer());
	}
}
