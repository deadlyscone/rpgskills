package me.deadlyscone.otherhandlers;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.deadlyscone.main.RPGSkills;

public class PopulatePlayerExperienceMap implements Listener{
	
	public PopulatePlayerExperienceMap(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		UUID playerUUID = e.getPlayer().getUniqueId();
		
		for(World world : Bukkit.getServer().getWorlds()){
		if (RPGSkills.worldExp.get(world.getName()) == null){
					RPGSkills.worldExp.put(world.getName(), new HashMap<UUID,HashMap<String,Double>>());
		}
		
		if (RPGSkills.playerMagicPoints.get(world.getName()) != null && RPGSkills.playerMagicPoints.get(world.getName()).get(playerUUID) == null){
			RPGSkills.playerMagicPoints.get(world.getName()).put(playerUUID, null);
		}
		
		if (RPGSkills.worldExp.get(world.getName()) != null && RPGSkills.worldExp.get(world.getName()).get(playerUUID) == null){
				RPGSkills.worldExp.get(world.getName()).put(playerUUID, new HashMap<String,Double>());
			for (String skill : RPGSkills.skills){
				RPGSkills.worldExp.get(world.getName()).get(playerUUID).put(skill, null);
			}
		}
		
	  }
		
	}
}
