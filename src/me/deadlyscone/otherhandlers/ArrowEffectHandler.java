package me.deadlyscone.otherhandlers;

import java.util.Arrays;
import java.util.List;

import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.deadlyscone.enums.ArrowEffect;
import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;

public class ArrowEffectHandler {
	
	public static void applyEffect(ArrowEffect effect, LivingEntity attacked){
			runEffect(effect, attacked);
	}
	
	private static void runEffect(ArrowEffect effect, LivingEntity attacked) {
		String worldName = ExperienceSystemHandler.checkActiveWorld(attacked.getWorld().getName());;
		String[] data = ((String)RPGSkills.configData.get(worldName).get(ConfigType.ARROWEFFECTDATA).get(effect)).split(",");
		List<String> listData = Arrays.asList(RPGSkills.ArrowEffectPropertyTypes);
		switch(effect){
		case EXPLOSION:{
			float magnitude = Float.valueOf(data[listData.indexOf("Magnitude")]);
			attacked.getWorld().createExplosion(attacked.getLocation().getX(), attacked.getLocation().getY(), attacked.getLocation().getZ(), magnitude, false, false);
		}
			break;
		case LIGHTNING:{
			int fireTicks = Integer.valueOf(data[listData.indexOf("Fire Ticks")]);
			attacked.getWorld().strikeLightningEffect(attacked.getLocation());
			attacked.setFireTicks(fireTicks);
		}
			break;
		case NONE:
			break;
		case POISON:{
			int duration = Integer.valueOf(data[listData.indexOf("Duration")]), level = Integer.valueOf(data[listData.indexOf("Level")]);
			attacked.addPotionEffect(new PotionEffect(PotionEffectType.POISON,(20*duration) , level));
		}
			break;
		case SLOWNESS:{
			int duration = Integer.valueOf(data[listData.indexOf("Duration")]), level = Integer.valueOf(data[listData.indexOf("Level")]);
			attacked.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,(20*duration) , level));
		}
			break;
		default:
			break;
		}
		
	}

}
