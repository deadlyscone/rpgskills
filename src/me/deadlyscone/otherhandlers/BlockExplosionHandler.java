package me.deadlyscone.otherhandlers;

import me.deadlyscone.main.RPGSkills;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

public class BlockExplosionHandler implements Listener{
	
	public BlockExplosionHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler()
	public void onBlockExplode(EntityExplodeEvent e){
		
		if (RPGSkills.ProjectileInt.containsKey(e.getEntity().getUniqueId()) &&
				RPGSkills.ProjectileInt.get(e.getEntity().getUniqueId()) < 1){
			int i = RPGSkills.ProjectileInt.get(e.getEntity().getUniqueId());
			e.blockList().clear();
			RPGSkills.ProjectileInt.put(e.getEntity().getUniqueId(), i++);
			
		}else if (RPGSkills.ProjectileInt.containsKey(e.getEntity().getUniqueId()) &&
				RPGSkills.ProjectileInt.get(e.getEntity().getUniqueId()) == 1){
			e.blockList().clear();
			RPGSkills.ProjectileInt.remove(e.getEntity().getUniqueId());
			
		}
	}

}
