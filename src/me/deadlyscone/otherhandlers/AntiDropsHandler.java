package me.deadlyscone.otherhandlers;

import java.util.HashMap;

import me.deadlyscone.main.RPGSkills;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class AntiDropsHandler implements Listener{
	Plugin rpgskills;
	
	private HashMap<Location, Player> drops = new HashMap<Location, Player>();
	
	public AntiDropsHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		rpgskills = plugin;
	}
	
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		if (RPGSkills.isAntiDropsEnabled && !e.isCancelled()){
				drops.put(e.getBlock().getLocation(), e.getPlayer());
				removeKeyDelay(e.getBlock().getLocation());
		}
	}
	
	@EventHandler
	public void onItemDrop(ItemSpawnEvent e){
		if (RPGSkills.isAntiDropsEnabled){
			
		Location itemLoc = e.getLocation().getBlock().getLocation();
		
		if (drops.containsKey(itemLoc)){
			Player player = drops.get(itemLoc);
			
			if (e.getEntity() instanceof Item){
				Item item = (Item)e.getEntity();
				
				HashMap<Integer, ItemStack> overflow = player.getInventory().addItem(item.getItemStack());
				
				if (overflow.isEmpty()){
					e.setCancelled(true);
				}
				
			}
		}
	  }
	}
	
	private void removeKeyDelay(final Location key){
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.scheduleSyncDelayedTask(rpgskills, new Runnable() {
            @Override
            public void run() {
                drops.remove(key);
            }
        }, 10L);
	}
	
}
