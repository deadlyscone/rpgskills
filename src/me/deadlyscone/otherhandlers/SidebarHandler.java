package me.deadlyscone.otherhandlers;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import me.deadlyscone.main.RPGSkills;

public class SidebarHandler {
	
	
	public static void updatePlayer(Player player, String skill, boolean canBypass){
		if (RPGSkills.showStatsBoard.get(player.getUniqueId()) == null){
			RPGSkills.showStatsBoard.put(player.getUniqueId(), false);
		}
		if (RPGSkills.showStatsBoard.get(player.getUniqueId()) || canBypass){
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective objective = board.registerNewObjective(player.getName(), "dummy");
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		Double exp = ExperienceSystemHandler.getPlayerExperience(worldName, player.getUniqueId(), skill);
		int level = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), skill);
		HashMap<Integer, Double> levelExpMap = new HashMap<Integer, Double>(ExperienceSystemHandler.getBaseExpPerLevel(worldName, skill));
		Double remaining = level < RPGSkills.skillMaxLevel.get(worldName).get(skill) ? (levelExpMap.get(level+1)) - exp: 0.0;
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		objective.setDisplayName(ChatColor.DARK_AQUA + "------[" + ChatColor.YELLOW + skill.toUpperCase() + ChatColor.DARK_AQUA + "]------");
		
		Score info = objective.getScore(ChatColor.GRAY + "Level: " + ChatColor.YELLOW +  String.valueOf(level) + "/" + String.valueOf(RPGSkills.skillMaxLevel.get(worldName).get(skill)));
		info.setScore(4);
		
        info = objective.getScore(ChatColor.GRAY + "Experience: "+ ChatColor.YELLOW +  String.valueOf(exp.intValue()) + "xp");
        info.setScore(3);
        
        info = objective.getScore(ChatColor.GRAY + " ");
        info.setScore(2);
        
        info = objective.getScore(ChatColor.GRAY + "Remaining: "+ ChatColor.YELLOW + String.valueOf(remaining.intValue()) + "xp");
        info.setScore(1);
        
        if (skill.equals("magic")){
        info = objective.getScore(ChatColor.GRAY + "MP: "+ ChatColor.YELLOW + String.valueOf(ExperienceSystemHandler.getCurrentPlayerMP(player)) + "MP/" + String.valueOf(ExperienceSystemHandler.getMaxPlayerMP(worldName, player.getUniqueId())) + "MP");
        info.setScore(0);
        }
        
        player.setScoreboard(board);
		}
	}
	
	public static void showTargetStats(String worldName, UUID targetPlayerUUID, Player player){
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective objective = board.registerNewObjective(player.getName(), "dummy");
		String targetPlayerName = Bukkit.getOfflinePlayer(targetPlayerUUID).getName();
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		String targetName = (targetPlayerName.length()+14) > 32 ? targetPlayerName.substring(0, 14).concat("...") : targetPlayerName;
		System.out.println("LENGTH: "+targetName.length());
		objective.setDisplayName(ChatColor.DARK_AQUA + "------[" + ChatColor.YELLOW + targetName + ChatColor.DARK_AQUA + "]------");
        int i = RPGSkills.skills.length;
        Score info = objective.getScore(ChatColor.GRAY + "CombatLevel: " + ChatColor.GREEN + JoinLeaveHandler.getCombatLevel(worldName, targetPlayerUUID));
        info.setScore(i+3);
        info = objective.getScore(ChatColor.GRAY + "World: " + ChatColor.GREEN + worldName);
        info.setScore(i+2);
        info = objective.getScore(ChatColor.WHITE + "");
        info.setScore(i+1);
		for(String skill : RPGSkills.skills){
			int level = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, targetPlayerUUID, skill);
    		info = objective.getScore(ChatColor.GRAY + skill +": "+ ChatColor.YELLOW +  String.valueOf(level));
    		info.setScore(i);
    		i--;
        }
        
        player.setScoreboard(board);
	}

}
