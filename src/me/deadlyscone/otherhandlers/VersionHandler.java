package me.deadlyscone.otherhandlers;

import me.deadlyscone.main.RPGSkills;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class VersionHandler implements Listener{
	
	public VersionHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		Player player = e.getPlayer();
		if (player.hasPermission("rpgskills.version")){
			RPGSkills.checkVersion();
		String currentVersion = Bukkit.getServer().getPluginManager().getPlugin("RPGSkills").getDescription().getVersion();
		if (!currentVersion.equals(RPGSkills.newVersion)){
			player.sendMessage(ChatColor.BLUE + "________________________________");
			player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.RED + "Update available!");
			player.sendMessage(ChatColor.BLUE + "Current Version: " + currentVersion + " | New Version: " + RPGSkills.newVersion);
			player.sendMessage("Click this link for manual installation: http://dev.bukkit.org/bukkit-plugins/rpgskills/files/");
			player.sendMessage("Type " + ChatColor.RED + "/rs update " + ChatColor.RESET + "for automatic installation. Once done, all you have to do is restart.");
			player.sendMessage(ChatColor.BLUE + "________________________________");
		}
	  }
	}
}
