package me.deadlyscone.otherhandlers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.deadlyscone.main.RPGSkills;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaceHandler
  implements Listener
  {
  private static Material[] MiningBlocks = { Material.STONE, Material.NETHERRACK, Material.QUARTZ_ORE, Material.COAL_ORE, Material.IRON_ORE, 
    Material.GOLD_ORE, Material.REDSTONE_ORE, Material.LAPIS_ORE, Material.EMERALD_ORE, Material.DIAMOND_ORE };

  private static Material[] ExcavBlocks = { Material.SAND, Material.GRASS, Material.DIRT, Material.GRAVEL, Material.SOIL, 
    Material.SOUL_SAND, Material.CLAY, Material.MYCEL };

  private static Material[] WoodBlocks = { Material.LOG, Material.LOG_2 };

  private static Material[] FarmBlocks = { Material.MELON_BLOCK, Material.PUMPKIN, Material.SUGAR_CANE_BLOCK};

  public static List<Material> MiningMaterialsToCheck = Arrays.asList(MiningBlocks);
  public static List<Material> ExcavMaterialsToCheck = Arrays.asList(ExcavBlocks);
  public static List<Material> WoodcuttingMaterialsToCheck = Arrays.asList(WoodBlocks);
  public static List<Material> FarmingMaterialsToCheck = Arrays.asList(FarmBlocks);

  public static ArrayList<String> PlayerPlacedBlockLocations = new ArrayList<String>();

  public BlockPlaceHandler(RPGSkills plugin) {
    plugin.getServer().getPluginManager().registerEvents(this, plugin);
  }

  @EventHandler
  public void onBlockPlace(BlockPlaceEvent e) {
    Material blockPlaced = e.getBlock().getType();
    Location blockPlacedLoc = e.getBlock().getLocation();

    if (MiningMaterialsToCheck.contains(blockPlaced))
    {
      String loc = SerializeLocation(blockPlacedLoc);

      PlayerPlacedBlockLocations.add(loc);
    }

    if (ExcavMaterialsToCheck.contains(blockPlaced))
    {
      String loc = SerializeLocation(blockPlacedLoc);

      PlayerPlacedBlockLocations.add(loc);
    }

    if (FarmingMaterialsToCheck.contains(blockPlaced))
    {
      String loc = SerializeLocation(blockPlacedLoc);

      PlayerPlacedBlockLocations.add(loc);
    }

    if (WoodcuttingMaterialsToCheck.contains(blockPlaced))
    {
      String loc = SerializeLocation(blockPlacedLoc);

      PlayerPlacedBlockLocations.add(loc);
    }
  }

  public static boolean isBreakingPlayerPlacedBlock(Location BlockBreakLocation)
  {
    String coords = DeserializeLocation(BlockBreakLocation);

    if (PlayerPlacedBlockLocations.contains(coords)) {
      return true;
    }
    return false;
  }

  public static String DeserializeLocation(Location location) {
    String x = String.valueOf(location.getBlockX());
    String y = String.valueOf(location.getBlockY());
    String z = String.valueOf(location.getBlockZ());
    String world = location.getWorld().getName();
    String finalCoords = x + ":" + y + ":" + z + ":" + world;

    return finalCoords;
  }

  public static String SerializeLocation(Location location) {
    String x = String.valueOf(location.getBlockX());
    String y = String.valueOf(location.getBlockY());
    String z = String.valueOf(location.getBlockZ());
    String world = location.getWorld().getName();
    String finalCoords = x + ":" + y + ":" + z + ":" + world;

    return finalCoords;
  }
}