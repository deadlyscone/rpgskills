package me.deadlyscone.otherhandlers;

import java.util.UUID;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.MessageType;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.DisplaySlot;

public class JoinLeaveHandler implements Listener{
	
	public RPGSkills plugin;
	public JoinLeaveHandler(RPGSkills plugin){
	    plugin.getServer().getPluginManager().registerEvents(this, plugin);
	    this.plugin = plugin;
	}
	
	@EventHandler()
	public void onPlayerJoin(PlayerJoinEvent e){
		Player player = e.getPlayer();
		player.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		int combatLevel = getCombatLevel(worldName, player);
		String joinMessage = RPGSkills.MessageMap.get(MessageType.Join).replace("%player%", player.getName()).replace("%cblvl%", String.valueOf(combatLevel));
		if (RPGSkills.UseRPGSkillsPlayerListName){
			player.setPlayerListName(ChatColor.YELLOW + "[Level " + String.valueOf(combatLevel) + "] " + ChatColor.DARK_AQUA + player.getName());
		}
		if (RPGSkills.UseJoinLeave){
			//e.setJoinMessage(ChatColor.YELLOW + "[Level " + combatLevel + "] " + ChatColor.DARK_AQUA + player.getName() + ChatColor.GREEN + " logged on.");
			e.setJoinMessage(joinMessage);
		}
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e){
		Player player = e.getPlayer();
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		int combatLevel = getCombatLevel(worldName, player);
		String quitMessage = RPGSkills.MessageMap.get(MessageType.Leave).replace("%player%", player.getName()).replace("%cblvl%", String.valueOf(combatLevel));
		if (RPGSkills.UseJoinLeave){
			
		//e.setQuitMessage(ChatColor.YELLOW + "[Level " +  combatLevel + "] " + ChatColor.DARK_AQUA + player.getName() + ChatColor.DARK_RED + " logged off.");
			e.setQuitMessage(quitMessage);
		}
	}
	
	public static void updatePlayerPrefix(Player player){
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		int combatLevel = getCombatLevel(worldName, player);
		if (RPGSkills.UseRPGSkillsPlayerListName){
		player.setPlayerListName(ChatColor.YELLOW + "[Level " + String.valueOf(combatLevel) + "] " + ChatColor.DARK_AQUA + player.getName());
		}
	}
	
	public static int getCombatLevel(String worldName, UUID playerUUID){
		String[] combatSkills = {"attack", "archery", "magic", "unarmed"};
		int i = 0;
		for (String skill : combatSkills){
			i += ExperienceSystemHandler.convertPlayerExpToLevel(worldName, playerUUID, skill);
		}
		return i;
	}
	public static int getCombatLevel(String worldName, Player player){
		String[] combatSkills = {"attack", "archery", "magic", "unarmed"};
		int i = 0;
		for (String skill : combatSkills){
			i += ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), skill);
		}
		return i;
	}
	
	
}
