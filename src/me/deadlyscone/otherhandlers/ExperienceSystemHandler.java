package me.deadlyscone.otherhandlers;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.main.RPGSkills.MessageType;

public class ExperienceSystemHandler {
	
	public static HashMap<Integer, Double> getBaseExpPerLevel(String worldName, String skillName) {
		
		
		HashMap<Integer, Double> h = new HashMap<Integer, Double>();
		int MinLevel = RPGSkills.skillMinLevel.get(worldName).get(skillName);
		int MaxLevel = RPGSkills.skillMaxLevel.get(worldName).get(skillName);
		
		for (int i = MinLevel+1; i < MaxLevel + 1; i++){
			double factor = RPGSkills.skillGainFactor.get(worldName).get(skillName);
			double nextLevel = i + 1;
			double expReqforLevel = i* Math.round((Math.pow(nextLevel, factor)));
			h.put(i, expReqforLevel);
		}
		return h;
	}
	
	public static double getPlayerExperience(String worldName, UUID playerUUID, String skillName) {
		
		
		int MinLevel = RPGSkills.skillMinLevel.get(worldName).get(skillName);
		try{
			if (RPGSkills.worldExp.get(worldName).get(playerUUID).get(skillName) == null){
				RPGSkills.worldExp.get(worldName).get(playerUUID).put(skillName, getBaseExpPerLevel(worldName, skillName).get(MinLevel));
			}else{
				return RPGSkills.worldExp.get(worldName).get(playerUUID).get(skillName);
			}
		}catch(NullPointerException npe){
			((Player) Bukkit.getOfflinePlayer(playerUUID)).sendMessage(ChatColor.RED + "[RPGSkills] Error encountered, please re-login.");
		}
		return MinLevel;
	}
	
	public static int convertPlayerExpToLevel(String worldName, UUID playerUUID, String skillName){
		
		int MinLevel = RPGSkills.skillMinLevel.get(worldName).get(skillName);
		int MaxLevel = RPGSkills.skillMaxLevel.get(worldName).get(skillName);
		double currentExp = getPlayerExperience(worldName, playerUUID, skillName);
		
		if (currentExp >= getBaseExpPerLevel(worldName, skillName).get(MaxLevel)){
			return MaxLevel;
			
		}else if (currentExp != getBaseExpPerLevel(worldName, skillName).get(MaxLevel) && currentExp < getBaseExpPerLevel(worldName, skillName).get(MaxLevel)){
			for(int i = MinLevel; i < (MaxLevel + 1); i++){
				if (currentExp < getBaseExpPerLevel(worldName, skillName).get(i+1)){
					return i;
				}
			}
			
		}
		return MinLevel;
		
		
	}
	
	@SuppressWarnings("deprecation")
	public static void CheckIfPlayerLeveled(String worldName, UUID playerUUID, int oldLevel, String skillName) {
		
		int MaxLevel = RPGSkills.skillMaxLevel.get(worldName).get(skillName);
		int currentLevel = convertPlayerExpToLevel(worldName, playerUUID, skillName);
		Player player = (Player) Bukkit.getOfflinePlayer(playerUUID);
			SidebarHandler.updatePlayer(player, skillName, false);
			String maxLevelMessage = RPGSkills.MessageMap.get(MessageType.MaxLevelAchieve).replace("%player%", player.getName()).replace("%sklvl%", String.valueOf(currentLevel)).replace("%skill%", skillName.toUpperCase());
			String levelUpMessage = RPGSkills.MessageMap.get(MessageType.LevelUp).replace("%sklvl%", String.valueOf(currentLevel)).replace("%skill%", skillName.toUpperCase());
			
		//this means we leveled up
		if (currentLevel > oldLevel){
			
			//check if old level is 99
			if (oldLevel == MaxLevel - 1){
				//level 100 celebration.
				for (Player p : Bukkit.getServer().getOnlinePlayers()){
					if (p != player){
						p.sendMessage(maxLevelMessage);
					}else if (p == player){
						player.sendMessage(levelUpMessage);
						player.getWorld().playSound(player.getLocation(), Sound.ENTITY_ZOMBIE_VILLAGER_CURE, 1f, 1f);
						player.playEffect(player.getLocation(), Effect.POTION_BREAK, 0);
						//update the level above the damagers head
						JoinLeaveHandler.updatePlayerPrefix(player);
					}
				}
				
				//check if skill was magic and add the gained MP
				if (skillName.equals("magic")){
					setTotalPlayerMP(player);
				}
				
				//if player is not leveling to 100
			}else{
				player.sendMessage(levelUpMessage);
				player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_BLAST, 1f, 0.9f);
				//update the level above the damagers head
				JoinLeaveHandler.updatePlayerPrefix(player);
				//check if skill was magic and add the gained MP
				if (skillName.equals("magic")){
					setTotalPlayerMP(player);
				}
			}
			
		}else if (currentLevel == oldLevel){
			
		}
		
	}
	
	public static void setPlayerExp(String worldName, UUID playerUUID, String skillName, double exp){
		if(RPGSkills.worldExp.get(worldName).get(playerUUID) == null){
			RPGSkills.worldExp.get(worldName).put(playerUUID, new HashMap<String, Double>());
		}
		RPGSkills.worldExp.get(worldName).get(playerUUID).put(skillName, exp);
		
	}
	
	// SOME MAGIC POINT GETTER AND SETTER METHODS \\
	
	public static int getCurrentPlayerMP(Player player){
		UUID playerUUID = player.getUniqueId();
		String worldName = checkActiveWorld(player.getWorld().getName());
		//populate the player mp maps first
		if (RPGSkills.playerMagicPoints.get(worldName).get(playerUUID) == null){
			RPGSkills.playerMagicPoints.get(worldName).put(playerUUID, (Integer)RPGSkills.configData.get(worldName).get(ConfigType.MAGICBASEMP).get(0));
			return (Integer) RPGSkills.configData.get(worldName).get(ConfigType.MAGICBASEMP).get(0);
		}else{
			return RPGSkills.playerMagicPoints.get(worldName).get(playerUUID);
		}
	}
	
	//this gets called when a player levels up. 
	private static void setTotalPlayerMP(Player player){
		// THIS IS GOING TO BE AN ISSUE
		String worldName = checkActiveWorld(player.getWorld().getName());
		int currentMP = getCurrentPlayerMP(player);
		int gainedMP =  (Integer)RPGSkills.configData.get(worldName).get(ConfigType.MAGICGAINMP).get(0);
		UUID playerUUID = player.getUniqueId();
		
		RPGSkills.playerMagicPoints.get(worldName).put(playerUUID, (currentMP + gainedMP));
	}
	
	public static int getMaxPlayerMP(String worldName, UUID playerUUID){
		int baseMP = (Integer)RPGSkills.configData.get(worldName).get(ConfigType.MAGICBASEMP).get(0);
		int mpGain = (Integer)RPGSkills.configData.get(worldName).get(ConfigType.MAGICGAINMP).get(0);
		int currentLevel = convertPlayerExpToLevel(worldName, playerUUID, "magic");
		
		return (baseMP + (mpGain * currentLevel));
		
	}
	
	public static String checkActiveWorld(String worldName){
		if (RPGSkills.ActiveWorldNames.contains(worldName)){
			
			int index = RPGSkills.ActiveWorldNames.indexOf(worldName);
			String[] data = RPGSkills.ActiveWorldsData.get(index).split(",");
			String childWorld = data[0];
			String parentWorld = data[1];
				if (!RPGSkills.ActiveWorldNames.contains(parentWorld) && parentWorld.equals("-")){
					return childWorld;
				}else if (RPGSkills.ActiveWorldNames.contains(parentWorld) && !parentWorld.equals("-")){
					return parentWorld;
				}else{
					System.out.println("[RPGSkills] Error, '"+parentWorld+"' cannot be used if it is not active. Please add to worlds.yml");
				}
			
		}
		return worldName;
	}
	
}
