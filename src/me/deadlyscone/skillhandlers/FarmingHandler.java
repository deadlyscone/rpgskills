package me.deadlyscone.skillhandlers;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.BlockPlaceHandler;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class FarmingHandler implements Listener{

	private final String ThisSkill = "farming";
	private Material[] ingb = {Material.MELON_BLOCK, Material.PUMPKIN, Material.SUGAR_CANE_BLOCK, Material.NETHER_WARTS, 
			Material.BROWN_MUSHROOM, Material.RED_MUSHROOM, Material.CACTUS};
	private List<Material> isNonGrowthBlock = Arrays.asList(ingb);
	Plugin rpgskills;

	public FarmingHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		rpgskills = plugin;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onFarm(BlockBreakEvent e){
		Player player = e.getPlayer();
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && player.getGameMode() != GameMode.CREATIVE && RPGSkills.ActiveWorldNames.contains(worldName)
				&& (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && !e.isCancelled()){
			
			Material materialBlock = e.getBlock().getType();
			Material materialHand = player.getItemInHand().getType();
			
			if (RPGSkills.configData.get(worldName).get(ConfigType.FARMINGBLOCKSMAP).containsKey(materialBlock) && !e.isCancelled()){
				
				//Variables
				double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, player.getUniqueId(), ThisSkill);
				int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
				int ExpGainedFromFarming = (int) RPGSkills.configData.get(worldName).get(ConfigType.FARMINGBLOCKSMAP).get(materialBlock);
				Location location = e.getBlock().getLocation();
				
				if (!BlockPlaceHandler.isBreakingPlayerPlacedBlock(location)){
					if (!isNonGrowthBlock.contains(materialBlock) && isFullGrownCrop(e.getBlock())){
						//give player his experience
						checkChanceEvents(worldName, player, currentLevel, e.getBlock(), materialHand);
						RPGSkills.worldExp.get(worldName).get(player.getUniqueId()).put(ThisSkill, (currentExp + ExpGainedFromFarming));
						//RPGSkills.playerFarmingExp.put(playerName, (currentExp + ExpGainedFromFarming));
						ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, player.getUniqueId(), currentLevel, ThisSkill);
					}else if (isNonGrowthBlock.contains(materialBlock)){
						//give player his experience
						checkChanceEvents(worldName, player, currentLevel, e.getBlock(), materialHand);
						RPGSkills.worldExp.get(worldName).get(player.getUniqueId()).put(ThisSkill, (currentExp + ExpGainedFromFarming));
						//RPGSkills.playerFarmingExp.put(playerName, (currentExp + ExpGainedFromFarming));
						ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, player.getUniqueId(), currentLevel, ThisSkill);
					}
				}
				
				
			}
			
		}
	}
	
	private void checkChanceEvents(String worldName, Player player,int currentLevel, Block block, Material hand){
		//check auto re-plant

		Material[] ab = {Material.CARROT, Material.POTATO, Material.COCOA, Material.CROPS};
		List<Material> appBlocks = Arrays.asList(ab);
		Random r = new Random();
		
		double ddc = (appBlocks.contains(block.getType()) && RPGSkills.configData.get(worldName).get(ConfigType.FARMINGTOOLMODIFIERS).containsKey(hand)) ? ((double)RPGSkills.configData.get(worldName).get(ConfigType.FARMINGDOUBLECHANCE).get(0) * currentLevel) + 
				(double)RPGSkills.configData.get(worldName).get(ConfigType.FARMINGTOOLMODIFIERS).get(hand) : (double)RPGSkills.configData.get(worldName).get(ConfigType.FARMINGDOUBLECHANCE).get(0) * currentLevel;
		
		double tdc = (appBlocks.contains(block.getType()) && RPGSkills.configData.get(worldName).get(ConfigType.FARMINGTOOLMODIFIERS).containsKey(hand)) ? ((double)RPGSkills.configData.get(worldName).get(ConfigType.FARMINGTRIPLECHANCE).get(0) * currentLevel) + 
				(double)RPGSkills.configData.get(worldName).get(ConfigType.FARMINGTOOLMODIFIERS).get(hand) : (double)RPGSkills.configData.get(worldName).get(ConfigType.FARMINGTRIPLECHANCE).get(0) * currentLevel;
		
		double chance = r.nextDouble()*100;
		
		
		//Double and Triple Chance Events**
		if ((boolean)RPGSkills.configData.get(worldName).get(ConfigType.FARMINGTRIPLEDROPENABLED).get(0) && chance <= tdc){
			ItemStack[] itemDropped = block.getDrops().toArray(new ItemStack[block.getDrops().size()]);
				block.getWorld().dropItemNaturally(block.getLocation(), itemDropped[0]);
				block.getWorld().dropItemNaturally(block.getLocation(), itemDropped[0]);
		}else if (chance <= ddc){
			ItemStack[] itemDropped = block.getDrops().toArray(new ItemStack[block.getDrops().size()]);
				block.getWorld().dropItemNaturally(block.getLocation(), itemDropped[0]);
		}
		
		//Auto Re-Plant Chance Events
		chance = r.nextDouble()*100;
		
		double arc = (appBlocks.contains(block.getType()) && RPGSkills.configData.get(worldName).get(ConfigType.FARMINGTOOLMODIFIERS).containsKey(hand)) ? ((double)RPGSkills.configData.get(worldName).get(ConfigType.FARMINGREPLANTCHANCE).get(0) * currentLevel) + 
				(double)RPGSkills.configData.get(worldName).get(ConfigType.FARMINGTOOLMODIFIERS).get(hand) : (double)RPGSkills.configData.get(worldName).get(ConfigType.FARMINGREPLANTCHANCE).get(0) * currentLevel;
				
		if ((boolean)RPGSkills.configData.get(worldName).get(ConfigType.FARMINGAUTOREPLANTENABLED).get(0) && chance <= arc){
			switch (block.getType()){
			
			case CROPS:
				//7
				replantDelay(block.getLocation(), block.getType(), 7);
				break;
				
			case CARROT:
				//3
				replantDelay(block.getLocation(), block.getType(), 3);
				break;
				
			case POTATO:
				//3
				replantDelay(block.getLocation(), block.getType(), 3);
				break;
			case NETHER_WARTS:
				//3
				replantDelay(block.getLocation(), block.getType(), 3);
				break;
				
				
				default:
					break;
			}
		}
		
	}
	
	private void replantDelay(final Location key, final Material type, final int growth){
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.scheduleSyncDelayedTask(rpgskills, new Runnable() {
            @SuppressWarnings("deprecation")
			@Override
            public void run() {
                Random r = new Random();
                int rGrowth = r.nextInt(growth);
                
                key.getWorld().getBlockAt(key).setType(type);
                key.getWorld().getBlockAt(key).setData((byte)rGrowth);
            }
        }, 10L);
	}
	
	@SuppressWarnings("deprecation")
	private boolean isFullGrownCrop(Block b){
		switch (b.getType()){
		
		case CROPS:
			if (b.getData() >= 7){
				return true;
			}else{
				return false;
			}
		case COCOA:
			if (b.getData() >= 2){
				return true;
			}else{
				return false;
			}
		case CARROT:
			if (b.getData() >= 3){
				return true;
			}else{
				return false;
			}
		case NETHER_WARTS:
			if (b.getData() >= 3){
				return true;
			}else{
				return false;
			}
		case POTATO:
			if (b.getData() >= 3){
				return true;
			}else{
				return false;
			}
		case MELON_STEM:
			if (b.getData() >= 7){
				return true;
			}else{
				return false;
			}
		case PUMPKIN_STEM:
			if (b.getData() >= 7){
				return true;
			}else{
				return false;
			}
			
			default:
				return false;
		}
	}
}
