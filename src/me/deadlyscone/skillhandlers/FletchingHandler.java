package me.deadlyscone.skillhandlers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import me.deadlyscone.enums.ArrowType;
import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class FletchingHandler implements Listener{
	
	private final String ThisSkill = "fletching";
	
	public FletchingHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onCraftItemEvent(CraftItemEvent e){
	    Material item = e.getCurrentItem().getType();
	    
		if (item == Material.ARROW  && e.getWhoClicked() instanceof Player){
			Player player = (Player) e.getWhoClicked();
			String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
			if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName)
					&& (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE && !e.isCancelled()){
				String arrowType = e.getCurrentItem().getItemMeta().getDisplayName() == null ? "Generic" : e.getCurrentItem().getItemMeta().getDisplayName().split(" ")[0];
				boolean isSpecialArrow = Arrays.asList(ArrowType.values()).contains(ArrowType.valueOf(arrowType.toUpperCase()));
			
				double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, player.getUniqueId(), ThisSkill);
			int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
			String[] fletchingData = ((String) RPGSkills.configData.get(worldName).get(ConfigType.FLETCHINGARROWDATA).get(ArrowType.valueOf(arrowType.toUpperCase()))).split(",");
			int levelRequiredtoFletch = Integer.valueOf(fletchingData[0]);
			boolean canFletch = currentLevel >= levelRequiredtoFletch;
			int ExpGainedFromFletching = Integer.valueOf(fletchingData[1]);
			
			System.out.println(levelRequiredtoFletch);
			if (isSpecialArrow && canFletch){
				
				if (e.isShiftClick()){
					int resultStacks = getNumberOfArrows(e.getInventory());
					int extraArrows = getExtraArrows(worldName, currentLevel, player);
					
					for (int i = 0; i < resultStacks; i++){
						extraArrows += getExtraArrows(worldName, currentLevel, player);
						RPGSkills.worldExp.get(worldName).get(player.getUniqueId()).put(ThisSkill, (currentExp + ExpGainedFromFletching));
						//RPGSkills.playerFletchingExp.put(playerName, (currentExp + ExpGainedFromFletching));
					}
					ItemStack extraItemArrows = new ItemStack(Material.ARROW, extraArrows);
					ItemMeta eiaMeta = extraItemArrows.getItemMeta();
					eiaMeta.setDisplayName(arrowType + " Arrow");
					extraItemArrows.setItemMeta(eiaMeta);
					player.getInventory().addItem(extraItemArrows);
					ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, player.getUniqueId(), currentLevel, ThisSkill);
					
					//if player didn't shift click
				}else if (e.isRightClick() || e.isLeftClick()){
					int extraArrows = getExtraArrows(worldName, currentLevel, player);
					int maxItemSize = e.getInventory().getResult().getMaxStackSize();
					int yield = e.getCurrentItem().getAmount();
					
					if (e.getCursor().getAmount() + (yield + extraArrows) <= maxItemSize){
						e.getInventory().getResult().setAmount(e.getInventory().getResult().getAmount() + extraArrows);
						
						//give player his experience
						RPGSkills.worldExp.get(worldName).get(player.getUniqueId()).put(ThisSkill, (currentExp + ExpGainedFromFletching));
						ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, player.getUniqueId(), currentLevel, ThisSkill);
						
					}else if (e.getCursor().getAmount() + (yield + extraArrows) > maxItemSize){
						e.setCancelled(true);
						//TRy CLOSING THE INVENTORY HERE!
						player.sendMessage(ChatColor.RED + "Please empty your cursor!");
						
					}
					
				}
			}else if(!canFletch){
				player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(levelRequiredtoFletch) + " to fletch this " + arrowType + " arrow.");
				e.setCancelled(true);
				player.closeInventory();
			}

			
		}
		}
	}

	
	private int getExtraArrows(String worldName, int currentLevel, Player player){
		Random r = new Random();
		int chance = r.nextInt(100);
		int levelBased = r.nextInt((int) (currentLevel * 0.05));
		int NlevelBased = levelBased >= 1 ? levelBased : 1;
		int extraArrows = r.nextInt((int) RPGSkills.configData.get(worldName).get(ConfigType.FLETCHINGBASEYIELD).get(0)) + NlevelBased;

		  if (chance <= currentLevel){
			  return extraArrows;
		  }
		  
		  //returns none if the chance was unlucky
		return 0;
	}
	private int getNumberOfArrows(CraftingInventory inv) {
		ArrayList<Integer> minNumber = new ArrayList<Integer>();
		for (int i = 1; i < 9; i++){
			if (inv.getItem(i) != null){
				minNumber.add(inv.getItem(i).getAmount());

			}

		}
		
		Collections.sort(minNumber);
		
		return minNumber.get(0);
	}

}
