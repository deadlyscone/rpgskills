package me.deadlyscone.skillhandlers;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class SummoningHandler implements Listener{
	private final String ThisSkill = "summoning";
	EntityType[] MobTypes = {EntityType.CHICKEN, EntityType.COW, EntityType.HORSE, EntityType.MUSHROOM_COW, EntityType.OCELOT, EntityType.PIG, EntityType.RABBIT, EntityType.WOLF};
	
	public SummoningHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerItemSpawn(PlayerInteractEvent e){
		Player player = e.getPlayer();
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName)
				&& (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE && !e.isCancelled()){
		EntityType summoned = getSummoned(worldName, player.getItemInHand().getType());
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK && player.isSneaking() && player.getItemInHand() != null && summoned != null){
			
			Material material = Material.getMaterial(Integer.valueOf(((String) RPGSkills.configData.get(worldName).get(ConfigType.SUMMONINGPROPERTYDATA).get(summoned)).split(",")[0]));
			int amountReq = Integer.valueOf(((String) RPGSkills.configData.get(worldName).get(ConfigType.SUMMONINGPROPERTYDATA).get(summoned)).split(",")[1]);
			int summonedAmount = Integer.valueOf(((String) RPGSkills.configData.get(worldName).get(ConfigType.SUMMONINGPROPERTYDATA).get(summoned)).split(",")[2]);
			int levelRequired = Integer.valueOf(((String) RPGSkills.configData.get(worldName).get(ConfigType.SUMMONINGPROPERTYDATA).get(summoned)).split(",")[3]);
			int ExpGainedFromSummoning = Integer.valueOf(((String) RPGSkills.configData.get(worldName).get(ConfigType.SUMMONINGPROPERTYDATA).get(summoned)).split(",")[4]);
			int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
			double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, player.getUniqueId(), ThisSkill);
			
			if (player.getItemInHand().getType() == material && player.getItemInHand().getAmount() >= amountReq && currentLevel >= levelRequired){
				player.setItemInHand(new ItemStack(player.getItemInHand().getType(), player.getItemInHand().getAmount() - amountReq));
				for (int i=0;i<summonedAmount;i++){
					
					LivingEntity le = (LivingEntity)player.getWorld().spawnEntity(e.getClickedBlock().getLocation().add(0, 1, 0), summoned);
					switch(summoned){
					case OCELOT:
						if (le instanceof Tameable){
							Tameable t = (Tameable)le;
							t.setOwner((AnimalTamer)player);
						}
					case WOLF:
						if (le instanceof Tameable){
							Tameable t = (Tameable)le;
							t.setOwner((AnimalTamer)player);
						}
					default:
						break;
				}
				RPGSkills.worldExp.get(worldName).get(player.getUniqueId()).put(ThisSkill, (currentExp + ExpGainedFromSummoning));
				//RPGSkills.playerSummoningExp.put(player.getName(), (currentExp + ExpGainedFromSummoning));
				ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, player.getUniqueId(), currentLevel, ThisSkill);
			}
		  }else if (currentLevel < levelRequired){
				player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(levelRequired) + " to summon " + summoned.toString() + ".");
		  }
		}
	  }
	}
	
	@SuppressWarnings("deprecation")
	private EntityType getSummoned(String worldName, Material m){
		for (EntityType s : MobTypes){
			String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.SUMMONINGPROPERTYDATA).get(s)).split(",");
			Material dataM = Material.getMaterial(Integer.valueOf(data[0]));
			if (m == dataM){
				return s;
			}
			
		}
		return null;
	}
}
