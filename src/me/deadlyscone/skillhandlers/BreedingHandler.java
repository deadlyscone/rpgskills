package me.deadlyscone.skillhandlers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class BreedingHandler implements Listener{
	private final String ThisSkill = "breeding";
	
	public BreedingHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onCreatureSpawnFromBreeding(CreatureSpawnEvent e){
		if (e.getSpawnReason() == SpawnReason.BREEDING){
			List<Entity> nearByEnt = e.getEntity().getNearbyEntities(0.8, 0.5, 0.8);
			List<UUID> playerOfParents = new ArrayList<UUID>();
			UUID player = null;
			String worldName = ExperienceSystemHandler.checkActiveWorld(e.getEntity().getWorld().getName());
			boolean isSame = false;
			int ExpGainedFromBreeding = Integer.valueOf(((String) RPGSkills.configData.get(worldName).get(ConfigType.BREEDINGMOBTYPEDATA).get(e.getEntityType())).split(",")[2]);
			
			for (int i = 0; i < nearByEnt.size();i++){
				if (nearByEnt.size() >= 1 && RPGSkills.BreedingEventData.containsKey(nearByEnt.get(i).getUniqueId())){
					player = RPGSkills.BreedingEventData.get(nearByEnt.get(i).getUniqueId());
					isSame = nearByEnt.get(i).getType() == e.getEntity().getType() ? true : false;
					playerOfParents.add(player);
					RPGSkills.BreedingEventData.remove(nearByEnt.get(i).getUniqueId());
				}
			}
			
			int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player, ThisSkill);
			double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, player, ThisSkill);
			if (playerOfParents.size() == 2 && playerOfParents.get(0).equals(playerOfParents.get(1)) && isSame){
				int gec = getExtraChildren(worldName, e.getEntityType(), currentLevel);
				
				for (int i = 0; i < gec;i++){
					Ageable extraChild = (Ageable) e.getEntity().getWorld().spawnEntity(e.getLocation(), e.getEntityType());
					extraChild.setBaby();
				}
				RPGSkills.worldExp.get(worldName).get(player).put(ThisSkill, currentExp + ExpGainedFromBreeding);
				//RPGSkills.playerBreedingExp.put(player, (currentExp + ExpGainedFromBreeding));
				ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, player, currentLevel, ThisSkill);
			}
			
		}
	}
	
	@EventHandler
	public void onPlayerBreedAttempt(PlayerInteractEntityEvent e){
		Player player = e.getPlayer();
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName)
				&& (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE && !e.isCancelled()){
		if (e.getRightClicked() instanceof Ageable){
			EntityType entityType = e.getRightClicked().getType();
			Ageable ageable = (Ageable)e.getRightClicked();
			if (ageable.canBreed()){
			switch (entityType){
			
			case COW:
				if (player.getItemInHand().getType() == Material.WHEAT){
					RPGSkills.BreedingEventData.put(e.getRightClicked().getUniqueId(), player.getUniqueId());
				}
				break;
			case CHICKEN:
				if (player.getItemInHand().getType() == Material.SEEDS){
					RPGSkills.BreedingEventData.put(e.getRightClicked().getUniqueId(), player.getUniqueId());
				}
				break;
			case HORSE:{
				Tameable tame = (Tameable)e.getRightClicked();
				if ((player.getItemInHand().getType() == Material.GOLDEN_APPLE || player.getItemInHand().getType() == Material.GOLDEN_CARROT) && tame.isTamed()){
					RPGSkills.BreedingEventData.put(e.getRightClicked().getUniqueId(), player.getUniqueId());
				}
				break;
			}
			case MUSHROOM_COW:
				if (player.getItemInHand().getType() == Material.WHEAT){
					RPGSkills.BreedingEventData.put(e.getRightClicked().getUniqueId(), player.getUniqueId());
				}
				break;
			case OCELOT:{
				Tameable tame = (Tameable)e.getRightClicked();
				if (player.getItemInHand().getType() == Material.RAW_FISH && tame.isTamed()){
					RPGSkills.BreedingEventData.put(e.getRightClicked().getUniqueId(), player.getUniqueId());
				}
				break;
			}
			case PIG:
				if (player.getItemInHand().getType() == Material.CARROT){
					RPGSkills.BreedingEventData.put(e.getRightClicked().getUniqueId(), player.getUniqueId());
				}
				break;
					
			case RABBIT:
				if (player.getItemInHand().getType() == Material.YELLOW_FLOWER || player.getItemInHand().getType() == Material.CARROT || player.getItemInHand().getType() == Material.GOLDEN_CARROT){
					RPGSkills.BreedingEventData.put(e.getRightClicked().getUniqueId(), player.getUniqueId());
				}
				break;
			case SHEEP:
				if (player.getItemInHand().getType() == Material.WHEAT){
					RPGSkills.BreedingEventData.put(e.getRightClicked().getUniqueId(), player.getUniqueId());
				}
				break;
			case WOLF:{
				Tameable tame = (Tameable)e.getRightClicked();
				if (isFood(player.getItemInHand().getType()) && tame.isTamed()){
					RPGSkills.BreedingEventData.put(e.getRightClicked().getUniqueId(), player.getUniqueId());
				}
				break;
			}
			default:
				break;
			}
		  }
		}
	  }
	}
	
	
	private int getExtraChildren(String worldName, EntityType entType, int currentLevel){
		
		int extraChildren = Integer.valueOf(((String) RPGSkills.configData.get(worldName).get(ConfigType.BREEDINGMOBTYPEDATA).get(entType)).split(",")[0]);
		int baseChance = Integer.valueOf(((String) RPGSkills.configData.get(worldName).get(ConfigType.BREEDINGMOBTYPEDATA).get(entType)).split(",")[1]);
		Random r = new Random();
		int c = (baseChance + (currentLevel / 2));
		int chance = r.nextInt(100);
		
		if (chance > c){
			return 0;
		}else{
			return r.nextInt(extraChildren)+1;
		}
	}
	
	private boolean isFood(Material item){
		Material[] foods = {Material.RAW_BEEF, Material.RAW_CHICKEN, Material.RAW_FISH, Material.RABBIT, 
				Material.PORK, Material.MUTTON, Material.COOKED_BEEF, Material.COOKED_CHICKEN, 
				Material.COOKED_MUTTON, Material.GRILLED_PORK, Material.COOKED_RABBIT};
		for (Material m : foods){
			if (item == m){
				return true;
			}
		}
		return false;
	}
	
}
