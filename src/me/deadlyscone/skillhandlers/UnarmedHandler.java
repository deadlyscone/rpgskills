package me.deadlyscone.skillhandlers;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.GameMode;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class UnarmedHandler{
	private final static String ThisSkill = "unarmed";

	public static double onPlayerPunch(Player player, Entity ent){
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		if ((RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName)
				&& (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms)) && player.getGameMode() != GameMode.CREATIVE){
				int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
				double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, player.getUniqueId(), ThisSkill);
				int ExpGainedFromAttacking;
				
				if (ent instanceof Player){
					ExpGainedFromAttacking = (int) RPGSkills.configData.get(worldName).get(ConfigType.UNARMEDEXPPERENTITY).get("Player");
				}else if (ent instanceof Creature){
					ExpGainedFromAttacking = (int) RPGSkills.configData.get(worldName).get(ConfigType.UNARMEDEXPPERENTITY).get("Mob");
				}else{
					return 0.0;
				}
				
					double appliedDamage = ((double)RPGSkills.configData.get(worldName).get(ConfigType.UNARMEDGLOBALMODIFIER).get(0));
					RPGSkills.worldExp.get(worldName).get(player.getUniqueId()).put(ThisSkill, (currentExp + ExpGainedFromAttacking));
					//RPGSkills.playerUnarmedExp.put(player.getName(), (currentExp + ExpGainedFromAttacking));
					ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, player.getUniqueId(), currentLevel, ThisSkill);
					return appliedDamage;

		}else{
			return 0.0;
		}
	}
}
