package me.deadlyscone.skillhandlers;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.BlockPlaceHandler;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class ExcavationHandler implements Listener{

	
	
	private final String ThisSkill = "excavation";

	public ExcavationHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onExcav(BlockBreakEvent e){
		Player player = e.getPlayer();
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName)
				&& (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE && !e.isCancelled()){
			UUID playerUUID = player.getUniqueId();
			Material materialHand = player.getItemInHand().getType();
			Material materialBlock = e.getBlock().getType();
			double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, player.getUniqueId(), ThisSkill);
			int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
			
			if (isSpade(materialHand) && RPGSkills.configData.get(worldName).get(ConfigType.EXCAVBLOCKSMAP).containsKey(materialBlock) &&
					//RPGSkills.configData.get(worldName).get(ConfigType.EXCAVTREASURES)
					currentLevel >= (int)RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(materialHand) && !e.isCancelled()){
				
				int ExpGainedFromDigging = Integer.valueOf((String) RPGSkills.configData.get(worldName).get(ConfigType.EXCAVBLOCKSMAP).get(materialBlock));
				Location location = e.getBlock().getLocation();
				String coords = BlockPlaceHandler.DeserializeLocation(location);
				
				if (!BlockPlaceHandler.isBreakingPlayerPlacedBlock(location)){
					
					//are we allowing treasures
					if ((boolean) RPGSkills.configData.get(worldName).get(ConfigType.EXCAVALLOWTREASURES).get(0)){
						ItemStack item = checkTreasure(worldName, currentLevel);
						if (item != null){
							location.getWorld().dropItemNaturally(location, item);
						}
					}
					
					//give player his experience
					RPGSkills.worldExp.get(worldName).get(playerUUID).put(ThisSkill, (currentExp + ExpGainedFromDigging));
					//RPGSkills.playerExcavationExp.put(playerName, (currentExp + ExpGainedFromDigging));
					ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, player.getUniqueId(), currentLevel, ThisSkill);
					
				}else if (BlockPlaceHandler.isBreakingPlayerPlacedBlock(location)){
					
					BlockPlaceHandler.PlayerPlacedBlockLocations.remove(coords);
				}
				
				if (materialBlock == Material.SOIL && (boolean)RPGSkills.configData.get(worldName).get(ConfigType.EXCAVSILKFARMLAND).get(0) && player.getItemInHand().containsEnchantment(Enchantment.SILK_TOUCH)){
					e.getBlock().setType(Material.AIR);
					location.getWorld().dropItemNaturally(location, new ItemStack(Material.SOIL, 1));
				}
				
			}else if(isSpade(materialHand) && currentLevel < (int)RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(materialHand)){
				player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(materialHand)) + " to use this spade.");
				e.setCancelled(true);
			}
			
		}
	}

	private boolean isSpade(Material m) {
		switch (m){
		
		case WOOD_SPADE:
			return true;
		case STONE_SPADE:
			return true;
		case IRON_SPADE:
			return true;
		case GOLD_SPADE:
			return true;
		case DIAMOND_SPADE:
			return true;
			
		default:
			return false;
		}
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	private ItemStack checkTreasure(String worldName, int currentLevel){
		//usage: (material ID),(amount),(meta),(chance)
		ArrayList<String> list = (ArrayList<String>) RPGSkills.configData.get(worldName).get(ConfigType.EXCAVTREASURES).get(0);
		Random r = new Random();
		String[] itemList = list.get(r.nextInt(list.size())).split(",");
		ItemStack item = new ItemStack(Material.getMaterial(Integer.valueOf(itemList[0])), Integer.valueOf(itemList[1]), Short.valueOf(itemList[2]));
		
		int chance = r.nextInt(500);
		long c = Integer.valueOf(itemList[3]) + Math.round(currentLevel * 0.5);
		if (chance > c){
			return null;
		}else if (chance < c){
			return item;
		}
		return null;
	}
}
