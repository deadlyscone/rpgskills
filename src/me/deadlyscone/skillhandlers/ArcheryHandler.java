package me.deadlyscone.skillhandlers;

import me.deadlyscone.enums.ArrowEffect;
import me.deadlyscone.enums.ArrowType;
import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.ArrowEffectHandler;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

public class ArcheryHandler implements Listener{

	private final String ThisSkill = "archery";
	Plugin plugin;
	
	public ArcheryHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityHitByArrow(EntityDamageByEntityEvent e){
			
			// Make sure the entity is living & the damage was from an arrow.
			if (e.getDamager() instanceof Arrow && e.getEntity() instanceof LivingEntity && !e.isCancelled()){
				Arrow arrow = (Arrow)e.getDamager();

				
				if (arrow.getShooter() instanceof Player){
					Player shooter = (Player) arrow.getShooter();
					String worldName = ExperienceSystemHandler.checkActiveWorld(shooter.getWorld().getName());
					if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName)
							&& (shooter.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && shooter.getGameMode() != GameMode.CREATIVE){
						
						double arrowDamage = getArrowDamage(arrow);
						ArrowEffect effect = getArrowEffect(arrow);
						
					double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, shooter.getUniqueId(), ThisSkill);
					int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName,shooter.getUniqueId(), ThisSkill);
					//double bowDamage = (double)RPGSkills.configData.get(worldName).get(ConfigType.ARCHERYBOWDAMAGE).get(0);
					double appliedDamage = (-e.getDamage() + arrowDamage) + (currentLevel * ((double)RPGSkills.configData.get(worldName).get(ConfigType.ARCHERYGLOBALMODIFIER).get(0) / 2));
					int expGained = e.getEntity() instanceof Player ? (int) RPGSkills.configData.get(worldName).get(ConfigType.ARCHERYEXPPERENTITY).get("Player") 
							: (int)RPGSkills.configData.get(worldName).get(ConfigType.ARCHERYEXPPERENTITY).get("Mob");
					
					
					//set the damage and apply arrow effect
					ArrowEffectHandler.applyEffect(effect, (LivingEntity)e.getEntity());
					e.setDamage(appliedDamage);
					
					
					//give shooter his experience
					RPGSkills.worldExp.get(worldName).get(shooter.getUniqueId()).put(ThisSkill, (currentExp + expGained));
					ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, shooter.getUniqueId(), currentLevel, ThisSkill);
					}
				}
			}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEntityShootBow(EntityShootBowEvent e){
		
		if (e.getProjectile() instanceof Arrow && e.getEntity() instanceof Player && !e.isCancelled()){
			Player player = (Player)e.getEntity();
			String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
			
			if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName)
					&& (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE){
				
				Vector velocity = e.getProjectile().getVelocity();
				int slot = player.getInventory().first(Material.ARROW);
				ItemMeta im = player.getInventory().getItem(slot).getItemMeta();
				String arrowLaunchType = "null";
				boolean isSpecialArrow = im.getDisplayName() != null;
				
				if (isSpecialArrow){
					arrowLaunchType = im.getDisplayName().split(" ")[0];
				}else{
					arrowLaunchType = "Generic";
				}
				
				String[] arrowLaunchData = ((String)RPGSkills.configData.get(worldName).get(ConfigType.ARCHERYARROWDATA).get
						(ArrowType.valueOf(arrowLaunchType.toUpperCase()))).split(",");
				int arrowRequiredLevel = Integer.valueOf(arrowLaunchData[0]);
				int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName,player.getUniqueId(), ThisSkill);
				boolean canShootSpecialArrow = currentLevel >= arrowRequiredLevel;
				if (canShootSpecialArrow){
					float force = e.getForce();
					int arrowDamage = Integer.valueOf(arrowLaunchData[1]);
					double formulatedDamage = (force * 10) + arrowDamage;
					ArrowEffect effect = ArrowEffect.valueOf(arrowLaunchData[2]);
					
					e.setCancelled(true);
					Arrow newArrow = (Arrow) player.launchProjectile(Arrow.class);
					ItemStack item = new ItemStack(Material.ARROW, 1);
					item.setItemMeta(im);
					player.getInventory().removeItem(item);
					newArrow.setMetadata("type", new FixedMetadataValue(plugin, arrowLaunchType));
					newArrow.setMetadata("damage", new FixedMetadataValue(plugin, formulatedDamage));
					newArrow.setMetadata("effect", new FixedMetadataValue(plugin, effect.toString()));
					newArrow.setVelocity(velocity);
					
				}else{
					player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(arrowRequiredLevel) + " to loose this " + arrowLaunchType + " arrow.");
					e.setCancelled(true);
					player.updateInventory();
				}
				
			}
			}

	}
	
	@EventHandler
	public void onArrowPickup(PlayerPickupItemEvent e){
		ItemStack item = e.getItem().getItemStack();
		Player player = e.getPlayer();
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		
		if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName)
				&& (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE){
			
			if (item.getType() == Material.ARROW){
				if (e.getItem().hasMetadata("type")){
					String arrowType = "";
					for(MetadataValue s : e.getItem().getMetadata("type")){
						arrowType = s.asString();
						break;
					}
					if (arrowType.equalsIgnoreCase("generic")){
						return;
					}
					e.setCancelled(true);
					e.getItem().remove();
					ItemStack newItem = new ItemStack(Material.ARROW,1);
					ItemMeta im = newItem.getItemMeta();
					im.setDisplayName(arrowType + " Arrow");
					newItem.setItemMeta(im);
					player.getInventory().addItem(newItem);
				}
			}
			
		}
		
	}
	
	private ArrowEffect getArrowEffect(Arrow arrow){
		if (arrow.hasMetadata("effect")){
			for (MetadataValue s : arrow.getMetadata("effect")){
				return ArrowEffect.valueOf(s.asString());
			}
		}
		return null;
	}
	
	private Double getArrowDamage(Arrow arrow){
		if (arrow.hasMetadata("damage")){
			for (MetadataValue s : arrow.getMetadata("damage")){
				return s.asDouble();
			}
		}
		return null;
	}
}
