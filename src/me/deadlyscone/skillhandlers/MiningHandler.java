package me.deadlyscone.skillhandlers;

import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.BlockPlaceHandler;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class MiningHandler implements Listener{
	
	public RPGSkills plugin;
	private final String ThisSkill = "mining";
	
	HashMap<Integer, Double> BaseExpPerLevel = new HashMap<Integer, Double>();
	
	public MiningHandler(RPGSkills plugin){
	    plugin.getServer().getPluginManager().registerEvents(this, plugin);
	    this.plugin = plugin;
	}
	//TODO: add level bars above players heads, maybe a health bar?
	//TODO: add a scoreboard on the side for mining skill info.
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockBreak(BlockBreakEvent e){
		
		Player player = e.getPlayer();
		UUID playerUUID = player.getUniqueId();
		Location location = e.getBlock().getLocation();
		String coords = BlockPlaceHandler.DeserializeLocation(location);
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		
		if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName)
				&& (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE && !e.isCancelled()){
			
			//Variables
			double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, player.getUniqueId(), ThisSkill);
			int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
			Material materialHand = player.getItemInHand().getType();
			Material materialBlock = e.getBlock().getType();
			if (materialBlock == Material.GLOWING_REDSTONE_ORE){
				materialBlock = Material.REDSTONE_ORE;
			}
			
			//Main code starts here:
				
			
			if (isPickaxe(materialHand) && RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(materialHand) != null && e.isCancelled() == false && Integer.valueOf(String.valueOf(RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(materialHand)))  <= currentLevel && 
					RPGSkills.configData.get(worldName).get(ConfigType.MININGBLOCKSMAP).containsKey(materialBlock) || materialBlock == Material.GLOWING_REDSTONE_ORE){
				
			
				String[] BlockDataFromMap = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MININGBLOCKSMAP).get(materialBlock)).split(",");
				int RequiredLevelToMine = Integer.valueOf(BlockDataFromMap[0]);
				int ExpGainedFromMining = Integer.valueOf(BlockDataFromMap[1]);
				
				if(currentLevel >= RequiredLevelToMine){
					if (!BlockPlaceHandler.isBreakingPlayerPlacedBlock(location)){
						//give player his experience
						
						checkDoubleDrop(player, currentLevel, e.getBlock(), e.getBlock().getDrops().size());
						RPGSkills.worldExp.get(worldName).get(playerUUID).put(ThisSkill, (currentExp + ExpGainedFromMining));
						ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, player.getUniqueId(), currentLevel, ThisSkill);
					}else if (BlockPlaceHandler.isBreakingPlayerPlacedBlock(location)){
						
						BlockPlaceHandler.PlayerPlacedBlockLocations.remove(coords);

					}
					
				}else if (RPGSkills.configData.get(worldName).get(ConfigType.MININGBLOCKSMAP).containsKey(materialBlock) || materialBlock == Material.GLOWING_REDSTONE_ORE){
					player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(RequiredLevelToMine) + " to mine this block.");
					e.setCancelled(true);
				}
				
			}else if (isPickaxe(materialHand) && RPGSkills.configData.get(worldName).get(ConfigType.MININGBLOCKSMAP).containsKey(materialBlock) && e.isCancelled() == false && RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(materialHand) != null){
				player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(materialHand)) + " to use this pickaxe.");
				e.setCancelled(true);
			}
		}
	}
	
	private boolean isPickaxe(Material item){
		switch (item){
		
		case WOOD_PICKAXE:
			return true;
		case STONE_PICKAXE:
			return true;
		case IRON_PICKAXE:
			return true;
		case GOLD_PICKAXE:
			return true;
		case DIAMOND_PICKAXE:
			return true;
		default:
			return false;
		}
	}
	
	@SuppressWarnings("deprecation")
	private void checkDoubleDrop(Player player,int currentLevel, Block block, int amount){
		Random r = new Random();
		double ddc = (double)RPGSkills.configData.get(player.getWorld().getName()).get(ConfigType.MININGDOUBLEDROPCHANCE).get(0) * currentLevel;
		RPGSkills.configData.get(player.getWorld().getName()).get(ConfigType.MININGDOUBLEDROPCHANCE).get(0);
		double chance = r.nextDouble()*100;
		ItemStack itemDropped = (player.getItemInHand().containsEnchantment(Enchantment.SILK_TOUCH) && RPGSkills.configData.get(player.getWorld().getName()).get(ConfigType.MININGBLOCKSMAP).containsKey(block.getType())) ? new ItemStack(block.getType(), 1, block.getData()) : block.getDrops().toArray(new ItemStack[block.getDrops().size()])[0];

		
		if (chance <= ddc){
					block.getWorld().dropItemNaturally(block.getLocation(), itemDropped);
			}
		}
}
