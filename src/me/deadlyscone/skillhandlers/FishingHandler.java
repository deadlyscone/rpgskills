package me.deadlyscone.skillhandlers;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;
import org.bukkit.util.Vector;

public class FishingHandler implements Listener{
	private final String ThisSkill = "fishing";
	
	public FishingHandler(RPGSkills plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onPlayerFish(PlayerFishEvent e){
		Player player = e.getPlayer();
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName)
				&& (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE && !e.isCancelled()){
		//We caught a fish!
		if (e.getState() != null && e.getState() == State.CAUGHT_FISH){
			//Variables
			UUID playerUUID = player.getUniqueId();
			double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, playerUUID, ThisSkill);
			int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, playerUUID, ThisSkill);
			int ExpGainedFromFishing = (int)RPGSkills.configData.get(worldName).get(ConfigType.FISHINGBASEEXP).get(0);
			
			if (!(boolean)RPGSkills.configData.get(worldName).get(ConfigType.FISHINGVANILLAENABLED).get(0)){
				
				//check if player was lucky
				if (isTreasure(worldName, currentLevel)){
					e.setCancelled(true);
					spawnItem(pickTreasureItem(worldName, currentLevel), e.getCaught(), player);
					
				//check if player was unlucky
				}else if(isJunk(worldName, currentLevel)){
					e.setCancelled(true);
					Object o = pickJunkItem(worldName, currentLevel);
					
					if (o instanceof ItemStack){
						player.sendMessage("Must have reeled some garbage.");
						spawnItem((ItemStack) o, e.getCaught(), player);
					
					}else if (o instanceof String){
						switch ((String)o){
						case "ROTTEN_FISH":
							throwPoisonFish(player, e.getCaught(), e.getCaught().getLocation());
							spawnItem(new ItemStack(Material.RAW_FISH,1), e.getCaught(), player);
							break;
						case "PRIMED_TNT":
							throwTNT(player, e.getCaught(), e.getCaught().getLocation());
							break;
							
						}
					}
					//check to see if we need to stop vanilla drops.
				}else if(!(boolean)RPGSkills.configData.get(worldName).get(ConfigType.FISHINGVANILLAPARALLELENABLED).get(0)){
					e.setCancelled(true);
					spawnItem(pickFish(), e.getCaught(), player);
				}
				//finally, give player his experience
				RPGSkills.worldExp.get(worldName).get(playerUUID).put(ThisSkill, (currentExp + ExpGainedFromFishing));
				ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, playerUUID, currentLevel, ThisSkill);
				
				
			}else{
				//give player his experience
				RPGSkills.worldExp.get(worldName).get(playerUUID).put(ThisSkill, (currentExp + ExpGainedFromFishing));
				ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, playerUUID, currentLevel, ThisSkill);
			}
			

		}
	  }
	}
	
	private ItemStack pickFish() {
		Random r = new Random();
		int chance = r.nextInt(100);
		System.out.println(chance);
		if (chance <= 2){
			return new ItemStack(Material.RAW_FISH, 1, (short)2);
		}else if (chance < 13){
			return new ItemStack(Material.RAW_FISH, 1, (short)3);
		}else if (chance <= 45){
			return new ItemStack(Material.RAW_FISH, 1, (short)1);
		}else{
			return new ItemStack(Material.RAW_FISH, 1, (short)0);
		}
	}

	private void spawnItem(ItemStack item, Entity caught, Player player) {
		double px = player.getEyeLocation().getX();
		double py = player.getEyeLocation().getY();
		double pz = player.getEyeLocation().getZ();
		
		double cx = caught.getLocation().getX();
		double cy = caught.getLocation().getY();
		double cz = caught.getLocation().getZ();
		
		Vector vector = new Vector(px - cx,(py - cy)+4, pz - cz);
		
		if (item != null){
			Item drop = player.getWorld().dropItem(caught.getLocation(), item);
			drop.setVelocity(vector.normalize());
		}else{
			spawnItem(pickFish(), caught, player);
		}
	}

	
	@SuppressWarnings({ "deprecation", "unchecked" })
	private ItemStack pickTreasureItem(String worldName, int currentLevel) {
		Random r = new Random();
		ArrayList<String> list = (ArrayList<String>) RPGSkills.configData.get(worldName).get(ConfigType.FISHINGTREASURES).get(0);
		String[] itemList = list.get(r.nextInt(list.size())).split(",");
		ItemStack item = new ItemStack(Integer.valueOf(itemList[0]), Integer.valueOf(itemList[1]), Short.valueOf(itemList[2]));
		
		int chance = r.nextInt(100);
		long c = Integer.valueOf(itemList[3]) + Math.round(currentLevel * 0.5);
		if (chance > c){
			return null;
		}else{
			return item;
		}
		
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	private Object pickJunkItem(String worldName, int currentLevel) {
		Random r = new Random();
		ArrayList<String> list = (ArrayList<String>) RPGSkills.configData.get(worldName).get(ConfigType.FISHINGJUNK).get(0);
		String[] itemList = list.get(r.nextInt(list.size())).split(",");
		int chance = r.nextInt(100);
		long c = Integer.valueOf(itemList[3]) - Math.round(currentLevel / 7);
		if (itemList[0].equals("ROTTEN_FISH")){
			if (chance > c){
				return "ROTTEN_FISH";
			}else{
				return null;
			}
		}else if (itemList[0].equals("PRIMED_TNT")){
			if (chance > c){
				return "PRIMED_TNT";
			}else{
				return null;
			}
		}else{
			if (chance > c){
				ItemStack item = new ItemStack(Integer.valueOf(itemList[0]), Integer.valueOf(itemList[1]), Short.valueOf(itemList[2]));
				return item;
			}else{
				return null;
			}
		}
		
	}

	private boolean isTreasure(String worldName, int currentLevel) {
		Random r = new Random();
		long c = (int)RPGSkills.configData.get(worldName).get(ConfigType.FISHINGTREASURECHANCE).get(0) + Math.round(currentLevel * 0.5);
		int chance = r.nextInt(100);
		
		if (chance > c){
			return false;
		}else{
			return true;
		}
	}
	private boolean isJunk(String worldName, int currentLevel) {
		Random r = new Random();
		long c = (int)RPGSkills.configData.get(worldName).get(ConfigType.FISHINGJUNKCHANCE).get(0) - Math.round(currentLevel / 7);
		int chance = r.nextInt(100);
		
		if (chance > c){
			return false;
		}else{
			return true;
		}
	}

	//VVVvvv **Thrown methods** VVVvvv
	private void throwTNT(Player player, Entity caught, Location caughtLocation){
		int px = player.getLocation().getBlockX();
		int py = player.getLocation().getBlockY();
		int pz = player.getLocation().getBlockZ();
		
		int cx = caught.getLocation().getBlockX();
		int cy = caught.getLocation().getBlockY();
		int cz = caught.getLocation().getBlockZ();
		
		Vector vector = new Vector(px - cx,(py - cy) + 5, pz - cz);
		
		TNTPrimed tnt = (TNTPrimed) player.getWorld().spawn(caughtLocation, TNTPrimed.class);
		tnt.setVelocity(vector.normalize());
		RPGSkills.ProjectileInt.put(tnt.getUniqueId(), 1);
		player.sendMessage("That sounds familiar...");
	}
	private void throwPoisonFish(Player player, Entity caught, Location caughtLocation){
		Potion potion = new Potion(PotionType.POISON, 2);
		potion.setSplash(true);
		ItemStack newPotion = new ItemStack(Material.POTION);
		potion.apply(newPotion);
		
		double px = player.getEyeLocation().getX();
		double py = player.getEyeLocation().getY();
		double pz = player.getEyeLocation().getZ();
		
		double cx = caught.getLocation().getX();
		double cy = caught.getLocation().getY();
		double cz = caught.getLocation().getZ();
		
		Vector vector = new Vector(px - cx,(py - cy)+4, pz - cz);
		
		ThrownPotion tp = (ThrownPotion) player.getWorld().spawn(caughtLocation, ThrownPotion.class);
		tp.setItem(newPotion);
		tp.setVelocity(vector.normalize());
		player.sendMessage("Something smells rotten...");
	}
}
