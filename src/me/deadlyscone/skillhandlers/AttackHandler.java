package me.deadlyscone.skillhandlers;

import java.text.DecimalFormat;
import java.util.HashMap;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class AttackHandler implements Listener{
	
	public RPGSkills plugin;
	private final String ThisSkill = "attack";
	HashMap<Integer, Double> BaseExpPerLevel = new HashMap<Integer, Double>();

	public AttackHandler(RPGSkills plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	    this.plugin = plugin;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e){
		Player damager = null;
		String worldName = ExperienceSystemHandler.checkActiveWorld(e.getDamager().getWorld().getName());
		//ATTACK**
		if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && 
				RPGSkills.ActiveWorldNames.contains(worldName) && !e.isCancelled()){
			
		//check if the damager is a player** we only want to know this for attacking
		if (e.getDamager() instanceof Player && (((Player)e.getDamager()).hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && ((Player)e.getDamager()).getGameMode() != GameMode.CREATIVE){
			//variables
			damager = (Player) e.getDamager();
			double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, damager.getUniqueId(), ThisSkill);
			int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, damager.getUniqueId(), ThisSkill);
			Material ItemInHand = damager.getItemInHand().getType();
			
			//check if the player is using a sword and the entity can take damage
			if (isSword(damager.getItemInHand().getType()) == true && e.isCancelled() == false && RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(ItemInHand) != null
					&& (int)RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(ItemInHand) <= currentLevel && e.getEntity() instanceof Damageable){
				    
				double damage = e.getDamage();
				
				//if the attackedEntity is a player and the Damager is a player^
				if (e.getEntity() instanceof Player && e.isCancelled() == false){
					Player player = (Player) e.getEntity();
					//check if the attacked player was blocking before setting the damage.
					Player attackedPlayer = (Player) e.getEntity();
					
					//check if the attacked player is blocking
					if (player.isBlocking()){
						
						//check if defense is enabled
						if (RPGSkills.isSkillEnabled.get(worldName).get("defense") == true && RPGSkills.ActiveWorldNames.contains(worldName)
								&& (player.hasPermission("rpgskills.skills.defense") ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE){
							damage -= getDamageToSubtract(damager.getItemInHand().getType());
							double negate = DefenseHandler.getEventDamage(attackedPlayer);
							
							double DamageOfSwordBasedOnConfig = (double)RPGSkills.configData.get(worldName).get(ConfigType.ATTACKDAMAGEMAP).get(damager.getItemInHand().getType());
							double DamageBasedOnLevel = DamageOfSwordBasedOnConfig + (currentLevel * (double)RPGSkills.configData.get(worldName).get(ConfigType.ATTACKGLOBALMODIFIER).get(0));
							//TODO: play with this to get it dialed in IF NEEDED
							//remember, what ever this value is, it will be reduced by 1.50
							double totalDamage = (((DamageBasedOnLevel + damage) * 1.50) - negate);
							if (totalDamage < 0){
								e.setDamage(0.0);
							}else{
								e.setDamage(totalDamage);
							}
							
							//check if defense is NOT enabled
						}else if (RPGSkills.isSkillEnabled.get(worldName).get("defense") == false && RPGSkills.ActiveWorldNames.contains(worldName)
								&& (player.hasPermission("rpgskills.skills.defense") ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE){
							damage -= getDamageToSubtract(damager.getItemInHand().getType());
							int newInt = Integer.parseInt(new DecimalFormat("##").format(damage));
							double DamageOfSwordBasedOnConfig = (double)RPGSkills.configData.get(worldName).get(ConfigType.ATTACKDAMAGEMAP).get(damager.getItemInHand().getType());
							double DamageBasedOnLevel = DamageOfSwordBasedOnConfig + (currentLevel * (double)RPGSkills.configData.get(worldName).get(ConfigType.ATTACKGLOBALMODIFIER).get(0));
							
							//set the damage based on their level
						    e.setDamage(DamageBasedOnLevel + damage);
							
							//give player his experience
							int ExpGainedFromAttacking = (int)RPGSkills.configData.get(worldName).get(ConfigType.ATTACKEXPPERENTITY).get("Player") + newInt;
							RPGSkills.worldExp.get(worldName).get(damager.getUniqueId()).put(ThisSkill, (currentExp + ExpGainedFromAttacking));
							ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, damager.getUniqueId(), currentLevel, ThisSkill);
						}

						
						
						//if the player is not blocking, do normal events
					}else if(!player.isBlocking()){
						damage -= getDamageToSubtract(damager.getItemInHand().getType());
						int newInt = Integer.parseInt(new DecimalFormat("##").format(damage));
						double DamageOfSwordBasedOnConfig = (double)RPGSkills.configData.get(worldName).get(ConfigType.ATTACKDAMAGEMAP).get(damager.getItemInHand().getType());
						double DamageBasedOnLevel = DamageOfSwordBasedOnConfig + (currentLevel * (double)RPGSkills.configData.get(worldName).get(ConfigType.ATTACKGLOBALMODIFIER).get(0));
						
						//set the damage based on their level.
					    e.setDamage(DamageBasedOnLevel + damage);
						
						//give player his experience
						int ExpGainedFromAttacking = (int)RPGSkills.configData.get(worldName).get(ConfigType.ATTACKEXPPERENTITY).get("Player") + newInt;
						//RPGSkills.playerAttackExp.put(damagerName, (currentExp + ExpGainedFromAttacking));
						RPGSkills.worldExp.get(worldName).get(damager.getUniqueId()).put(ThisSkill, (currentExp + ExpGainedFromAttacking));
						ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, damager.getUniqueId(), currentLevel, ThisSkill);
					}
				
					
					
					
					//if the attackedEntity is NOT a player and the Damager is a player^
				}else if (e.getEntity().getType() != EntityType.PLAYER){
					
					damage -= getDamageToSubtract(damager.getItemInHand().getType());
					int newInt = Integer.parseInt(new DecimalFormat("##").format(damage));
					int ExpGainedFromAttacking = (int)RPGSkills.configData.get(worldName).get(ConfigType.ATTACKEXPPERENTITY).get("Mob") + newInt;
					double DamageOfSwordBasedOnConfig = (double)RPGSkills.configData.get(worldName).get(ConfigType.ATTACKDAMAGEMAP).get(damager.getItemInHand().getType());
					double DamageBasedOnLevel = DamageOfSwordBasedOnConfig + (currentLevel * (double)RPGSkills.configData.get(worldName).get(ConfigType.ATTACKGLOBALMODIFIER).get(0));
					
					//set the damage based on their level.
				    e.setDamage(DamageBasedOnLevel + damage);
				    
					//give player his experience
					//RPGSkills.playerAttackExp.put(damagerName, (currentExp + ExpGainedFromAttacking));
				    
				    //System.out.println("XP: "+ExpGainedFromAttacking);
					RPGSkills.worldExp.get(worldName).get(damager.getUniqueId()).put(ThisSkill, (currentExp + ExpGainedFromAttacking));
					ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, damager.getUniqueId(), currentLevel, ThisSkill);
				}
				
				
				//if the player can't use that sword
			}else if (e.isCancelled() == false && isSword(damager.getItemInHand().getType()) == true && RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(ItemInHand) != null && e.getEntity() instanceof Damageable){
				e.setCancelled(true);
				damager.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf((int)RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(ItemInHand)) + " to attack with this sword.");
			
			
				//UNARMED**
			}else if (!e.isCancelled() && isNotTool(damager.getItemInHand().getType()) && damager.getItemInHand().getTypeId() != RPGSkills.MagicWandID && (e.getEntity() instanceof Creature || e.getEntity() instanceof Player) && !e.getEntity().isDead()){
				int unarmedCurrentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, damager.getUniqueId(), "unarmed");
				if (e.getEntity() instanceof Player){
					//if the attacked is blocking
					Player attacked = (Player)e.getEntity();
					if (attacked.isBlocking()){
						double negate = DefenseHandler.getEventDamage(attacked);
						double factoredDamage = UnarmedHandler.onPlayerPunch(damager, e.getEntity());
						double damage = ((e.getDamage() - 1) + ((double)RPGSkills.configData.get(worldName).get(ConfigType.UNARMEDBASEDAMAGE).get(0) + (unarmedCurrentLevel * factoredDamage)));
						double totalDamage = (((damage) * 1.50) - negate);
						
						if (totalDamage < 0){
							e.setDamage(0.0);
						}else{
							e.setDamage(totalDamage);
						}
					    
					    //if the attacked is NOT blocking
					}else if (!attacked.isBlocking()){
						double factoredDamage = UnarmedHandler.onPlayerPunch(damager, e.getEntity());
						double damage = ((e.getDamage() - 1) + ((double)RPGSkills.configData.get(worldName).get(ConfigType.UNARMEDBASEDAMAGE).get(0) + (unarmedCurrentLevel * factoredDamage)));
					    e.setDamage(damage);
					}
				}else{
					double factoredDamage = UnarmedHandler.onPlayerPunch(damager, e.getEntity());
					double damage = ((e.getDamage() - 1) + ((double)RPGSkills.configData.get(worldName).get(ConfigType.UNARMEDBASEDAMAGE).get(0) + (unarmedCurrentLevel * factoredDamage)));
				    e.setDamage(damage);

				}

			}
			
			//check if the damager was an arrow or creature(this is used to pass through the block event)
		}else if (e.getEntity() instanceof Player){
			Player attackedPlayer = (Player) e.getEntity();
			double damage = e.getDamage();
			
			//check if the attacked player is blocking
			if (attackedPlayer.isBlocking()){
				//check if defense is enabled
				if (RPGSkills.isSkillEnabled.get(worldName).get("defense") == true && RPGSkills.ActiveWorldNames.contains(worldName)
						&& (attackedPlayer.hasPermission("rpgskills.skills.defense") ? true : RPGSkills.usePerms)&& attackedPlayer.getGameMode() != GameMode.CREATIVE){
					
					double negate = DefenseHandler.getEventDamage(attackedPlayer);
					//remember, what ever this value is, it will be reduced by 1.50
					double totalDamage = ( (damage * 1.50) - negate);
					
					if(totalDamage < 0){
						e.setDamage(0.0);
					}else{
						e.setDamage(totalDamage);
					}
				}else if (RPGSkills.isSkillEnabled.get(worldName).get("defense") == false && RPGSkills.ActiveWorldNames.contains(worldName)
						&& (attackedPlayer.hasPermission("rpgskills.skills.defense") ? true : RPGSkills.usePerms) && attackedPlayer.getGameMode() != GameMode.CREATIVE){
					e.setDamage(damage);
				}
				

				
				
				
				//if the player is not blocking, do normal events
			}
			
		}
		
		//if attack skill is disabled, we still need to check if the player is blocking(add a check to see if defence is enabled)
	}else if (!RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) && 
			RPGSkills.ActiveWorldNames.contains(worldName) && e.getEntity() instanceof Player && !e.isCancelled()){
		
		Player attackedPlayer = (Player) e.getEntity();
		double damage = e.getDamage();
		
		
		//check if the attacked player is blocking
		if (attackedPlayer.isBlocking()){
			
			//check if defense is enabled
			if (RPGSkills.isSkillEnabled.get(worldName).get("defense") == true && RPGSkills.ActiveWorldNames.contains(worldName)
					&& (attackedPlayer.hasPermission("rpgskills.skills.defense") ? true : RPGSkills.usePerms) && attackedPlayer.getGameMode() != GameMode.CREATIVE){
				
				double negate = DefenseHandler.getEventDamage(attackedPlayer);
				//remember, what ever this value is, it will be reduced by 1.50
				double totalDamage = ( (damage * 1.50) - negate);
				if (totalDamage < 0){
					e.setDamage(0.0);
				}else{
					e.setDamage(totalDamage);
				}
			}else if (RPGSkills.isSkillEnabled.get(worldName).get("defense") == false && RPGSkills.ActiveWorldNames.contains(worldName)
					&& (attackedPlayer.hasPermission("rpgskills.skills.defense") ? true : RPGSkills.usePerms) && attackedPlayer.getGameMode() != GameMode.CREATIVE){
				e.setDamage(damage);
			}
			
			
			//if the player is not blocking, do normal events
		}
	}
		
	}
	
	private boolean isSword(Material m){
		switch (m){
			case WOOD_SWORD:
				return true;
			case STONE_SWORD:
				return true;
			case IRON_SWORD:
				return true;
			case GOLD_SWORD:
				return true;
			case DIAMOND_SWORD:
				return true;
			default:
				return false;
			
		}
		
	}
	
	private boolean isNotTool(Material m){
		switch (m){
		case WOOD_SWORD:
			return false;
		case STONE_SWORD:
			return false;
		case IRON_SWORD:
			return false;
		case GOLD_SWORD:
			return false;
		case DIAMOND_SWORD:
			return false;
		case WOOD_SPADE:
			return false;
		case STONE_SPADE:
			return false;
		case IRON_SPADE:
			return false;
		case GOLD_SPADE:
			return false;
		case DIAMOND_SPADE:
			return false;
		case WOOD_PICKAXE:
			return false;
		case STONE_PICKAXE:
			return false;
		case IRON_PICKAXE:
			return false;
		case GOLD_PICKAXE:
			return false;
		case DIAMOND_PICKAXE:
			return false;
		case WOOD_AXE:
			return false;
		case STONE_AXE:
			return false;
		case IRON_AXE:
			return false;
		case GOLD_AXE:
			return false;
		case DIAMOND_AXE:
			return false;
		case WOOD_HOE:
			return false;
		case STONE_HOE:
			return false;
		case IRON_HOE:
			return false;
		case GOLD_HOE:
			return false;
		case DIAMOND_HOE:
			return false;
		default:
			return true;
		}
	}
	
	private static double getDamageToSubtract(Material iteminhand){
		switch (iteminhand){
		case WOOD_SWORD:
			return 5.0;
		case STONE_SWORD:
			return 6.0;
		case IRON_SWORD:
			return 7.0;
		case GOLD_SWORD:
			return 5.0;
		case DIAMOND_SWORD:
			return 8.0;
		default:
			return 0.0;
		
	  }
	}
}

