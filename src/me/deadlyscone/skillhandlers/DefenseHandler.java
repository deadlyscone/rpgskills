package me.deadlyscone.skillhandlers;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class DefenseHandler {
	
	private static final String ThisSkill = "defense";
	
	public static double getEventDamage(Player attackedPlayer){
		String worldName = ExperienceSystemHandler.checkActiveWorld(attackedPlayer.getWorld().getName());
		double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, attackedPlayer.getUniqueId(), ThisSkill);
		int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, attackedPlayer.getUniqueId(), ThisSkill);
		int LevelRequiredToBlock = (int) RPGSkills.configData.get(worldName).get(ConfigType.DEFENSETOOLLEVELS).get(attackedPlayer.getItemInHand().getType());
		
		//if the player is able to block with their current sword.
		if(currentLevel >= LevelRequiredToBlock){
			
			//some variables

			double rawNegateAmountFromConfig = (double)RPGSkills.configData.get(worldName).get(ConfigType.DEFENSENEGATEMAP).get(attackedPlayer.getItemInHand().getType());
			double factoredNegate = rawNegateAmountFromConfig + (currentLevel * (double)RPGSkills.configData.get(worldName).get(ConfigType.DEFENSEGLOBALMODIFIER).get(0));
			switch(attackedPlayer.getItemInHand().getType()){
			
			case WOOD_SWORD:
				attackedPlayer.playSound(attackedPlayer.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_DOOR_WOOD, 0.6f, 1f);
				break;
			case STONE_SWORD:
				attackedPlayer.playSound(attackedPlayer.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_DOOR_WOOD, 0.5f, 0.1f);
				break;
			case IRON_SWORD:
				attackedPlayer.playSound(attackedPlayer.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1f, 1f);
				break;
			case GOLD_SWORD:
				attackedPlayer.playSound(attackedPlayer.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1f, 1f);
				break;
			case DIAMOND_SWORD:
				attackedPlayer.playSound(attackedPlayer.getLocation(), Sound.BLOCK_ANVIL_LAND, 0.6f, 1f);
				break;
			default:
				break;
			}
			
			//give player his exp
			int ExpGainedFromAttacking = (int)RPGSkills.configData.get(worldName).get(ConfigType.DEFENSEBASEEXP).get(0);
			//RPGSkills.playerDefenseExp.put(attackedPlayer.getName(), (currentExp + ExpGainedFromAttacking));
			RPGSkills.worldExp.get(worldName).get(attackedPlayer.getUniqueId()).put(ThisSkill, (currentExp + ExpGainedFromAttacking));
			ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, attackedPlayer.getUniqueId(), currentLevel, ThisSkill);
			
			return factoredNegate;
			
		}else if (currentLevel < LevelRequiredToBlock){
			attackedPlayer.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf((int)RPGSkills.configData.get(worldName).get(ConfigType.DEFENSETOOLLEVELS).get(attackedPlayer.getItemInHand().getType())) + " to block with that sword.");
		}
		
		//the player was not able to use the sword.
		return 0.0;
		
	}

}
