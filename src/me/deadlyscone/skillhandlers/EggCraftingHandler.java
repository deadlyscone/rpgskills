package me.deadlyscone.skillhandlers;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;

public class EggCraftingHandler implements Listener{
	private final String ThisSkill = "eggcrafting";
	public EggCraftingHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
	}
	
	@EventHandler
	public void onPlayerCraftEgg(CraftItemEvent e){
		String worldName = ExperienceSystemHandler.checkActiveWorld(e.getWhoClicked().getWorld().getName());
		if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName) && !e.isCancelled()){
			
			Player player = e.getWhoClicked() instanceof Player ? (Player)e.getWhoClicked() : null;
		    
			if(e.getInventory().getResult().getType() == Material.MONSTER_EGG && !e.getClick().isShiftClick() && (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE){
				double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, player.getUniqueId(), ThisSkill);
				int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
			    EntityType entityType = getEntityTypeByID(e.getInventory().getResult().getDurability());
				int ExpGainedFromCrafting = Integer.valueOf(((String) RPGSkills.configData.get(worldName).get(ConfigType.EGGCRAFTINGPROPERTYDATA).get(entityType)).split(",")[1]);
				
			    int levelRequired = Integer.valueOf(((String) RPGSkills.configData.get(worldName).get(ConfigType.EGGCRAFTINGPROPERTYDATA).get(entityType)).split(",")[0]);
			    
				if (currentLevel >= levelRequired){
					//RPGSkills.playerEggCraftingExp.put(player.getName(), (currentExp + ExpGainedFromCrafting));
					RPGSkills.worldExp.get(worldName).get(player.getUniqueId()).put(ThisSkill, (currentExp + ExpGainedFromCrafting));
					ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, player.getUniqueId(), currentLevel, ThisSkill);
				}else if(currentLevel < levelRequired){
					e.setCancelled(true);
					player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(levelRequired) + " to craft this creature.");
				}
			}else if (e.getInventory().getResult().getType() == Material.MONSTER_EGG && e.getClick().isShiftClick() && player.hasPermission("rpgskills.skills.eggcrafting") && player.getGameMode() != GameMode.CREATIVE){
				e.setCancelled(true);
				player.sendMessage(ChatColor.RED + "You cannot shift click.");
			}
			
		}
		
	}

	private EntityType getEntityTypeByID(int entityID) {
		switch (entityID){
		case 50:
			return EntityType.CREEPER;
		case 51:
			return EntityType.SKELETON;
		case 52:
			return EntityType.SPIDER;
		case 54:
			return EntityType.ZOMBIE;
		case 55:
			return EntityType.SLIME;
		case 56:
			return EntityType.GHAST;
		case 57:
			return EntityType.PIG_ZOMBIE;
		case 58:
			return EntityType.ENDERMAN;
		case 59:
			return EntityType.CAVE_SPIDER;
		case 60:
			return EntityType.SILVERFISH;
		case 61:
			return EntityType.BLAZE;
		case 62:
			return EntityType.MAGMA_CUBE;
		case 65:
			return EntityType.BAT;
		case 66:
			return EntityType.WITCH;
		case 67:
			return EntityType.ENDERMITE;
		case 68:
			return EntityType.GUARDIAN;
		case 90:
			return EntityType.PIG;
		case 91:
			return EntityType.SHEEP;
		case 92:
			return EntityType.COW;
		case 93:
			return EntityType.CHICKEN;
		case 94:
			return EntityType.SQUID;
		case 95:
			return EntityType.WOLF;
		case 96:
			return EntityType.MUSHROOM_COW;
		case 98:
			return EntityType.OCELOT;
		case 100:
			return EntityType.HORSE;
		case 101:
			return EntityType.RABBIT;
		case 120:
			return EntityType.VILLAGER;
			default:
				return null;
		}
	}
}
