package me.deadlyscone.skillhandlers;

import java.util.Random;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.BlockPlaceHandler;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Tree;

public class WoodcuttingHandler implements Listener{
private final String ThisSkill = "woodcutting";

public WoodcuttingHandler(RPGSkills plugin){
	plugin.getServer().getPluginManager().registerEvents(this, plugin);
}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onWoodCut(BlockBreakEvent e){
		
		Player player = e.getPlayer();
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
	if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName)
			&& (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE && !e.isCancelled()){
		//Variables
		double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, player.getUniqueId(), ThisSkill);
		int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
		Material itemInHand = e.getPlayer().getItemInHand().getType();
		
		if (isAxe(itemInHand) && (int)RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(itemInHand) <= currentLevel && (e.getBlock().getType() == Material.LOG
				|| e.getBlock().getType() == Material.LOG_2)){
			
			String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.WOODTYPEDATA).get(getWoodType(e.getBlock()))).split(",");
			int requiredLevelToCut = Integer.valueOf(data[0]);
			int expGainedFromCutting = Integer.valueOf(data[1]);
			Location location = e.getBlock().getLocation();
			
			if (requiredLevelToCut <= currentLevel){
				if (!BlockPlaceHandler.isBreakingPlayerPlacedBlock(location)){
					//give player his experience
					checkDoubleDrop(player,currentLevel, e.getBlock());
					RPGSkills.worldExp.get(worldName).get(player.getUniqueId()).put(ThisSkill, (currentExp + expGainedFromCutting));
					//RPGSkills.playerWoodcuttingExp.put(worldName, player.getName(), (currentExp + expGainedFromCutting));
					ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, player.getUniqueId(), currentLevel, ThisSkill);
					//-----
				}else{
					BlockPlaceHandler.PlayerPlacedBlockLocations.remove(BlockPlaceHandler.DeserializeLocation(location));
				}
				
			}else if (requiredLevelToCut > currentLevel){
				player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(requiredLevelToCut) + " to chop this log.");
				e.setCancelled(true);
			}
		}else if(!isAxe(itemInHand) && (e.getBlock().getType() == Material.LOG || e.getBlock().getType() == Material.LOG_2)){
		    if ((boolean)RPGSkills.configData.get(worldName).get(ConfigType.WOODCUTTINGFORCEAXE).get(0)){
				player.sendMessage(player.getItemInHand().getType() != Material.AIR ? ChatColor.RED + "You cannot use " + String.valueOf(player.getItemInHand().getType()) + " to chop this log.":
			    	ChatColor.RED + "You cannot use your fist to chop this log.");
				e.setCancelled(true);
		    }
		}else if (isAxe(itemInHand) && (int)RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(itemInHand) > currentLevel && (e.getBlock().getType() == Material.LOG
				|| e.getBlock().getType() == Material.LOG_2)){
			player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf((int)RPGSkills.configData.get(worldName).get(ConfigType.TOOLLEVELS).get(itemInHand)) + " to use this axe.");
			e.setCancelled(true);
		}
		
	}
  }
	
	private boolean isAxe(Material itemInHand){
		switch (itemInHand){
		
		case WOOD_AXE:
			return true;
		case STONE_AXE:
			return true;
		case IRON_AXE:
			return true;
		case GOLD_AXE:
			return true;
		case DIAMOND_AXE:
			return true;
		default:
			return false;
			
		}
	}
	
	@SuppressWarnings("deprecation")
	private String getWoodType(Block block){
		
		
		Tree tree = block.getType() != Material.LOG_2 ? (Tree)block.getState().getData() : null;
		TreeSpecies ts = tree != null ? tree.getSpecies() : null;
		
		// Fix for log_2 cast to tree error.
		if (block.getType() == Material.LOG_2){
			byte data = block.getData();
			boolean isDarkOak = data == 1 || data == 5 || data == 9 ? true : false;
			boolean isAcacia = data == 0 || data == 4 || data == 8 ? true : false;
			if (isDarkOak){
				ts = TreeSpecies.DARK_OAK;
			}else if (isAcacia){
				ts = TreeSpecies.ACACIA;
			}
		}
			
			switch (ts){
			
			case GENERIC:
				return "OAK";
				
			case ACACIA:
				return "ACACIA";
				
			case BIRCH:
				return "BIRCH";
				
			case JUNGLE:
				return "JUNGLE";
				
			case REDWOOD:
				return "SPRUCE";
				
			case DARK_OAK:
				return "DARK OAK";
				
				default:
					return null;
			}
		
	}
	
	private void checkDoubleDrop(Player player,int currentLevel, Block block){
		Random r = new Random();
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		double ddc = (double)RPGSkills.configData.get(worldName).get(ConfigType.WOODCUTTINGDOUBLEDROPCHANCE).get(0) * currentLevel;
		double chance = r.nextDouble()*100;
		
		if (chance <= ddc){
			ItemStack[] itemDropped = block.getDrops().toArray(new ItemStack[block.getDrops().size()]);
				block.getWorld().dropItemNaturally(block.getLocation(), itemDropped[0]);
		}
	}
}