package me.deadlyscone.skillhandlers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import me.deadlyscone.main.RPGSkills;
import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.otherhandlers.ExperienceSystemHandler;
import me.deadlyscone.otherhandlers.SpellHandler;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MagicHandler implements Listener{
	private final String ThisSkill = "magic";
	private Spell spell1, spell2;
    final HashMap<String, Boolean> isFirstClick = new HashMap<String, Boolean>();
    final HashMap<String, Action> FirstClickActionType = new HashMap<String, Action>();
	public MagicHandler(RPGSkills plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onMagicUse(PlayerInteractEvent e){
		Player player = e.getPlayer();
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		if (RPGSkills.isSkillEnabled.get(worldName).get(ThisSkill) == true && RPGSkills.ActiveWorldNames.contains(worldName)
				&& (player.hasPermission("rpgskills.skills." + ThisSkill) ? true : RPGSkills.usePerms) && player.getGameMode() != GameMode.CREATIVE){
			
		
		//checks if we need to get the second click event.
		if (isFirstClick.get(player.getName()) != null && isFirstClick.get(player.getName()) == false && (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK)){
			
			e.setCancelled(true);
    		isFirstClick.put(player.getName(), true);
    		
        	double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, player.getUniqueId(), ThisSkill);
			int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
			
			if (FirstClickActionType.get(player.getName()) == Action.LEFT_CLICK_AIR || FirstClickActionType.get(player.getName()) == Action.LEFT_CLICK_BLOCK){
				switch (spell1){
	    		case SUPERHEAT:
	        		ItemStack itemToSmelt = player.getItemInHand();
	        		if (SpellHandler.doSuperHeat(itemToSmelt, player)){
		    			handleExpAndMP(spell1, currentExp, currentLevel, player);
	        		}
	    			break;
				default:
					break;
	    			
	    		}
			}

			
			if (FirstClickActionType.get(player.getName()) == Action.RIGHT_CLICK_AIR || FirstClickActionType.get(player.getName()) == Action.RIGHT_CLICK_BLOCK){
				switch (spell2){
	    		case SUPERHEAT:
	        		ItemStack itemToSmelt = player.getItemInHand();
	        		if (SpellHandler.doSuperHeat(itemToSmelt, player)){
		    			handleExpAndMP(spell2, currentExp, currentLevel, player);
	        		}
	    			break;
				default:
	    			break;
	    		}
			}
    	}
		
		if (player.getItemInHand().getType() == Material.getMaterial(RPGSkills.MagicWandID) && (e.getAction() == Action.LEFT_CLICK_AIR || 
				e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR)){
			//define the spell book item
	        ItemStack spellBook = new ItemStack(e.getItem());
	        ItemMeta meta = spellBook.getItemMeta();
	        
	        //check the format
	        	
	        	
	        	//**VARIABLES**
				
				
	        	//check what spells we have selected.
	        	String[] selectedSpells = getSelectedSpells(meta).split(":");
	        	//spell 1 is strictly left click
	        	spell1 = selectedSpells.length > 0 && !selectedSpells[0].isEmpty() ? Spell.valueOf(selectedSpells[0].toUpperCase()) : null;
	        	//spell 2 is strictly right click
	        	spell2 = selectedSpells.length > 1 && !selectedSpells[1].isEmpty() ? Spell.valueOf(selectedSpells[1].toUpperCase()) : null;
	        	//**---------**

	        	double currentExp = ExperienceSystemHandler.getPlayerExperience(worldName, player.getUniqueId(), ThisSkill);
				int currentLevel = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, player.getUniqueId(), ThisSkill);
	        	
	        	
	    //** Vv SPELL 1 CODE vV **
				
	        	if ((isFirstClick.get(player.getName()) == null || isFirstClick.get(player.getName()) == true) && (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) && spell1 != null){
	        		e.setCancelled(true);
	        		
	        	    //**Variables for Spell 1**
					String[] MagicPropertiesFromList = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICEXPPROPERTYDATA).get(spell1.toString())).split(",");
					
					int requiredLevelToCast = Integer.valueOf(MagicPropertiesFromList[1]);
					int mpCost = Integer.valueOf(MagicPropertiesFromList[2]);
					int currentMP = ExperienceSystemHandler.getCurrentPlayerMP(player);
					
					boolean hasEnoughMP = (currentMP >= mpCost);
					boolean meetsLevelReq = (currentLevel >= requiredLevelToCast);
					
					if (meetsLevelReq && hasEnoughMP){
						
						handleSpell(spell1, player, e.getAction(), currentExp, currentLevel);
		        		//if handled spell returns true VvV
						/**
		        		if (handleSpell(spell1, player, e.getAction())){
		        			handleExpAndMP(spell1, currentExp, currentLevel, player);
		        		}	
		        		**/
					}else if (!meetsLevelReq){
						player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(requiredLevelToCast) + " to cast " + spell1.toString());
					}else if (!hasEnoughMP){
						player.sendMessage(ChatColor.RED + "You need to have atleast " + String.valueOf(mpCost) + "MP to cast " + spell1.toString());
					}
	        		
					
	//** Vv SPELL 2 CODE vV **
					
	        	}else if ((isFirstClick.get(player.getName()) == null || isFirstClick.get(player.getName()) == true) && (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) && spell2 != null){
	        	    e.setCancelled(true);
	        	    
	        		//**Variables for Spell 2**
					String[] MagicPropertiesFromList = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICEXPPROPERTYDATA).get(spell2.toString())).split(",");
					
					int requiredLevelToCast = Integer.valueOf(MagicPropertiesFromList[1]);
					int mpCost = Integer.valueOf(MagicPropertiesFromList[2]);
					int currentMP = ExperienceSystemHandler.getCurrentPlayerMP(player);
					
					boolean hasEnoughMP = (currentMP >= mpCost);
					boolean meetsLevelReq = (currentLevel >= requiredLevelToCast);
					
					if (meetsLevelReq && hasEnoughMP){
						
						handleSpell(spell2, player, e.getAction(), currentExp, currentLevel);
		        		//if handled spell returns true VvV
		        		/**if (handleSpell(spell2, player, e.getAction())){
		        			handleExpAndMP(spell2, currentExp, currentLevel, player);
		        		}	
						**/
					}else if (!meetsLevelReq){
						player.sendMessage(ChatColor.RED + "You need to be level " + String.valueOf(requiredLevelToCast) + " to cast " + spell2.toString());
					}else if (!hasEnoughMP){
						player.sendMessage(ChatColor.RED + "You need to have atleast " + String.valueOf(mpCost) + "MP to cast " + spell2.toString());
					}
	        		
	        	}else if(isFirstClick.get(player.getName()) == null || isFirstClick.get(player.getName()) == true){
	    			player.sendMessage(ChatColor.RED + "Please select a spell first.");
	        	}
	        	
		}
		
		}
	}	
	
	

	
	private void handleSpell(Spell spell, Player player, Action action, double currentExp, int currentLevel){
		
		String worldName = player.getWorld().getName();
		String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get(spell.toString().toUpperCase())).split(",");
		List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
		int radius = !(data[in.indexOf("Radius")]).equals("null") ? Integer.valueOf(data[in.indexOf("Radius")]):-1;
		
		if (radius != -1){
			testPvP(player, SpellHandler.getTarget(player, radius), spell, action, currentExp, currentLevel);
		}else{
			//perform spell
			if (performSpell(spell, player, action)){
				handleExpAndMP(spell, currentExp, currentLevel, player);
			}
		}
	}
	
	
	//** OTHER METHODS **
	private String getSelectedSpells(ItemMeta spellBookMeta) {
			String sspell1 = "", sspell2 = "";
			ArrayList<String> lores = new ArrayList<String>(spellBookMeta.getLore());
			
			//check the pages on a properly formatted book
			if (lores.size() >= 1 && !lores.get(0).isEmpty()){
				sspell1 = lores.get(0);
				
			}
			if (lores.size() > 1 && !lores.get(1).isEmpty()){
				sspell2 = lores.get(1);
			}
		return (sspell1 + ":" + sspell2);
	}
	
	//TODO: to add a spell, add it to: config, enum, case, main, 
	public static enum Spell{
		BONESTOAPPLES, SNARE, SUPERHEAT, 
		ENTANGLE, WINDBOLT, SUFFOCATE, FIREBOLT, SHADOWBOLT,
		REGENERATION, WINDSURGE, FIREBARRAGE, WINDBARRAGE,
		EYELESS, CONFUSE, BIND, CURSE, LIGHTNINGBOLT, LIGHTNINGBARRAGE,
		VULNERABILITY, HUMIDIFY, NIGHTVISION, GILLS, VANISH, DECAY, POISON,
		HEAL, HEALOTHER, HEALGROUP, HASTE, RESISTFIRE
	}
	
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onMagicDamageEntity(EntityDamageByEntityEvent e){
		String worldName = ExperienceSystemHandler.checkActiveWorld(e.getEntity().getWorld().getName());
		
		if(RPGSkills.SpellProjectileUUIDs != null && RPGSkills.SpellProjectileUUIDs.get(e.getEntity().getUniqueId()) != null){
			if (RPGSkills.ProjectileInt.containsKey(e.getDamager().getUniqueId()) &&
					RPGSkills.ProjectileInt.get(e.getDamager().getUniqueId()) < 1){
				String spell = RPGSkills.SpellProjectileUUIDs.get(e.getDamager().getUniqueId());
				String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get(spell)).split(",");
				
				List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
				
				double damage = Double.valueOf(data[in.indexOf("Damage")]);
				int fireTicks = (Integer.valueOf(data[in.indexOf("Fire Ticks")])) != null ? Integer.valueOf(data[in.indexOf("Fire Ticks")]):0;
				e.setDamage(damage);
				e.getEntity().setFireTicks(fireTicks);
				int i = RPGSkills.ProjectileInt.get(e.getDamager().getUniqueId());
				RPGSkills.ProjectileInt.put(e.getDamager().getUniqueId(), i++);
				
			}else if (RPGSkills.ProjectileInt.containsKey(e.getDamager().getUniqueId()) &&
				    RPGSkills.ProjectileInt.get(e.getDamager().getUniqueId()) == 1){
				String spell = RPGSkills.SpellProjectileUUIDs.get(e.getDamager().getUniqueId());
				RPGSkills.ProjectileInt.remove(e.getDamager().getUniqueId());
				String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get(spell)).split(",");
				List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
				int fireTicks = (Integer.valueOf(data[in.indexOf("Fire Ticks")])) != null ? Integer.valueOf(data[in.indexOf("Fire Ticks")]):0;
				double damage = Double.valueOf(data[in.indexOf("Damage")]);
				e.setDamage(damage);
				e.getEntity().setFireTicks(fireTicks);
				
				
			}
	}
  }
	
	private boolean DoNearbyEntChecks(Player player, double radius, String worldName){
		for (Entity entity : player.getNearbyEntities(radius, radius, radius)){
			if (entity instanceof Creature || (entity instanceof Player && entity != player)){
        		if (entity instanceof Tameable){
        			Tameable tam = (Tameable)entity;
        			if (tam.getOwner() != null && tam.getOwner().equals((AnimalTamer)player)){
        				return false;
        			}
        		}
				return true;
			}
			
		}
		return false;
	}
	
	public void testPvP(Player damager, Entity damagee, Spell spell, Action action, double currentExp, int currentLevel){
		if (damagee instanceof Player){
			String worldName = damager.getWorld().getName();
			String[] MSPdata = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get(spell.toString().toUpperCase())).split(",");
			List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
			Boolean useInNonPvP = Boolean.valueOf(MSPdata[in.indexOf("Use in non-PvP")]);
			
			if (MSPdata[in.indexOf("Use in non-PvP")].equals("null") || useInNonPvP){
				//perform spell
				if (performSpell(spell, damager, action)){
					handleExpAndMP(spell, currentExp, currentLevel, damager);
				}
			}else{
				RPGSkills.MagicTestPvPMap.put(damagee.getUniqueId(), spell.toString() + "," + action.toString() + "," + String.valueOf(currentExp) + "," + String.valueOf(currentLevel));
				((Player)damagee).damage(1.0, damager);
			}
		}else{
			//perform spell
			if (performSpell(spell, damager, action)){
				handleExpAndMP(spell, currentExp, currentLevel, damager);
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void checkPVP(EntityDamageByEntityEvent e){
		String worldName = e.getDamager().getWorld().getName();

		boolean isTarget = RPGSkills.MagicTestPvPMap.containsKey(e.getEntity().getUniqueId());

		if (isTarget){
			String[] data = RPGSkills.MagicTestPvPMap.get(e.getEntity().getUniqueId()).split(",");
			Spell spell = Spell.valueOf(data[0]);
			Player player = (Player) e.getDamager();
			Action action = Action.valueOf(data[1]);
			double currentExp = Double.valueOf(data[2]);
			int currentLevel = Integer.valueOf(data[3]);
			String[] MSPdata = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get(spell.toString().toUpperCase())).split(",");
			List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
			boolean PvPEnabled = !e.isCancelled();
			boolean useInNonPvP = Boolean.valueOf(MSPdata[in.indexOf("Use in non-PvP")]);
			RPGSkills.MagicTestPvPMap.remove(e.getEntity().getUniqueId());
			
			if (!useInNonPvP && !PvPEnabled){
				player.sendMessage(ChatColor.RED + "You cannot cast " + spell.toString() + " in a non-pvp zone.");
				return;
			}else if (PvPEnabled && !useInNonPvP){
				//perform spell
				if (performSpell(spell, player, action)){
					handleExpAndMP(spell, currentExp, currentLevel, player);
				}
			}
		}
	}
	
	private void handleExpAndMP(Spell spell, double currentExp, int currentLevel, Player player){
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		String[] MagicPropertiesFromList = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICEXPPROPERTYDATA).get(spell.toString())).split(",");
		
		UUID playerUUID = player.getUniqueId();
		int expGainedFromCasting = Integer.valueOf(MagicPropertiesFromList[0]);
		int mpCost = Integer.valueOf(MagicPropertiesFromList[2]);
		int currentMP = ExperienceSystemHandler.getCurrentPlayerMP(player);
		
		//RPGSkills.playerMagicExp.put(playerName, (currentExp + expGainedFromCasting));
		//RPGSkills.playerMagicPoints.put(playerName, (currentMP - mpCost));
		RPGSkills.worldExp.get(worldName).get(playerUUID).put(ThisSkill, (currentExp + expGainedFromCasting));
		RPGSkills.playerMagicPoints.get(worldName).put(playerUUID, (currentMP - mpCost));
		
		
		ExperienceSystemHandler.CheckIfPlayerLeveled(worldName, player.getUniqueId(), currentLevel, ThisSkill);
	}
	
	private boolean performSpell(Spell spell, Player player, Action action){
		String worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
		switch (spell){
		case CURSE:
			if (SpellHandler.doCurse(player,worldName)){
				return true;
			}
			return false;
			
		case CONFUSE:
			if (SpellHandler.doConfuse(player, worldName)){
				return true;
			}
			return false;
			
		case EYELESS:
			if (SpellHandler.doEyeless(player, worldName)){
				return true;
			}
			return false;
			
		case BIND:
			if (SpellHandler.doBind(player, worldName)){
				return true;
			}
			return false;
			
		case BONESTOAPPLES:
			if (SpellHandler.doBonesToApples(player, worldName)){
				return true;
			}
			return false;
			
		case SUPERHEAT:
			player.sendMessage(ChatColor.GREEN + "Left-click the item in hand that you wish to smelt.");
			isFirstClick.put(player.getName(), false);
			FirstClickActionType.put(player.getName(), action);
			return false;
			
		case ENTANGLE:
			if (SpellHandler.doEntangle(player, worldName)){
				return true;
			}
			return false;
			
		case SNARE:
			if (SpellHandler.doSnare(player, worldName)){
				return true;
			}
			return false;
		case WINDSURGE:
			if (SpellHandler.doWindSurge(player, worldName)){
				return true;
			}
			return false;
			
		case WINDBOLT:
			if (SpellHandler.doWindBolt(player, worldName)){
				return true;
			}
			return false;
			
		case FIREBOLT:
			SpellHandler.doFireBolt(player, worldName);
			return true;
		case FIREBARRAGE:
		{
			String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("FIREBARRAGE")).split(",");
			List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
			double radius = Double.valueOf(data[in.indexOf("Radius")]);
			
			if (DoNearbyEntChecks(player, radius, worldName)){
				SpellHandler.doFireBarrage(player, worldName);
				return true;
			}
			return false;
		}
		
			//**WINDBARRAGE**
		case WINDBARRAGE:
		{
			String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("WINDBARRAGE")).split(",");
			List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
			double radius = Double.valueOf(data[in.indexOf("Radius")]);
			
			if (DoNearbyEntChecks(player, radius, worldName)){
				SpellHandler.doWindBarrage(player, worldName);
				return true;
			}
			return false;
		}
			
		case SHADOWBOLT:
			SpellHandler.doShadowBolt(player, worldName);
				return true;
				
		case SUFFOCATE:
			if (SpellHandler.doSuffocate(player, worldName)){
				return true;
			}
			return false;
		case DECAY:
			if (SpellHandler.doDecay(player, worldName)){
				return true;
			}
			return false;
		case GILLS:
		{
			SpellHandler.doGills(player, worldName);
			return true;
		}
		case HASTE:
		{
			SpellHandler.doHaste(player, worldName);
			return true;
		}
		case HEAL:
		{
			SpellHandler.doHeal(player, worldName);
			return true;
		}
		case HEALGROUP:
		{
			String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("HEALGROUP")).split(",");
			List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
			double radius = Double.valueOf(data[in.indexOf("Radius")]);
			
			if (DoNearbyEntChecks(player, radius, worldName)){
				SpellHandler.doHealGroup(player, worldName);
				return true;
			}
			return false;
		}
		case HEALOTHER:
			if (SpellHandler.doHealOther(player, worldName)){
				return true;
			}
			return false;
		case HUMIDIFY:
			if (SpellHandler.doHumidify(player, worldName)){
				return true;
			}
			return false;
		case LIGHTNINGBARRAGE:
		{
			String[] data = ((String) RPGSkills.configData.get(worldName).get(ConfigType.MAGICSPELLPROPERTYDATA).get("LIGHTNINGBARRAGE")).split(",");
			List<String> in = Arrays.asList(RPGSkills.SpellPropertyTypes);
			double radius = Double.valueOf(data[in.indexOf("Radius")]);
			if (DoNearbyEntChecks(player, radius, worldName)){
				SpellHandler.doLightningBarrage(player, worldName);
				return true;
			}
			return false;
		}
			
		case LIGHTNINGBOLT:
			if (SpellHandler.doLightningBolt(player, worldName)){
				return true;
			}
			return false;
			
		case NIGHTVISION:
		{
			SpellHandler.doNightVision(player, worldName);
			return true;
		}
				
		case POISON:
			if (SpellHandler.doPoison(player, worldName)){
				return true;
			}
			return false;
		case REGENERATION:
		{
			SpellHandler.doRegeneration(player, worldName);
			return true;
		}
		case RESISTFIRE:
		{
			SpellHandler.doResistFire(player, worldName);
			return true;
		}
		case VANISH:
		{
			SpellHandler.doVanish(player, worldName);
			return true;
		}
		case VULNERABILITY:
			if (SpellHandler.doVulnerability(player, worldName)){
				return true;
			}
			return false;
		default:
			player.sendMessage(ChatColor.RED + "Please select a spell first.");
			return false;
		}
	}
}
