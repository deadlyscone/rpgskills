package me.deadlyscone.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import me.deadlyscone.enums.ArrowEffect;
import me.deadlyscone.enums.ArrowType;
import me.deadlyscone.otherhandlers.*;
import me.deadlyscone.skillhandlers.*;
import me.deadlyscone.skillhandlers.MagicHandler.Spell;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class RPGSkills extends JavaPlugin implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -825676998082391488L;
	
	File pluginFolder = getDataFolder();
    File dataFolder = new File(getDataFolder(), "/data");
    File converter = new File(getDataFolder(), "/converter.yml");
    File usePermsFile = new File(getDataFolder(), "/permissions.yml");
    File configFile = new File(getDataFolder(), "/config.yml");
    File messagesFile = new File(getDataFolder(), "/messages.yml");
    
    File worldsFolder = new File(getDataFolder(), "/worlds");
    File wo = new File(getDataFolder(), "/worlds/worlds.yml");
    File ad = new File(getDataFolder(), "/antidrops.yml");
    File reci = new File(getDataFolder(), "/recipes.yml");
    File ppb = new File(dataFolder, "/ppb.dat");
    File pmp = new File(dataFolder, "/pmp.dat");
    File mpu = new File(dataFolder, "/mpu.dat");
    File mpb = new File(dataFolder, "/mpb.dat");
    File userDat = new File(dataFolder, "/user.dat");
    
    
    //General**
    //>world / player / skill = experience
    public static HashMap<String, HashMap<UUID,HashMap<String,Double>>> worldExp = new HashMap<String, HashMap<UUID,HashMap<String,Double>>>();
	public static HashMap<String, HashMap<ConfigType,HashMap<Object,Object>>> configData = new HashMap<String, HashMap<ConfigType,HashMap<Object,Object>>>();
	public static ArrayList<String> ActiveWorldsData = new ArrayList<String>();
	public static ArrayList<String> ActiveWorldNames = new ArrayList<String>();
	public static ArrayList<String> ConverterWorlds = new ArrayList<String>();
	public static HashMap<UUID, UUID> BreedingEventData = new HashMap<UUID, UUID>();
	private ArrayList<Integer> magicRegenTaskID = new ArrayList<Integer>();
	public static HashMap<UUID,Integer> ProjectileInt = new HashMap<UUID,Integer>();
	public static HashMap<EntityType, String> EggCraftingRecipeData = new HashMap<EntityType, String>();
	public static HashMap<ArrowType, String> FletchingRecipeData = new HashMap<ArrowType, String>();
	public static HashMap<UUID, Boolean> showStatsBoard = new HashMap<UUID, Boolean>();
	public static HashMap<MessageType, String> MessageMap = new HashMap<MessageType, String>();
	public static HashMap<Spell, String> SpellGUIMap = new HashMap<Spell, String>();
	public static HashMap<UUID, String> MagicTestPvPMap = new HashMap<UUID, String>();
	public static int MagicWandID;
	
	public static HashMap<String, HashMap<UUID, Integer>> playerMagicPoints = new HashMap<String, HashMap<UUID, Integer>>();
    public static HashMap<String, HashMap<String,Double>> skillGainFactor = new HashMap<String, HashMap<String,Double>>();
    public static HashMap<String, HashMap<String,Integer>> skillMinLevel = new HashMap<String, HashMap<String,Integer>>();
    public static HashMap<String, HashMap<String,Integer>> skillMaxLevel = new HashMap<String, HashMap<String,Integer>>();
    public static HashMap<String, HashMap<String,Boolean>> isSkillEnabled = new HashMap<String, HashMap<String, Boolean>>();
    
    public static String newVersion = new String();
	String[] attackEntities = {"Player", "Mob"};
	String[] extendedAttackEntities = {"Bat", "Blaze","CaveSpider","Chicken","Cow","Creeper","ElderGuardian","EnderDragon","Enderman","Endermite","Ghast"
			,"Guardian","Horse","IronGolem","MagmaCube","Mooshroom","Ocelot","Pig","Rabbit","Sheep","Silverfish","Skeleton","Slime","SnowGolem","Spider",
			"Squid","Villager","Witch","Wither","WitherSkeleton","Wolf","Zombie","ZombiePigman"};
	
    public static boolean usePerms, isAntiDropsEnabled, UseJoinLeave, UseRPGSkillsPlayerListName, UseSkillPane;
    

	
	//**DO NOT change order of these elements**
    public static String[] skills = {"attack", "archery", "defense", "excavation", "fishing", "fletching", "mining",
    		"woodcutting", "magic", "farming", "unarmed", "breeding", "summoning", "eggcrafting"};
    String[] Tools = {"Wood", "Stone", "Iron", "Gold", "Diamond"};
    String[] Armors = {"Leather", "Chainmail", "Iron", "Gold", "Diamond"};
    
    //Converter**
    private boolean isConverting;
    private int mmoCap;
    
    //AntiDrops**
	public static HashMap<UUID, String> SpellProjectileUUIDs;
	public static String[] SpellPropertyTypes = {"Damage", "Push", "Lift", "Duration", "Bones Required", "Fire Ticks", 
		"Level", "Material", "Radius", "Yield", "Use in non-PvP"};
	public static String[] ArrowEffectPropertyTypes = {"Damage", "Duration", "Fire Ticks", "Magnitude", "Level"};

    public static String[] MagicSpells = {"Wind Bolt", "Snare", "Bones To Apples", "Fire Bolt", "Suffocate", 
		"Entangle", "Shadow Bolt", "Regeneration", "SuperHeat", "Wind Surge",
		"Fire Barrage", "Wind Barrage", "Bind", "Eyeless", "Confuse", "Curse", "Lightning Bolt", "Lightning Barrage",
		"Vulnerability", "Humidify", "Night Vision", "Gills", "Vanish", "Decay", "Poison", "Heal", "Heal Other",
		"Heal Group", "Haste", "Resist Fire"};
    String[] WoodTypes = {"Oak", "Birch", "Spruce", "Jungle", "Acacia", 
	"Dark Oak"};
    String[] MiningBlocks = {"Stone", "Netherrack", "Quartz_Ore", "Coal_Ore", "Iron_Ore", 
    		"Gold_Ore", "Redstone_Ore", "Lapis_Ore", "Emerald_Ore", "Diamond_Ore", };
    String[] ExcavBlocks = {"Grass", "Dirt", "Sand", "Clay", "Soil", 
    		"Gravel", "Mycel", "Soul_Sand"};
    String[] FarmBlocks = {"Crops", "Melon_Block", "Melon_Stem", "Pumpkin_Stem", "Cocoa", "Carrot", "Potato", "Pumpkin",
    		"Sugar_Cane_Block", "Nether_Warts", "Cactus", "Brown_Mushroom", "Red_Mushroom"};
	String[] BreedingMobTypes = {"Chicken", "Cow", "Horse", "Mushroom_Cow", "Ocelot", "Pig", "Rabbit", "Wolf"};
	String[] FletchingArrowTypes = {"Generic", "Wood", "Iron", "Gold", "Emerald", "Diamond", "Explosive", "Frost", "Elemental", "Odisiac", "Poison", 
		  "Electric", "Glass", "Dragon", "Enderbone"};
	@Override
	public void onEnable() {
		checkVersion();
		CheckFiles();
		System.out.println("[RPGSkills] Parsing files [...]");
		ConfigParser.scanFiles();
		LoadValues();
		if (OldExpMapConverter.convertOldSystem()){
			System.out.println("[RPGSkills] Sucessfully migrated to UUID's and new RPGSkills file system!");
			worldExp = OldExpMapConverter.convertedWorldExp;
		}
		checkWorldExpMap();
		InitializeClasses();
		runMPRegenScheduler();
		checkConverter();
		addCustomRecipes();
		
	}


	@Override
	public void onDisable() {
		System.out.println("[RPGSkills] Saving data [...]");
		SaveValues();
		System.out.println("[RPGSkills] Saved data [DONE]");
		
	}
	
	public void CheckFiles(){
		
		//independent files/folders
        System.out.println("[RPGSkills] Checking Files [...]");
        if(!pluginFolder.exists()){
            pluginFolder.mkdir();
            System.out.println("[RPGSkills] Created Plugin Folder!");
        }
        if (ad.exists()){
        	ad.delete();
        }
        if (usePermsFile.exists()){
        	usePermsFile.delete();
        }
        if(!dataFolder.exists()){
            dataFolder.mkdir();
        }
        if(!worldsFolder.exists()){
            worldsFolder.mkdir();
            System.out.println("[RPGSkills] Created Worlds Folder!");
        }
		if (!userDat.exists()){
			try{
				userDat.createNewFile();
			}catch (IOException e){
				e.printStackTrace();
			}
		}
		if (!configFile.exists()){
            exportResource(RPGSkills.class.getResource("/config.yml"), configFile);
		}
		if (!messagesFile.exists()){
            exportResource(RPGSkills.class.getResource("/messages.yml"), messagesFile);
		}
		if (!converter.exists()){
            saveResource("converter.yml", true);
            writeConverterYMLWorlds();
		}
		if (!reci.exists()){
            saveResource("recipes.yml", true);
		}
		if (!wo.exists()){
            saveResource("worlds/worlds.yml", true);
            writeWorldsYML();
		}
		if (!ppb.exists()){
			try{
				ppb.createNewFile();
			}catch (IOException e){
				e.printStackTrace();
			}
		}
		if (!pmp.exists()){
			try{
				pmp.createNewFile();
			}catch (IOException e){
				e.printStackTrace();
			}
		}
		if (!mpb.exists()){
			try{
				mpb.createNewFile();
			}catch (IOException e){
				e.printStackTrace();
			}
		}
		if (!mpu.exists()){
			try{
				mpu.createNewFile();
			}catch (IOException e){
				e.printStackTrace();
			}
		}
		
		//per world dependent files/folders
		for(World world : getServer().getWorlds()){
			File currentWorld = new File(worldsFolder, "/" + world.getName());
			if (!currentWorld.exists()){
				currentWorld.mkdir();
			}
        	for (int i = 0; i < skills.length; i++){
        	    File skillYML = new File(getDataFolder(), "/worlds/" + world.getName() + "/" + skills[i] +".yml");
        	    
        		if (!skillYML.exists()){
                    URL resourceURL = RPGSkills.class.getResource("/skills/"+ skills[i] +".yml");
                    exportResource(resourceURL, skillYML);
        		}
        	}
		}
        System.out.println("[RPGSkills] Checking Files [DONE]");
	}
	
	@SuppressWarnings("unchecked")
	public void LoadValues(){
		File dataFilePaths = new File(dataFolder, "/user.dat");
		try{
			FileInputStream fis = new FileInputStream(dataFilePaths);
			ObjectInputStream ois = new ObjectInputStream(fis);
			worldExp = (HashMap<String, HashMap<UUID, HashMap<String, Double>>>) ois.readObject();
			ois.close();
		}catch(EOFException e){
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    File ppb = new File(dataFolder, "/ppb.dat");
	    File pmp = new File(dataFolder, "/pmp.dat");
	    File mpu = new File(dataFolder, "/mpu.dat");
	    File mpb = new File(dataFolder, "/mpb.dat");
		//gather the player placed blocks for Excavating, Farming, and mining
		try{
			FileInputStream fis = new FileInputStream(ppb);
			ObjectInputStream ois = new ObjectInputStream(fis);
			BlockPlaceHandler.PlayerPlacedBlockLocations = (ArrayList<String>)ois.readObject();
			ois.close();
		}catch(EOFException e){
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//gather magic projectile UUID's
		try{
			FileInputStream fis = new FileInputStream(mpu);
			ObjectInputStream ois = new ObjectInputStream(fis);
			SpellProjectileUUIDs = (HashMap<UUID,String>)ois.readObject();
			ois.close();
		}catch(EOFException e){
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//gather magic projectile booleans
		try{
			FileInputStream fis = new FileInputStream(mpb);
			ObjectInputStream ois = new ObjectInputStream(fis);
			ProjectileInt = (HashMap<UUID,Integer>)ois.readObject();
			ois.close();
		}catch(EOFException e){
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//gather the player magic points
		try{
			FileInputStream fis = new FileInputStream(pmp);
			ObjectInputStream ois = new ObjectInputStream(fis);
			playerMagicPoints = (HashMap<String, HashMap<UUID, Integer>>)ois.readObject();
			ois.close();
		}catch(EOFException e){
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//gather the general data**
		for (World world : getServer().getWorlds()){
			isSkillEnabled.put(world.getName(), new HashMap<String,Boolean>());
			skillGainFactor.put(world.getName(), new HashMap<String,Double>());
			skillMaxLevel.put(world.getName(), new HashMap<String,Integer>());
			skillMinLevel.put(world.getName(), new HashMap<String,Integer>());
		for (int i = 0; i < skills.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[i] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);

			
			//get if the skill is enabled
			boolean bool = customSkillsConfig.getBoolean("Enabled");
			isSkillEnabled.get(world.getName()).put(skills[i], bool);
			
			//gain factor for each skill
			double GainFactor = customSkillsConfig.getDouble("Gain Factor");

			skillGainFactor.get(world.getName()).put(skills[i], GainFactor);
			
			//max level for each skill
			int maxLevel = customSkillsConfig.getInt("Level Cap");

			skillMaxLevel.get(world.getName()).put(skills[i], maxLevel);
			
			//min level for each skill
			int minLevel = customSkillsConfig.getInt("Starting Level");

			skillMinLevel.get(world.getName()).put(skills[i], minLevel);
			
			//gather the data for each skill
			
		}
		///---------START per skill info--------------\\\
		initializeConfigMapValues(world.getName());
		
		//++++MINING++++\\
		//gets the mining tool requirements
		for (int i = 0; i < Tools.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[6] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			int level = customSkillsConfig.getInt("Tool Levels." + Tools[i] + ".Level");
			configData.get(world.getName()).get(ConfigType.TOOLLEVELS).put(Material.getMaterial(Tools[i].toUpperCase() + "_PICKAXE"), level);
			
		}
		
		//gets 'Blocks' data for mining
		for (int i = 0; i < MiningBlocks.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[6] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			String data = customSkillsConfig.getString("Blocks." + MiningBlocks[i]);
			//miningBlocksMap.put(Material.getMaterial(MiningBlocks[i].toUpperCase()), data);
			configData.get(world.getName()).get(ConfigType.MININGBLOCKSMAP).put(Material.getMaterial(MiningBlocks[i].toUpperCase()), data);
		}
		
		//gets 'Blocks' data for excavating
		for (int i = 0; i < ExcavBlocks.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[3] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			String data = customSkillsConfig.getString("Blocks." + ExcavBlocks[i]);
			//ExcavBlocksMap.put(Material.getMaterial(ExcavBlocks[i].toUpperCase()), data);
			configData.get(world.getName()).get(ConfigType.EXCAVBLOCKSMAP).put(Material.getMaterial(ExcavBlocks[i].toUpperCase()), data);
		}
		
		//gets 'Mob Type Data' data for breeding
		for (int i = 0; i < BreedingMobTypes.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[11] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			String data = customSkillsConfig.getString("Mob Types." + BreedingMobTypes[i]);
			//BreedingMobTypeData.put(EntityType.valueOf(BreedingMobTypes[i].toUpperCase()), data);
			configData.get(world.getName()).get(ConfigType.BREEDINGMOBTYPEDATA).put(EntityType.valueOf(BreedingMobTypes[i].toUpperCase()), data);
		}
		
		//gets 'Mob Type Data' data for summoning
		for (int i = 0; i < BreedingMobTypes.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[12] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			String data = customSkillsConfig.getString("Summoning Properties." + BreedingMobTypes[i]);
			//SummoningPropertyData.put(EntityType.valueOf(BreedingMobTypes[i].toUpperCase()), data);
			configData.get(world.getName()).get(ConfigType.SUMMONINGPROPERTYDATA).put(EntityType.valueOf(BreedingMobTypes[i].toUpperCase()), data);
		}
	    //gets 'Mob Type Data' data for eggcrafting
		for (int i = 0; i < BreedingMobTypes.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[13] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			String data = customSkillsConfig.getString("Level Properties." + BreedingMobTypes[i]);
			configData.get(world.getName()).get(ConfigType.EGGCRAFTINGPROPERTYDATA).put(EntityType.valueOf(BreedingMobTypes[i].toUpperCase()), data);
		}
		
		// recipes.yml 
		for (int i = 0; i < BreedingMobTypes.length; i++){
			File customYmlSkills = reci;
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			String data = customSkillsConfig.getString("EggCrafting." + BreedingMobTypes[i]);
			EggCraftingRecipeData.put(EntityType.valueOf(BreedingMobTypes[i].toUpperCase()), data);
		}
		for (int i = 0; i < FletchingArrowTypes.length; i++){
			File customYmlSkills = reci;
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			String data = customSkillsConfig.getString("Fletching." + FletchingArrowTypes[i]);
			FletchingRecipeData.put(ArrowType.valueOf(FletchingArrowTypes[i].toUpperCase()), data);
		}
		
		//gets 'Blocks' data for farming
		for (int i = 0; i < FarmBlocks.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[9] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			int data = customSkillsConfig.getInt("Blocks." + FarmBlocks[i]);
			//FarmingBlocksMap.put(Material.getMaterial(FarmBlocks[i].toUpperCase()), data);
			configData.get(world.getName()).get(ConfigType.FARMINGBLOCKSMAP).put(Material.getMaterial(FarmBlocks[i].toUpperCase()), data);
		}
		
		//gets 'WOOD TYPES' data for woodcutting
		for (int i = 0; i < WoodTypes.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[7] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			String data = customSkillsConfig.getString("Wood Types." + WoodTypes[i]);
			//WoodTypeData.put(WoodTypes[i].toUpperCase(), data);
			configData.get(world.getName()).get(ConfigType.WOODTYPEDATA).put(WoodTypes[i].toUpperCase(), data);
		}
		//++++WOODCUTTING++++\\
		
		for (int i = 0; i < Tools.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[7] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			int level = customSkillsConfig.getInt("Tool Levels." + Tools[i] + ".Level");
			//ToolLevels.put(Material.getMaterial(Tools[i].toUpperCase() + "_AXE"), level);
			configData.get(world.getName()).get(ConfigType.TOOLLEVELS).put(Material.getMaterial(Tools[i].toUpperCase() + "_AXE"), level);
			
		}
		
		//++++FARMING++++\\
		//gets the farm tool modifiers
		for (int i = 0; i < Tools.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[9] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			double data = customSkillsConfig.getDouble("Hoe Yield Modifiers." + Tools[i]);
			//FarmingToolModifiers.put(Material.getMaterial(Tools[i].toUpperCase() + "_HOE"), data);
			configData.get(world.getName()).get(ConfigType.FARMINGTOOLMODIFIERS).put(Material.getMaterial(Tools[i].toUpperCase() + "_HOE"), data);
			
		}
		
		//++++EXCAVATION++++\\
		//gets the excav tool requirements
		for (int i = 0; i < Tools.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[3] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			int level = customSkillsConfig.getInt("Tool Levels." + Tools[i] + ".Level");
			//ToolLevels.put(Material.getMaterial(Tools[i].toUpperCase() + "_SPADE"), level);
			configData.get(world.getName()).get(ConfigType.TOOLLEVELS).put(Material.getMaterial(Tools[i].toUpperCase() + "_SPADE"), level);
			
		}
		
		//++++ATTACK++++\\
		
		for (int i = 0; i < Tools.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[0] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			int level = customSkillsConfig.getInt("Tool Levels." + Tools[i] + ".Level");
			//ToolLevels.put(Material.getMaterial(Tools[i].toUpperCase() + "_SWORD"), level);
			configData.get(world.getName()).get(ConfigType.TOOLLEVELS).put(Material.getMaterial(Tools[i].toUpperCase() + "_SWORD"), level);
			
			double damage = customSkillsConfig.getDouble("Damage Modifiers." + Tools[i] + ".Damage");
			//AttackDamageMap.put(Material.getMaterial(Tools[i].toUpperCase() + "_SWORD"), damage);
			configData.get(world.getName()).get(ConfigType.ATTACKDAMAGEMAP).put(Material.getMaterial(Tools[i].toUpperCase() + "_SWORD"), damage);
			
			
		}
		
		for (int i = 0; i < attackEntities.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[0] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			//AttackGlobalModifier = customSkillsConfig.getDouble("Global Modifier");
			//AttackExpPerEntity.put(attackEntities[i], customSkillsConfig.getInt("Experience." + attackEntities[i]));
			configData.get(world.getName()).get(ConfigType.ATTACKEXPPERENTITY).put(attackEntities[i], customSkillsConfig.getInt("Experience." + attackEntities[i]));
		}
		
		//++++DEFENSE++++\\
		for (int i = 0; i < Tools.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[2] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			int level = customSkillsConfig.getInt("Tool Levels." + Tools[i] + ".Level");
			//DefenseToolLevels.put(Material.getMaterial(Tools[i].toUpperCase() + "_SWORD"), level);
			configData.get(world.getName()).get(ConfigType.DEFENSETOOLLEVELS).put(Material.getMaterial(Tools[i].toUpperCase() + "_SWORD"), level);
			
			double negate = customSkillsConfig.getDouble("Negate Modifiers." + Tools[i] + ".Negate");
			//DefenseNegateMap.put(Material.getMaterial(Tools[i].toUpperCase() + "_SWORD"), negate);
			configData.get(world.getName()).get(ConfigType.DEFENSENEGATEMAP).put(Material.getMaterial(Tools[i].toUpperCase() + "_SWORD"), negate);
			
		}
		for(int i = 0; i < Armors.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[2] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			int level = customSkillsConfig.getInt("Armor Levels." + Armors[i] + ".Level");
			String[] ArmorType = {"HELMET", "CHESTPLATE", "LEGGINGS", "BOOTS"};
			
			for (String type : ArmorType){
				configData.get(world.getName()).get(ConfigType.DEFENSEARMORLEVELS).put(Material.getMaterial(Armors[i].toUpperCase() + "_" + type), level);
			}
			
		}
		
		//++++Magic++++\\
		for (int i = 0; i < MagicSpells.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[8] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			String data = customSkillsConfig.getString("Experience Properties." + MagicSpells[i]);
			//MagicExpPropertyData.put(MagicSpells[i].toUpperCase().replaceAll("\\s", ""), data);	
			configData.get(world.getName()).get(ConfigType.MAGICEXPPROPERTYDATA).put(MagicSpells[i].toUpperCase().replaceAll("\\s", ""), data);
			
		    String data1 = "";
			for (int i1 = 0; i1 < SpellPropertyTypes.length; i1++){
			data1 += (customSkillsConfig.getString("Spell Properties." + MagicSpells[i].toUpperCase() + "." + SpellPropertyTypes[i1]) + ",");
			}
			
			//MagicSpellPropertyData.put(MagicSpells[i].toUpperCase().replaceAll("\\s", ""), data1);
			configData.get(world.getName()).get(ConfigType.MAGICSPELLPROPERTYDATA).put(MagicSpells[i].toUpperCase().replaceAll("\\s", ""), data1);
		}
		
		//++++Archery++++\\
		for (int i = 0; i < attackEntities.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[1] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			//ArcheryGlobalModifier = customSkillsConfig.getDouble("Global Modifier");
			//ArcheryExpPerEntity.put(attackEntities[i], customSkillsConfig.getInt("Experience." + attackEntities[i]));
			configData.get(world.getName()).get(ConfigType.ARCHERYEXPPERENTITY).put(attackEntities[i], customSkillsConfig.getInt("Experience." + attackEntities[i]));
		}
		for (String arrowType : FletchingArrowTypes){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[1] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			configData.get(world.getName()).get(ConfigType.ARCHERYARROWDATA).put(ArrowType.valueOf(arrowType.toUpperCase()), customSkillsConfig.getString("Arrow Properties." + arrowType));
		}
		
		for(ArrowEffect effect : ArrowEffect.values()){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[1] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
		    String data1 = "";
			for(String str : ArrowEffectPropertyTypes){
				data1 += (customSkillsConfig.getString("ArrowEffect Properties." +  effect.toString().toUpperCase() + "." + str)) + ",";
			}
			configData.get(world.getName()).get(ConfigType.ARROWEFFECTDATA).put(effect, data1);
			
		}
		
		//++++Fletching++++\\
		for (String arrowType : FletchingArrowTypes){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[5] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			configData.get(world.getName()).get(ConfigType.FLETCHINGARROWDATA).put(ArrowType.valueOf(arrowType.toUpperCase()), customSkillsConfig.getString("Level Properties." + arrowType));
		}
		
		//++++Unarmed++++\\
		for (int i = 0; i < attackEntities.length; i++){
			File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[10] +".yml");
			FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
			
			//UnarmedExpPerEntity.put(attackEntities[i], customSkillsConfig.getInt("Experience." + attackEntities[i]));
			configData.get(world.getName()).get(ConfigType.UNARMEDEXPPERENTITY).put(attackEntities[i], customSkillsConfig.getInt("Experience." + attackEntities[i]));
		}
		
		
		//++++SingularGetters++++\\
		
		//Defense**
		File customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[2] +".yml");
		FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		configData.get(world.getName()).get(ConfigType.DEFENSEBASEEXP).put(0, customSkillsConfig.getInt("Experience"));
		configData.get(world.getName()).get(ConfigType.DEFENSEGLOBALMODIFIER).put(0, customSkillsConfig.getDouble("Global Modifier"));
		configData.get(world.getName()).get(ConfigType.DEFENSEARMORLEVELSENABLED).put(0, customSkillsConfig.getBoolean("Armor Levels.Enabled"));
		
		//Attack**
		customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[0] +".yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		configData.get(world.getName()).get(ConfigType.ATTACKGLOBALMODIFIER).put(0, customSkillsConfig.getDouble("Global Modifier"));
		
		//Fletching**
		customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[5] +".yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		configData.get(world.getName()).get(ConfigType.FLETCHINGBASEEXP).put(0, customSkillsConfig.getInt("Experience"));
		configData.get(world.getName()).get(ConfigType.FLETCHINGBASEYIELD).put(0, customSkillsConfig.getInt("Base Yield"));
		
		//Magic**
		customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[8] +".yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		configData.get(world.getName()).get(ConfigType.MAGICBASEMP).put(0, customSkillsConfig.getInt("Magic Points.Base"));
		configData.get(world.getName()).get(ConfigType.MAGICGAINMP).put(0, customSkillsConfig.getInt("Magic Points.Level Gain"));
		configData.get(world.getName()).get(ConfigType.MAGICREGENRATE).put(0, customSkillsConfig.getInt("Magic Points.Regeneration Rate"));
		configData.get(world.getName()).get(ConfigType.MAGICREGENMP).put(0, customSkillsConfig.getInt("Magic Points.Regeneration Amount"));
		
		//Fishing**
		customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[4] +".yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		configData.get(world.getName()).get(ConfigType.FISHINGTREASURES).put(0, (ArrayList<String>) customSkillsConfig.getList("Treasure"));
		configData.get(world.getName()).get(ConfigType.FISHINGTREASURECHANCE).put(0, customSkillsConfig.getInt("Treasure Chance"));
		configData.get(world.getName()).get(ConfigType.FISHINGJUNK).put(0, (ArrayList<String>) customSkillsConfig.getList("Junk"));
		configData.get(world.getName()).get(ConfigType.FISHINGJUNKCHANCE).put(0, customSkillsConfig.getInt("Junk Chance"));
		configData.get(world.getName()).get(ConfigType.FISHINGVANILLAENABLED).put(0, customSkillsConfig.getBoolean("Use Vanilla"));
		configData.get(world.getName()).get(ConfigType.FISHINGVANILLAPARALLELENABLED).put(0, customSkillsConfig.getBoolean("Run Vanilla Parallel"));
		configData.get(world.getName()).get(ConfigType.FISHINGBASEEXP).put(0, customSkillsConfig.getInt("Experience"));
		
		//Archery**
		customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[1] +".yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
	    
		configData.get(world.getName()).get(ConfigType.ARCHERYGLOBALMODIFIER).put(0, customSkillsConfig.getDouble("Global Modifier"));
		//configData.get(world.getName()).get(ConfigType.ARCHERYBOWDAMAGE).put(0, customSkillsConfig.getDouble("Global Damage"));
	    
		//Woodcutting**
		customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[7] +".yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		configData.get(world.getName()).get(ConfigType.WOODCUTTINGDOUBLEDROPCHANCE).put(0, customSkillsConfig.getDouble("Double Drop Chance"));
		configData.get(world.getName()).get(ConfigType.WOODCUTTINGFORCEAXE).put(0, customSkillsConfig.getBoolean("Force Axe"));
		
		//Mining**
		customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[6] +".yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		configData.get(world.getName()).get(ConfigType.MININGDOUBLEDROPCHANCE).put(0, customSkillsConfig.getDouble("Double Drop Chance"));
		configData.get(world.getName()).get(ConfigType.ISSILKSPAWNERSENABLED).put(0, customSkillsConfig.getBoolean("Silk Spawners Enabled"));
		
		//Unarmed**
	    customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[10] +".yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		configData.get(world.getName()).get(ConfigType.UNARMEDGLOBALMODIFIER).put(0, customSkillsConfig.getDouble("Global Modifier"));
		
		//Excavation**
		customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[3] +".yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		configData.get(world.getName()).get(ConfigType.EXCAVALLOWTREASURES).put(0, customSkillsConfig.getBoolean("Allow Treasures"));
		configData.get(world.getName()).get(ConfigType.EXCAVSILKFARMLAND).put(0, customSkillsConfig.getBoolean("Silk Soil"));
		configData.get(world.getName()).get(ConfigType.EXCAVTREASURES).put(0, (ArrayList<String>) customSkillsConfig.getList("Treasures"));
		
		//Farming**
		customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[9] +".yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		configData.get(world.getName()).get(ConfigType.FARMINGAUTOREPLANTENABLED).put(0, customSkillsConfig.getBoolean("Auto-Replant Enabled"));
		configData.get(world.getName()).get(ConfigType.FARMINGTRIPLEDROPENABLED).put(0, customSkillsConfig.getBoolean("Triple Drop Enabled"));
		configData.get(world.getName()).get(ConfigType.FARMINGREPLANTCHANCE).put(0, customSkillsConfig.getDouble("Auto-Replant Chance"));
		configData.get(world.getName()).get(ConfigType.FARMINGDOUBLECHANCE).put(0, customSkillsConfig.getDouble("Double Drop Chance"));
		configData.get(world.getName()).get(ConfigType.FARMINGTRIPLECHANCE).put(0, customSkillsConfig.getDouble("Triple Drop Chance"));
		
		//Unarmed**
		customYmlSkills = new File(getDataFolder(), "/worlds/" + world.getName() + "/"+ skills[10] +".yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		configData.get(world.getName()).get(ConfigType.UNARMEDBASEDAMAGE).put(0, customSkillsConfig.getDouble("Base Damage"));
	}
		//Converter**
		File customYmlSkills = new File(getDataFolder(), "/converter.yml");
		FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		isConverting = customSkillsConfig.getBoolean("convert");
		mmoCap = customSkillsConfig.getInt("mmoCap");
		ConverterWorlds = (ArrayList<String>)(customSkillsConfig.getList("worlds"));
		
		//Config file**
		customYmlSkills = new File(getDataFolder(), "/config.yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		
		usePerms = customSkillsConfig.getBoolean("Skill Permissions") ? false : true;
		
		isAntiDropsEnabled = customSkillsConfig.getBoolean("Anti-Drops Enabled");
		UseJoinLeave = customSkillsConfig.getBoolean("Use Join/Leave Messages");
		UseRPGSkillsPlayerListName = customSkillsConfig.getBoolean("Use RPGSkills PlayerListName");
		MagicWandID = customSkillsConfig.getInt("Magic.Wand");
		UseSkillPane = customSkillsConfig.getBoolean("Use Skill Info Pane");
		for (String spell : MagicSpells){
			String data = customSkillsConfig.getString("Magic.Spell GUI." + spell);
			
			SpellGUIMap.put(Spell.valueOf(spell.replace(" ", "").toUpperCase()), data);
		}
		
		//Messages.yml
		
		customYmlSkills = new File(getDataFolder(), "/messages.yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		MessageMap.put(MessageType.LevelUp, ChatColor.translateAlternateColorCodes('&', customSkillsConfig.getString("Level-Up Message")));
		MessageMap.put(MessageType.Join, ChatColor.translateAlternateColorCodes('&', customSkillsConfig.getString("Join Message")));
		MessageMap.put(MessageType.MaxLevelAchieve, ChatColor.translateAlternateColorCodes('&', customSkillsConfig.getString("Level Max Achieved Message")));
		MessageMap.put(MessageType.ModLevelUp, ChatColor.translateAlternateColorCodes('&', customSkillsConfig.getString("Modify Level Message")));
		MessageMap.put(MessageType.ModAllLevelUp, ChatColor.translateAlternateColorCodes('&', customSkillsConfig.getString("ModifyAll Level Message")));
		MessageMap.put(MessageType.Leave, ChatColor.translateAlternateColorCodes('&', customSkillsConfig.getString("Leave Message")));
		
		
		//Active Worlds
		customYmlSkills = new File(getDataFolder(), "/worlds/worlds.yml");
		customSkillsConfig = YamlConfiguration.loadConfiguration(customYmlSkills);
		ActiveWorldsData = (ArrayList<String>) customSkillsConfig.getList("Active Worlds");
		for (String s : ActiveWorldsData){
			ActiveWorldNames.add(s.split(",")[0]);
		}
	}
	
	public void SaveValues(){
        ObjectOutputStream oos;
        	File userDatFilePath = new File(dataFolder, "/user.dat");
    		try {
    		    oos = new ObjectOutputStream(new FileOutputStream(userDatFilePath, false));
    	        oos.writeObject(worldExp);
    	        oos.flush();
    	        oos.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		
            try {
    			oos = new ObjectOutputStream(new FileOutputStream(pmp, false));
    	        oos.writeObject(playerMagicPoints);
    	        oos.flush();
    	        oos.close();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    		
        try {
			oos = new ObjectOutputStream(new FileOutputStream(ppb, false));
	        oos.writeObject(BlockPlaceHandler.PlayerPlacedBlockLocations);
	        oos.flush();
	        oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        try {
			oos = new ObjectOutputStream(new FileOutputStream(mpu, false));
	        oos.writeObject(SpellProjectileUUIDs);
	        oos.flush();
	        oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        try {
			oos = new ObjectOutputStream(new FileOutputStream(mpb, false));
	        oos.writeObject(ProjectileInt);
	        oos.flush();
	        oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void InitializeClasses() {
		new PopulatePlayerExperienceMap(this);
		new JoinLeaveHandler(this);
		new BlockPlaceHandler(this);
		new AntiDropsHandler(this);
		new PlayerWorldChangeHandler(this);
		new MiningHandler(this);
		new AttackHandler(this);
		new FletchingHandler(this);
		new FishingHandler(this);
		new MagicHandler(this);
		new SilkSpawnerHandler(this);
		new WoodcuttingHandler(this);
		new BlockExplosionHandler(this);
		new ArcheryHandler(this);
		new ExcavationHandler(this);
		new FarmingHandler(this);
		new BreedingHandler(this);
		new SummoningHandler(this);
		new EggCraftingHandler(this);
		new VersionHandler(this);
		new SpellGUIHandler(this);
		new WieldHandler(this);
	}
	
	@SuppressWarnings("deprecation")
	private void addCustomRecipes(){
		for (World world : getServer().getWorlds()){
			if(RPGSkills.isSkillEnabled.get(world.getName()).get("eggcrafting")){
			    System.out.println("[RPGSkills] Adding EggCrafting Recipes [...]");
			    for (String s : BreedingMobTypes) {
			        String[] data = ((String)EggCraftingRecipeData.get(EntityType.valueOf(s.toUpperCase()))).split(",");
			        ItemStack egg = new ItemStack(Material.MONSTER_EGG, 1, EntityType.valueOf(s.toUpperCase()).getTypeId());
			        ShapedRecipe expBottle = new ShapedRecipe(egg);
			        String row = new String("123,456,789");
			    	
			        for (int i = 0;i < 9;i++){
			        	if (Integer.valueOf(data[i]) == 0){
				        	String is = String.valueOf(i+1);
					        row = row.replace(is.charAt(0), ' ');
			        	}
			        }
			        
			        expBottle.shape(row.split(",")[0],row.split(",")[1],row.split(",")[2]);
			        
			        for (int i = 0;i < 9;i++){
			        	if(row.contains(String.valueOf(i+1))){
			        		String is = String.valueOf(i+1);
			        		if (Integer.valueOf(data[i]) == 5){
			        			expBottle.setIngredient(is.charAt(0), Material.getMaterial(Integer.valueOf(data[i])), (short)32767);
			        		}else{
					  	        expBottle.setIngredient(is.charAt(0), Material.getMaterial(Integer.valueOf(data[i])));
			        		}
			        	}
			        }
			        
			        getServer().addRecipe(expBottle);
			        
			      }
			    System.out.println("[RPGSkills] Adding EggCrafting Recipes [DONE]");
				break;
			}
			
		}
		for (World world : getServer().getWorlds()){
			if(RPGSkills.isSkillEnabled.get(world.getName()).get("fletching")){
			    System.out.println("[RPGSkills] Adding Custom Fletching Recipes [...]");
			    for (String s : FletchingArrowTypes) {
			    	if (s.equalsIgnoreCase("generic")){
			    		continue;
			    	}
			        String[] data = ((String)FletchingRecipeData.get(ArrowType.valueOf(s.toUpperCase()))).split(",");
			        ItemStack arrow = new ItemStack(Material.ARROW, 1);
			        
			        ItemMeta im = arrow.getItemMeta();
			        im.setDisplayName(s + " Arrow");
			        arrow.setItemMeta(im);
			        ShapedRecipe expBottle = new ShapedRecipe(arrow);
			        String row = new String("123,456,789");
			    	
			        for (int i = 0;i < 9;i++){
			        	if (Integer.valueOf(data[i]) == 0){
				        	String is = String.valueOf(i+1);
					        row = row.replace(is.charAt(0), ' ');
			        	}
			        }
			        
			        expBottle.shape(row.split(",")[0],row.split(",")[1],row.split(",")[2]);
			        
			        for (int i = 0;i < 9;i++){
			        	if(row.contains(String.valueOf(i+1))){
			        		String is = String.valueOf(i+1);
			        		if (Integer.valueOf(data[i]) == 5){
			        			expBottle.setIngredient(is.charAt(0), Material.getMaterial(Integer.valueOf(data[i])), (short)32767);
			        		}else{
					  	        expBottle.setIngredient(is.charAt(0), Material.getMaterial(Integer.valueOf(data[i])));
			        		}
			        	}
			        }
			        
			        getServer().addRecipe(expBottle);
			        
			      }
			    System.out.println("[RPGSkills] Adding Custom Fletching Recipes [DONE]");
				break;
			}
			
		}
	}
	
	private void runMPRegenScheduler(){
		//ONLY regenerate in the players current world if enabled.
		ArrayList<String> regenWorlds = new ArrayList<String>();
		for (String activeWorld : ActiveWorldNames){
			String worldName = ExperienceSystemHandler.checkActiveWorld(activeWorld);
			if (!regenWorlds.contains(worldName)){
				regenWorlds.add(worldName);
			}
		}
		for (final String worldName : regenWorlds){
			
			if (isSkillEnabled.get(worldName) != null && isSkillEnabled.get(worldName).get("magic")){
		        int regenRate = (Integer)configData.get(worldName).get(ConfigType.MAGICREGENRATE).get(0);
		        int taskID = 0;
		        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();

		        
				taskID = scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
		            @Override
		            public void run() {
		            	if (playerMagicPoints.get(worldName) != null){
		    	        Iterator<Entry<UUID, Integer>> it = playerMagicPoints.get(worldName).entrySet().iterator();
		            	//first get all applicable players and their max MP
		    	        while (it.hasNext()) {
							HashMap.Entry<UUID, Integer> pairs = (HashMap.Entry<UUID, Integer>)it.next();
							//System.out.println("KEY: " + pairs.getKey() + " VALUE: " + pairs.getValue());
							if (pairs.getKey() != null && pairs.getValue() != null){
							UUID player = pairs.getKey();
		    	        	int maxPlayerMP = ExperienceSystemHandler.getMaxPlayerMP(worldName, player);
		    	        	int currentPlayerMP = pairs.getValue();
		    	        	boolean canIncrement = (currentPlayerMP + (Integer)configData.get(worldName).get(ConfigType.MAGICREGENMP).get(0)) <= maxPlayerMP;
		    	        	if (canIncrement){
		    	        		pairs.setValue((currentPlayerMP + (Integer)configData.get(worldName).get(ConfigType.MAGICREGENMP).get(0)));
		    	        	}
		    	        }
		    	        }
		            }
		            }
		        }, 0L, (long)regenRate);
				magicRegenTaskID.add(taskID);
			}
			
		}
	}
	
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (label.equalsIgnoreCase("spellbook") && sender instanceof Player && sender.hasPermission("rpgskills.spellbook")){
			
			Player player = (Player) sender;
			SpellGUIHandler.displayGUI(player);
			return true;
		}
		
		if (label.equalsIgnoreCase("skill") && sender instanceof Player ) {
			Player player = (Player) sender;
			if ((args.length == 1 || args.length == 2) && Arrays.asList(skills).contains(args[0])){
				String worldName = args.length == 2 ? ExperienceSystemHandler.checkActiveWorld(args[1]) : ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
				if (getServer().getWorlds().contains(getServer().getWorld(worldName))){
					SidebarHandler.updatePlayer(player, args[0].toLowerCase(), true);
				return true;
				}else{
					player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GREEN + "World '" + ChatColor.RED + worldName + ChatColor.GREEN + "' does not exist.");
					return true;
				}
			}else if (args.length == 1 && args[0].equalsIgnoreCase("list")){
				String str = "";
				for (String s : skills){
					if (str.isEmpty()){
						str += s;
					}else{
						str += ", " + s;
					}
			}
				player.sendMessage(ChatColor.GREEN + "Skills:");
				player.sendMessage(ChatColor.RED + str.toUpperCase());
				return true;
		  }
			player.sendMessage(ChatColor.GREEN + "Proper usage: /skill [<skill> {world} : list]");
			return true;
		}
		
		if (label.equalsIgnoreCase("rpgskills") || label.equalsIgnoreCase("rs")&& sender instanceof Player ) {
			
			Player player = (Player) sender;
			if (args.length == 0){
				player.sendMessage(ChatColor.DARK_AQUA+"_________________[RPGSkills]_________________");
				player.sendMessage(ChatColor.AQUA + "Version: " + getDescription().getVersion());
				player.sendMessage(ChatColor.AQUA + "Created by: deadlyscone.");
				player.sendMessage(ChatColor.AQUA + "______________________________________________");
				
				
				player.sendMessage(ChatColor.GOLD + "---------------[Commands]---------------");
				if (player.hasPermission("rpgskills.modify")){
					player.sendMessage(ChatColor.RED + "/rs modify <PLAYER> <SKILL> <WORLD> <LEVEL>  -  " + ChatColor.GREEN + "modifies a skill level for the player");
				}
				if (player.hasPermission("rpgskills.modifyall")){
					player.sendMessage(ChatColor.RED + "/rs modifyall <PLAYER> <WORLD> <LEVEL>  -  " + ChatColor.GREEN + "modifies all skill levels for the player");
				}
				if (player.hasPermission("rpgskills.reload")){
					player.sendMessage(ChatColor.RED + "/rs reload  -  " + ChatColor.GREEN + "updates plugin with new config values");
				}
				player.sendMessage(ChatColor.RED + "/rs who <player> {world}  -  " + ChatColor.GREEN + "gives stats info on the specified player");
				player.sendMessage(ChatColor.RED + "/rs top <skill> [<page> : <world> {page}]  -  " + ChatColor.GREEN + "gives leaderboard info for the specific world");
				player.sendMessage(ChatColor.RED + "/rs scoreboard {clear}  -  " + ChatColor.GREEN + "shows you info on the current skill. Toggles the scoreboard. <Clears the current scoreboard.>");
				player.sendMessage(ChatColor.RED + "/spellbook  -  " + ChatColor.GREEN + "opens the spellbook GUI");
				player.sendMessage(ChatColor.RED + "/skill [<skill> {world} : list]  -  " + ChatColor.GREEN + "<Displays personal info for the specified skill>, lists the available skills.");
				player.sendMessage(ChatColor.GOLD + "---------------------------------------");
				return true;
			}
			if (args.length == 5 && player.hasPermission("rpgskills.modify") && args[0].equalsIgnoreCase("modify")){
				try{
				String playerName = args[1], skill = args[2];
				Player tPlayer = getServer().getPlayer(playerName);
				UUID tPlayerUUID = tPlayer != null ? tPlayer.getUniqueId() : null;
				String worldName = ExperienceSystemHandler.checkActiveWorld(args[3]);
				
				if (tPlayerUUID != null && Arrays.asList(skills).contains(skill) && getServer().getWorlds().contains(getServer().getWorld(worldName))){
				playerName = tPlayer.getName();
				HashMap<Integer, Double> levelExpMap = new HashMap<Integer, Double>(ExperienceSystemHandler.getBaseExpPerLevel(worldName, args[2]));
				Integer exp = new Integer(args[4].trim());
				Double newExp = (levelExpMap.get(exp) == null) ? Double.valueOf(skillMinLevel.get(worldName).get(skill)) : levelExpMap.get(exp);
				ExperienceSystemHandler.setPlayerExp(worldName, tPlayerUUID, skill, newExp);
				JoinLeaveHandler.updatePlayerPrefix(getServer().getPlayer(playerName));
				String skillModMessage = MessageMap.get(MessageType.ModLevelUp).replace("%sklvl%", String.valueOf(exp)).replace("%skill%", skill.toUpperCase());
				tPlayer.sendMessage(skillModMessage);
				return true;
				}else if (tPlayerUUID == null){
					player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GREEN + "Player '" + ChatColor.RED + playerName + ChatColor.GREEN + "' does not exist.");
					return true;
				}else if(!getServer().getWorlds().contains(getServer().getWorld(worldName))){
					player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GREEN + "World '" + ChatColor.RED + worldName + ChatColor.GREEN + "' does not exist.");
					return true;
				}
				
				}catch(Exception ex){
				ex.printStackTrace();
				}
			}
			
			if (args.length == 4 && player.hasPermission("rpgskills.modifyall") && args[0].equalsIgnoreCase("modifyall")){
				try{
					String playerName = args[1];
					Player tPlayer = getServer().getPlayer(playerName);
					UUID tPlayerUUID = tPlayer != null ? tPlayer.getUniqueId() : null;
					String worldName = ExperienceSystemHandler.checkActiveWorld(args[2]);
					
				if (tPlayerUUID != null && getServer().getWorlds().contains(getServer().getWorld(worldName))){
					Integer exp = new Integer(args[3].trim());
				for (String s: skills){
				HashMap<Integer, Double> levelExpMap = new HashMap<Integer, Double>(ExperienceSystemHandler.getBaseExpPerLevel(worldName, s));
				Double newExp = (levelExpMap.get(exp) == null) ? Double.valueOf(skillMinLevel.get(worldName).get(s)) : levelExpMap.get(exp);
				ExperienceSystemHandler.setPlayerExp(worldName, tPlayerUUID, s, newExp);
				JoinLeaveHandler.updatePlayerPrefix(tPlayer);
					}
				String skillModAllMessage = MessageMap.get(MessageType.ModAllLevelUp).replace("%sklvl%", String.valueOf(exp));
				tPlayer.sendMessage(skillModAllMessage);
				return true;
					}else if(!getServer().getWorlds().contains(getServer().getWorld(worldName))){
						player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GREEN + "World '" + ChatColor.RED + worldName + ChatColor.GREEN + "' does not exist.");
						return true;
					}else if (tPlayerUUID == null){
						player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GREEN + "Player '" + ChatColor.RED + playerName + ChatColor.GREEN + "' does not exist.");
						return true;
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}

			}
			
			if ((args.length == 3 || args.length == 2)){
				
				if (args[0].equalsIgnoreCase("who")){
					
					String worldName = args.length == 3 ? ExperienceSystemHandler.checkActiveWorld(args[2]) : ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
					UUID tPlayerUUID = null;
					String tPlayerName = "null";
					UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(args[1]));
					
					try {
						HashMap<String, UUID> map = new HashMap<String, UUID>(fetcher.call());
						
						//iterate over our returned players
						Iterator<Entry<String, UUID>> it = map.entrySet().iterator();
					    while (it.hasNext()) {
					        Map.Entry<String, UUID> pair = (Map.Entry<String, UUID>)it.next();
					        if (pair.getKey().equalsIgnoreCase(args[1])){
					        	tPlayerUUID = pair.getValue();
					        	tPlayerName = pair.getKey();
					        	break;
					        }
					    }
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					if (tPlayerUUID != null && getServer().getWorlds().contains(getServer().getWorld(worldName))){
						if (UseSkillPane){
							player.sendMessage(ChatColor.GRAY + "[RPGSkills] Gathering info...");
							SidebarHandler.showTargetStats(worldName, tPlayerUUID, player);
							return true;
						}else{
							player.sendMessage(ChatColor.DARK_AQUA + "------[" + ChatColor.YELLOW + tPlayerName + ChatColor.DARK_AQUA + "]------");
							player.sendMessage(ChatColor.GRAY + "CombatLevel: " + ChatColor.GREEN + String.valueOf(JoinLeaveHandler.getCombatLevel(worldName, tPlayerUUID)));
							player.sendMessage(ChatColor.GRAY + "World: " + ChatColor.GREEN + worldName);
							for (String skill : skills){
								int level = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, tPlayerUUID, skill);
								player.sendMessage(ChatColor.RED + skill.toUpperCase() + ": " + ChatColor.GREEN + String.valueOf(level));
							}
							return true;
						}
					}else if (tPlayerUUID == null){
						player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GREEN + "Player '" + ChatColor.RED + args[1] + ChatColor.GREEN + "' does not exist.");
					return true;
					}else if (!getServer().getWorlds().contains(getServer().getWorld(worldName))){
						player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GREEN + "World '" + ChatColor.RED + worldName + ChatColor.GREEN + "' does not exist.");
					return true;
					}
					
				}
				
			}
			// /rs top {skill} {world} {page}
			if (args.length > 1 && args.length < 5){
			if (args[0].equalsIgnoreCase("top")){
				
					int perPageCount = 15;
					String worldName = args.length > 2 ? ExperienceSystemHandler.checkActiveWorld(args[2]) : 
						ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
					String skill = args[1].toLowerCase();
					int dPage = args.length == 4 ? Integer.valueOf(args[3]) : args.length < 4 ? 1 : 1;

					boolean a2isWorld = getServer().getWorlds().contains(getServer().getWorld(worldName)), a2isPage = args.length == 4 ? StringUtils.isNumeric(args[3]) : true;
					
					if (a2isPage && !a2isWorld){
						worldName = ExperienceSystemHandler.checkActiveWorld(player.getWorld().getName());
						dPage = Integer.valueOf(args[2]);
					}
					
					
					if (Arrays.asList(skills).contains(skill) && getServer().getWorlds().contains(getServer().getWorld(worldName))){
						int pages = getLeaderPageCount(worldName, perPageCount);
						
						if (dPage <= pages){
								List<String> leaderList = getLeaderboardInfo(worldName, skill, (dPage * perPageCount), ((dPage * perPageCount)-perPageCount));
								Collections.reverse(leaderList);
								int rank = (dPage * perPageCount)-perPageCount;
								player.sendMessage(ChatColor.DARK_AQUA + "--------------[" + ChatColor.YELLOW + skill + ChatColor.DARK_AQUA + "]--------------");
								for (int i = 0;i < perPageCount && i < leaderList.size(); i++){
									String str = leaderList.get(i);
									String[] data = str.split(":");
									player.sendMessage(String.valueOf(rank+1) + ".  " + ChatColor.GRAY + data[0] + ":" + ChatColor.YELLOW + data[1]);
									rank++;
								}
								
								player.sendMessage(ChatColor.DARK_AQUA + "----------[ Page " + dPage + " of " + pages + " ]----------");
								return true;
							
							
						}else if (dPage > pages){
							player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GREEN + "Page '" + ChatColor.RED + dPage + ChatColor.GREEN + "' does not exist. Maximum pages are " + pages);
							return true;
						}
						
					}else if (!Arrays.asList(skills).contains(skill)){
						player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GREEN + "Skill '" + ChatColor.RED + args[1] + ChatColor.GREEN + "' does not exist.");
					return true;
					}else if (!getServer().getWorlds().contains(getServer().getWorld(worldName))){
						player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GREEN + "World '" + ChatColor.RED + worldName + ChatColor.GREEN + "' does not exist.");
					return true;
					}
				}
			}
			
			
			if ((args.length == 1 || args.length == 2) && args[0].equalsIgnoreCase("scoreboard")){
				UUID playerUUID = player.getUniqueId();
				if (args.length == 1){
					if(RPGSkills.showStatsBoard.get(playerUUID) == null){
						RPGSkills.showStatsBoard.put(playerUUID, true);
					}else{
						showStatsBoard.put(player.getUniqueId(), !(showStatsBoard.get(player.getUniqueId())));
					}
					
					if (!RPGSkills.showStatsBoard.get(playerUUID)){
						player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GREEN + "Turned [OFF] scoreboard.");
							player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
					}else if(RPGSkills.showStatsBoard.get(playerUUID)){
						player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GREEN + "Turned [ON] scoreboard.");
						player.sendMessage(ChatColor.GRAY + "[RPGSkills] Say '/rs scoreboard clear' to remove the current scoreboard at anytime.");
					}
					return true;
				}
				if (args.length == 2 && args[1].equalsIgnoreCase("clear")){
					player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
					return true;
				}
			}
			
			if (args.length == 1 && args[0].equalsIgnoreCase("reload") && player.hasPermission("rpgskills.reload")){
				System.out.println("[RPGSkills] Saving data [...]");
				SaveValues();
				System.out.println("[RPGSkills] Saving data [DONE]");
				System.out.println("[RPGSkills] Reloading [...]");
				CheckFiles();
				ConfigParser.scanFiles();
				ActiveWorldsData.clear();
				ActiveWorldNames.clear();
				if (ConverterWorlds != null){
				ConverterWorlds.clear();
				}
				LoadValues();
				for (Integer task : magicRegenTaskID){
					Bukkit.getScheduler().cancelTask(task);
				}
				runMPRegenScheduler();
				getServer().resetRecipes();
				addCustomRecipes();
				System.out.println("[RPGSkills] Reloading [DONE]");
				System.out.println("[RPGSkills] Reloaded by " + player.getName());
				player.sendMessage(ChatColor.GOLD + "[RPGSkills] Reloaded!");
				return true;
			}
			
			if (args.length == 1 && args[0].equalsIgnoreCase("update") && player.hasPermission("rpgskills.version")){
				player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GRAY + "Please wait...");
				RPGSkills.checkVersion();
				String currentVersion = Bukkit.getServer().getPluginManager().getPlugin("RPGSkills").getDescription().getVersion();
				if (!currentVersion.equals(RPGSkills.newVersion)){
					System.out.println(player.getName() + " has initiated an auto installation for plugin RPGSkills...");
						updatePlugin(player, RPGSkills.newVersion);
						return true;
				}else{
					player.sendMessage(ChatColor.GRAY + "[RPGSkills] " + ChatColor.RED + "Current version is up to date already.");
					return true;
				}
				
			}
			
		}
		if (sender instanceof Player){
			Player player = (Player)sender;
			player.sendMessage(ChatColor.GREEN + "Strange arguments. Try /rs");
		}

		return true;
	}
	
	private List<String> getLeaderboardInfo(String worldName, String skillName, Integer max, Integer min){
		
		List<Double> cList = new ArrayList<Double>();
		List<String> tList = new ArrayList<String>();
		List<String> rList = new ArrayList<String>();
		
		for (UUID playerUUID : RPGSkills.worldExp.get(worldName).keySet()){
			String pName = Bukkit.getServer().getOfflinePlayer(playerUUID).getName();
			if (pName != null){
				Double exp = ExperienceSystemHandler.getPlayerExperience(worldName, playerUUID, skillName);
				int lvl = ExperienceSystemHandler.convertPlayerExpToLevel(worldName, playerUUID, skillName);
				cList.add(exp);
				tList.add(String.valueOf(exp + "," + pName + "," + lvl));
			}
		}
		//Now we need to order the list
		if (!cList.isEmpty()){
		Collections.sort(cList);
		Collections.reverse(cList);
		
		for (int i = (max - 1); i > (min - 1); i--){
			if (i < cList.size()){
				Double cExp = cList.get(i);
				for (int j = 0; j < tList.size(); j++){
					String str = tList.get(j);
					if (str.split(",")[0].contains(String.valueOf(cExp))){
						String playerName = str.split(",")[1];
						int lvl = Integer.valueOf(str.split(",")[2]);
						rList.add(playerName + ": <" + lvl + "> " + String.valueOf(cExp));
						
						//avoid concurrency
						int index = tList.indexOf(str);
						tList.remove(index);
					}
				}
			}
			
		}
	}
		
		return rList;
	}
	
	private int getLeaderPageCount(String worldName, int perPageCount){
		int results = 0;
		int pages = 0;
		
		
		for (UUID uid : RPGSkills.worldExp.get(worldName).keySet()){
			String pName = Bukkit.getServer().getOfflinePlayer(uid).getName();
			if (pName != null){
				results++;
			}
		}
		String sPages = String.valueOf(results / new Double(perPageCount*1.0));
		if (sPages.contains(".")){
			int fN = Integer.valueOf(sPages.split("\\.")[0]);
			int sN = results % perPageCount;
			if (sN > 0){
				pages++;
			}
			pages += fN;
		}
		return pages;
		
	}
	
	private void exportResource(URL resourceURL, File dest){
        try {
        	dest.createNewFile();
            BufferedReader in = new BufferedReader(new InputStreamReader(resourceURL.openStream()));
            BufferedWriter out = new BufferedWriter(new FileWriter(dest));
            String line = "";
            while((line = in.readLine()) != null){
            	out.write(line);
            	out.newLine();
            	out.flush();
            }
            in.close();
            out.close();

        }catch(Exception e){
        	e.printStackTrace();
        }
	}
	
	public static void checkVersion(){
		try{
        URL oracle = new URL("http://dev.bukkit.org/bukkit-plugins/rpgskills/pages/main/");
        URLConnection urlConn = oracle.openConnection();
        urlConn.setConnectTimeout(20 * 1000);
        
        BufferedReader in = new BufferedReader(
        new InputStreamReader(urlConn.getInputStream()));

        String version;
        while ((version = in.readLine()) != null){
        	if(version.contains("Version: ")){
            newVersion = (version.split(":")[2].trim()).replace("<br>", "");
            break;
        	}
        }
	    in.close();
		} catch (IOException e) {
			
			System.out.println("[RPGSkills] Could not gather update info!");
			newVersion = "null";
		}finally{
		}
	}
	
	private void updatePlugin(Player player, String ver){
		try{
        URL oracle = new URL("http://dev.bukkit.org/bukkit-plugins/rpgskills/");
        URLConnection urlConn = oracle.openConnection();
        urlConn.setConnectTimeout(20 * 1000);
        
        BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

        String version;
        String bridgeLink = "http://dev.bukkit.org";
        
        while ((version = in.readLine()) != null){
        	if(version.contains(">Download</a>")){
                bridgeLink += (version.split("\"")[1]);
                
                oracle = new URL(bridgeLink);
                urlConn = oracle.openConnection();
                urlConn.setConnectTimeout(20 * 1000);
                
                in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
               String dlLink = "";
                while ((version = in.readLine()) != null){
                	if(version.contains("RPGSkills.jar\">Download</a>")){
                		dlLink = (version.split("\"")[3]);
                		player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GRAY + "Downloading RPGSkills " + ChatColor.BLUE + ver);
                		String dest = getDataFolder().getAbsolutePath().split("RPGSkills")[0];
                		FileUtils.copyURLToFile(new URL(dlLink), new File(dest, "RPGSkills.jar"));
                		player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GRAY + "Done");
                		player.sendMessage(ChatColor.GOLD + "[RPGSkills] " + ChatColor.GRAY + "Please use /stop ,then start server.");
                		//Bukkit.getPluginManager().getPlugin("RPGSkills").getPluginLoader().disablePlugin(this);
                		
                		//TODO figure out how to load new instance.
                		
                		break;
                	}
                }
                break;
            	}
        }
	    in.close();
		} catch (IOException e) {
			
			System.out.println("[RPGSkills] Could not gather update info!");
			newVersion = "null";
		}finally{
		}
	}
	private void checkConverter(){
		if (isConverting){
			try {
				MMOConversionHandler.convertLevels(mmoCap);
			} catch (IOException e) {
				System.out.println("[RPGSkills] Error while converting levels.");
			}
		}
	}
	
	private void writeConverterYMLWorlds(){
		FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(converter);
		String mainWorld = getServer().getWorlds().get(0).getName();
		List<String> thisList = new ArrayList<String>();
		thisList.add(mainWorld);
		try{
			customSkillsConfig.options().copyDefaults(true);
			customSkillsConfig.set("worlds", thisList);
			customSkillsConfig.save(converter);
		}catch(IOException e){
			System.out.println("[RPGSkills] Error while writing to converter.yml");
		}
		
	}
	
	private void writeWorldsYML(){
		FileConfiguration customSkillsConfig = YamlConfiguration.loadConfiguration(wo);
		List<String> world = new ArrayList<String>();
		World main = getServer().getWorlds().get(0);
		world.add(main.getName()+",-");
		
		for(World w : getServer().getWorlds()){
			if (w != main){
				world.add(w.getName()+"," + main.getName());
			}
		}
		customSkillsConfig.set("Active Worlds", world);
		try{
			customSkillsConfig.save(wo);
		}catch(IOException e){
			System.out.println("[RPGSkills] Error while writing to worlds.yml");
		}
	}
	
	private void initializeConfigMapValues(String worldName){
		configData.put(worldName, new HashMap<ConfigType,HashMap<Object,Object>>());
		
		for (ConfigType ct : ConfigType.values()){
		configData.get(worldName).put(ct, new HashMap<Object,Object>());
		}
	}
	
	private void checkWorldExpMap(){
		if (RPGSkills.SpellProjectileUUIDs == null){
			SpellProjectileUUIDs = new HashMap<UUID, String>();
		}
		if (RPGSkills.playerMagicPoints == null){
			playerMagicPoints = new HashMap<String, HashMap<UUID, Integer>>();
		}
		for (World world : getServer().getWorlds()){
		if (RPGSkills.worldExp.get(world.getName()) == null){
			RPGSkills.worldExp.put(world.getName(), new HashMap<UUID,HashMap<String,Double>>());
			
		}
		if (RPGSkills.playerMagicPoints.get(world.getName()) == null){
			RPGSkills.playerMagicPoints.put(world.getName(), new HashMap<UUID,Integer>());
		}
		}
	}
	
	
	public static enum MessageType{LevelUp, Join, Leave, ModLevelUp, ModAllLevelUp, MaxLevelAchieve}
	
	
	public static enum ConfigType{TOOLLEVELS,MININGBLOCKSMAP,EXCAVBLOCKSMAP,BREEDINGMOBTYPEDATA,SUMMONINGPROPERTYDATA,
		EGGCRAFTINGPROPERTYDATA,FARMINGBLOCKSMAP,ARCHERYEXPPERENTITY,
		ARCHERYGLOBALMODIFIER,ATTACKDAMAGEMAP,ATTACKEXPPERENTITY,ATTACKGLOBALMODIFIER,DEFENSEBASEEXP,
		DEFENSEGLOBALMODIFIER,DEFENSENEGATEMAP,DEFENSETOOLLEVELS, DEFENSEARMORLEVELS, EXCAVALLOWTREASURES,EXCAVSILKFARMLAND,EXCAVTREASURES,
		FARMINGAUTOREPLANTENABLED,FARMINGDOUBLECHANCE,FARMINGREPLANTCHANCE,FARMINGTOOLMODIFIERS,FARMINGTRIPLECHANCE,
		FARMINGTRIPLEDROPENABLED,FISHINGBASEEXP,FISHINGJUNK,FISHINGJUNKCHANCE,FISHINGTREASURECHANCE,FISHINGTREASURES,
		FISHINGVANILLAENABLED,FISHINGVANILLAPARALLELENABLED,FLETCHINGBASEEXP,FLETCHINGBASEYIELD,ISSILKSPAWNERSENABLED,
		MAGICBASEMP,MAGICEXPPROPERTYDATA,MAGICGAINMP,MAGICREGENMP,MAGICREGENRATE,MAGICSPELLPROPERTYDATA,
		MININGDOUBLEDROPCHANCE,UNARMEDBASEDAMAGE,UNARMEDEXPPERENTITY,UNARMEDGLOBALMODIFIER,WOODCUTTINGDOUBLEDROPCHANCE,
		WOODCUTTINGFORCEAXE,WOODTYPEDATA, FLETCHINGARROWDATA, ARCHERYARROWDATA, ARROWEFFECTDATA, DEFENSEARMORLEVELSENABLED}

}
