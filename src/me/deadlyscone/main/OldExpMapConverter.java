package me.deadlyscone.main;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;

public class OldExpMapConverter implements Serializable{
	private static final long serialVersionUID = -825676998082391488L;
	
    private static File rpgDataFolder = new File(Bukkit.getPluginManager().getPlugin("RPGSkills").getDataFolder(), "/data");
    private static File rpgSkillsFolder = new File(Bukkit.getPluginManager().getPlugin("RPGSkills").getDataFolder(), "/skills");
    private static File converter = new File(Bukkit.getPluginManager().getPlugin("RPGSkills").getDataFolder(), "/converter.yml");
    public static HashMap<String, HashMap<UUID,HashMap<String,Double>>> convertedWorldExp = new HashMap<String, HashMap<UUID,HashMap<String,Double>>>();
    private static String worldName = Bukkit.getWorlds().get(0).getName();
    @SuppressWarnings({ "deprecation", "unchecked", "resource" })
	public static boolean convertOldSystem(){
    	if (rpgSkillsFolder.exists()){
    		convertedWorldExp.put(worldName, new HashMap<UUID, HashMap<String, Double>>());
			HashMap<String, Double> dataMap = new HashMap<String, Double>();
			UUID playerUUID = null;
			converter.delete();
			deleteSkillsFolder(rpgSkillsFolder);
    		for (String skill : RPGSkills.skills){
    			try{
    			File skillFile = new File(rpgDataFolder, "/" + skill +".dat");
    			if (skillFile.exists()){
				FileInputStream fis = new FileInputStream(skillFile);
				ObjectInputStream ois = new ObjectInputStream(fis);
				HashMap<String, Double> fileData = new HashMap<String, Double>();
    			fileData = (HashMap<String, Double>)ois.readObject();
    			
    			Iterator<Entry<String, Double>> it = fileData.entrySet().iterator();
    			while(it.hasNext()){
    				HashMap.Entry<String, Double> pairs = (HashMap.Entry<String, Double>)it.next();
    				
    				if (pairs.getKey() != null && pairs.getValue() != null){
    					
    				playerUUID = Bukkit.getServer().getOfflinePlayer(pairs.getKey()).getUniqueId();
    				Double playerExp = pairs.getValue();
    				dataMap.put(skill, playerExp);
    				//convertedWorldExp.get(worldName).get(playerUUID).put(skill, playerExp);
    				
    				}
    			}
    			skillFile.delete();
    			}
    			}catch(EOFException e){
    				
    			} catch (ClassNotFoundException e) {
    				e.printStackTrace();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
			convertedWorldExp.get(worldName).put(playerUUID, dataMap);
    		return true;
    	}else{
    		return false;
    	}
    	
    	
    }
	private static void deleteSkillsFolder(File dir) {
		
		if(dir.list().length > 0){
			String[] files = dir.list();
			for(String fileInDir : files){
				File toDelete = new File(dir, fileInDir);
				toDelete.delete();
			}
			deleteSkillsFolder(dir);
		}else if(dir.list().length == 0){
			dir.delete();
		}
		
	}
    
}
