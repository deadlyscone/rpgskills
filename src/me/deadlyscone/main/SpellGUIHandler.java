package me.deadlyscone.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import me.deadlyscone.main.RPGSkills.ConfigType;
import me.deadlyscone.skillhandlers.MagicHandler.Spell;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SpellGUIHandler implements Listener{

	private String SpellGUIName = ChatColor.DARK_PURPLE + "SpellBook GUI";
	private HashMap<UUID, ArrayList<String>> lores = new HashMap<UUID, ArrayList<String>>();
	public SpellGUIHandler(RPGSkills plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	@SuppressWarnings("deprecation")
	public static void displayGUI(Player player){
		Inventory gui = Bukkit.getServer().createInventory(null, 36, ChatColor.DARK_PURPLE + "SpellBook GUI");
		
		for (String spell : RPGSkills.MagicSpells){
		List<String> lores = new ArrayList<String>();
		//get the spell level based off the world
		Spell thisSpell = Spell.valueOf(spell.toUpperCase().replace(" ", ""));
		//System.out.println(RPGSkills.configData.get(player.getWorld().getName()).get(ConfigType.MAGICEXPPROPERTYDATA).get(thisSpell.toString()));
		
		String spellLevel = ((String) RPGSkills.configData.get(player.getWorld().getName()).get(ConfigType.MAGICEXPPROPERTYDATA).get(thisSpell.toString())).split(",")[1];
		String spellCost = ((String) RPGSkills.configData.get(player.getWorld().getName()).get(ConfigType.MAGICEXPPROPERTYDATA).get(thisSpell.toString())).split(",")[2];
		String[] data = RPGSkills.SpellGUIMap.get(Spell.valueOf(spell.toUpperCase().replace(" ", ""))).split(",");
		int itemID = Integer.valueOf(data[0]), meta = Integer.valueOf(data[1]);
		int slot = Integer.valueOf(data[2]) - 1;
		ItemStack item = new ItemStack(itemID, 1, (short)meta);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(spell);
		lores.add(ChatColor.GRAY + "Level: " + ChatColor.YELLOW + spellLevel);
		lores.add("MP Cost: " + ChatColor.YELLOW + spellCost);
		itemMeta.setLore(lores);
		item.setItemMeta(itemMeta);
		gui.setItem(slot, item);
		}
		player.openInventory(gui);
	}
	
	@EventHandler
	public void onGUIClick(InventoryClickEvent e){
		
		if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR &&
				e.getInventory().getName().equals(SpellGUIName) && e.getView().getItem(e.getSlot()).equals(e.getCurrentItem())){
			ItemStack item = e.getCurrentItem();
			ItemMeta itemMeta = item.getItemMeta();
			Player player = e.getWhoClicked() instanceof Player ? (Player) e.getWhoClicked() : null;
			ArrayList<String> lore = new ArrayList<String>();
			String spellName = itemMeta.getDisplayName();
			if (lores.get(player.getUniqueId()) != null){
				lore = lores.get(player.getUniqueId());
			}
				lore.add(spellName + "," + itemMeta.getDisplayName().replace(" ", "").toUpperCase());
				lores.put(player.getUniqueId(), lore);
			e.getView().getTopInventory().setItem(e.getSlot(), new ItemStack(Material.AIR));
			e.setCancelled(true);
			if (lores.get(player.getUniqueId()) != null && lores.get(player.getUniqueId()).size() == 2){
				player.closeInventory();
			}
		}else if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR &&
			e.getInventory().getName().equals(SpellGUIName) && !e.getView().getItem(e.getSlot()).equals(e.getCurrentItem())){
			e.setCancelled(true);
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onGUIClose(InventoryCloseEvent e){
		Player player = e.getPlayer() instanceof Player ? (Player) e.getPlayer() : null;
		if (player != null && e.getInventory().getTitle().equals(SpellGUIName) && lores.get(player.getUniqueId()) != null){
			String names = "";
			
			ArrayList<String> lore = new ArrayList<String>();
			int size = lores.get(player.getUniqueId()).size();
			
			if (size >= 1){
				String spellName = lores.get(player.getUniqueId()).get(0).split(",")[0], l = lores.get(player.getUniqueId()).get(0).split(",")[1];
				lore.add(l);
				names = spellName;
				if (size >= 2){
					spellName = lores.get(player.getUniqueId()).get(1).split(",")[0];
					l = lores.get(player.getUniqueId()).get(1).split(",")[1];
					lore.add(l);
					names += " - " + spellName;
				}
			}
			
			ItemStack item = new ItemStack(RPGSkills.MagicWandID);
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(names);
			itemMeta.setLore(lore);
			item.setItemMeta(itemMeta);
			player.getInventory().addItem(item);
			lores.put(player.getUniqueId(), null);
			player.sendMessage(String.valueOf(RPGSkills.MagicWandID));
			
		}
	}
}
